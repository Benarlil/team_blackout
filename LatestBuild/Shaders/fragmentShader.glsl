#version 330 core

//------------------------------------------------------------------------------
//
// File Name:	Particle.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

in vec4 VertexColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;

out vec4 FragColor;

void main()
{
    if (VertexColor.a == 0.0f || texture(ourTexture, TexCoord).a == 0.0f)
      discard; 
    FragColor = texture(ourTexture, TexCoord) * VertexColor;
}
