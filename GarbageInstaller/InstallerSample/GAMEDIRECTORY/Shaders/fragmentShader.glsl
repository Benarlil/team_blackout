#version 330 core

in vec4 VertexColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;

out vec4 FragColor;

void main()
{
    if (VertexColor.a < 0.1f || texture(ourTexture, TexCoord).a < 0.2f)
      discard;
    FragColor = texture(ourTexture, TexCoord) * VertexColor;
}
