#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec4 aColor;
layout(location = 2) in vec2 aTexCoord;
layout(location = 3) in mat4 transformMatrix;
layout(location = 7) in vec2 aTexCoordOffset;

uniform mat4 camera;

out vec4 VertexColor;
out vec2 TexCoord;

void main()
{
  gl_Position = camera * transformMatrix * vec4(aPos, 1.0f);
  VertexColor = aColor;
  TexCoord = aTexCoord + aTexCoordOffset;
}
