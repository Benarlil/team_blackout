﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent] //prevents adding this componnent twice on same object
public class Oscillator : MonoBehaviour
{

    [SerializeField] Vector3 movementVector = new Vector3(10f, 10f, 10f);
    [SerializeField] float period = 10f;

    //todo remove from inspector
    [Range(0, 1)]
    [SerializeField]
    float movementPercent;

    Vector2 startingPos;

    void Start()
    {
        startingPos = transform.position;
    }

    void Update()
    {
        float cycles = Time.time / period;

        const float tau = Mathf.PI * 2f;
        float RawSinWave = Mathf.Sin(cycles * tau);

        //set movement factor
        movementPercent = RawSinWave / 2f + 0.5f;
        Vector2 offset = movementVector * movementPercent;
        transform.position = startingPos + offset;
    }
}