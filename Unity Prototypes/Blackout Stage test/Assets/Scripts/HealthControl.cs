﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthControl : MonoBehaviour {

  public Image healthBar;
  public GameObject Player;
  public GameObject Head;
  public LayerMask otherPlayer;
  public LayerMask enemyLayer;
  float newFill;

  // Use this for initialization
  void Start ()
  {
    newFill = healthBar.fillAmount; // starting fill amount is 1.0

	}
	
	// Update is called once per frame
	void Update ()
  {

    // if the player collides with another player or enemy
    if (Player.GetComponent<CapsuleCollider2D>().IsTouchingLayers(otherPlayer) || Player.GetComponent<CapsuleCollider2D>().IsTouchingLayers(enemyLayer))
    {

      newFill = healthBar.fillAmount - 0.2f; // decrease health

    }

    // smoothly decrease health bar
    healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, newFill, Time.deltaTime * 2);

    Animator playerAnimate = Head.GetComponent<Animator>();

    playerAnimate.SetFloat("PlayerHealth", healthBar.fillAmount);

    // quit the application when someone's health reaches 0
    if (healthBar.fillAmount == 0)
      Application.Quit();

  }
}
