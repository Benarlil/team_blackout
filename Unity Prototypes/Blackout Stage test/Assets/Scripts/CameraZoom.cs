﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {


  public GameObject player1;
  public GameObject player2;
  public Camera Cam;

	// Use this for initialization
	void Start ()
  {
  }
	
	// Update is called once per frame
	void Update ()
  {

    // Get the midpoint between the two players
    Vector3 midpoint;
    midpoint.x = (player1.transform.position.x + player2.transform.position.x) / 2;
    midpoint.y = (player1.transform.position.y + player2.transform.position.y) / 2;
    midpoint.z = transform.position.z;

    // set the camera to that midpoint
    transform.position = midpoint;

    // get the distance between the two players
    float distance = (player1.transform.position - player2.transform.position).magnitude;

    // set camera zoom based on that distance
    Cam.orthographicSize = distance/1.5f;

    // clamp zoom so that it can't get far too close
    if (Cam.orthographicSize < 4)
      Cam.orthographicSize = 4;


  }
}
