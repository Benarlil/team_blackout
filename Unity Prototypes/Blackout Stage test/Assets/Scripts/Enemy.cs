﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

  public GameObject enemy;
  public GameObject homeInOn;
  public float moveSpeed = 2;
  public LayerMask wall;

  Rigidbody2D enemyBody;
  bool randomMove = false;
  float timer;
  BoxCollider2D enemyBox;
  BoxCollider2D hammer;
  float timerMax;

  int health = 3;

  // Use this for initialization
  void Start ()
  {

  // set "ai" to random move or tracking the player
    if (Random.value > 0.4f)
      randomMove = true;

    else
      randomMove = false;

    // get the enemy rigid body
    enemyBody = GetComponent<Rigidbody2D>();

    // get the enemy collision box
    enemyBox = GetComponent<BoxCollider2D>();

    // get the hammer collider from the player
    hammer = homeInOn.GetComponent<BoxCollider2D>();

    // set a random movement timer
    timerMax = Random.value * 10;

    // start moving right away
    timer = timerMax;

	}
	
	// Update is called once per frame
	void Update ()
  {

    timer += Time.deltaTime; // increment the timer

    // position variables
    float x;
    float y;

    // if the enemy is set to move randomly
    if (randomMove)
    {
      // and the timer has reached max
      if (timer >= timerMax)
      {

        timer = 0; // reset the time

        // take random values to decide movement
        float randomValue = Random.Range(0.0f, 1.0f);
        float randomValue2 = Random.value;
        float randomValue3 = Random.value;

        // half of the time
        if (randomValue > 0.5f)
        {
          // move in a positive direction
          x = randomValue2;
          y = randomValue3;
        }

        // other half
        else
        {
          // move in a negative
          x = -randomValue2;
          y = -randomValue3;
        }

        // set up the movement vector
        Vector3 movementVector;
        movementVector.x = x;
        movementVector.y = y;
        movementVector.z = 0;

        // normalize it to stop diagonal from being faster
        movementVector.Normalize();

        // set the velocity
        enemyBody.velocity = movementVector * moveSpeed;

        // set the angle based on where the enemy is moving
        float angle = Mathf.Atan2(movementVector.y, movementVector.x) * Mathf.Rad2Deg;
        // rotate to that angle
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


      }
    }

    // if the enemy is set to home in on the player
    else
    {

      Vector3 current = transform.position; // get the current position
      
      var direction = homeInOn.transform.position - current; // find the direction by taking the player position 
      
      // get the angle in that direction
      var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
      
      // rotate in that direction
      transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

      // setup the movement vectors
      Vector3 movementVector;
      movementVector.x = transform.right.x;
      movementVector.y = transform.right.y;
      movementVector.z = 0;

      // move the enemy towards the player
      enemyBody.velocity = movementVector * moveSpeed;

    }

    // if the enemy is hit by the hammer
    if (enemyBox.IsTouching(hammer))
    {

      health -= 1; // decrease health

      // kill the enemy when health hits 0
      if(health <= 0)
        enemy.SetActive(false);
    }
      
  }

}
