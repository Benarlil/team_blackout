﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRigid : MonoBehaviour
{

    public GameObject playerObj; // the player object (either a ship or helicopter)
    Vector3 camPosOff; // the vector for the camera's position based on the player's


    // Use this for initialization
    void Start()
    {

        // For the purposes of this small project, just search for a helicopter OR a spaceship with if statements
        if (GameObject.Find("Helicopter") != null)
            playerObj = GameObject.Find("Helicopter");

        else if (GameObject.Find("Spaceship") != null)
            playerObj = GameObject.Find("Spaceship");

        // set the camera's offset based on starting player position and starting camera position
        camPosOff = transform.position - playerObj.transform.position;

    }

    // Camera only uses LateUpdate
    void Update()
    {}

    // Update after the player sprite does
    void LateUpdate()
    {

        transform.position = playerObj.transform.position + camPosOff; // Move the camera to the position of the player offset by the earlier found amount

    }
}