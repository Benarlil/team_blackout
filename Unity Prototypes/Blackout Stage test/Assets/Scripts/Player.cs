﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

  public float startRotSpeed = 1;
  public float capRotSpeed = 3;
  public float moveSpeed = 5;
  public float multiplier = 10;
  public LayerMask collidableLayers;
  public int PlayerNumber;


  int rotationDirection = 1;
  float currSpeed;
  Rigidbody2D playerBody;
  BoxCollider2D hammerCollider;
  //CapsuleCollider2D playerCollider;
  float collideTimer;
  Vector3 startingPos;

	// Use this for initialization
	void Start ()
  {

    // get the player rigid body
    playerBody = GetComponent<Rigidbody2D>();

    // get the collider for the weapon
    hammerCollider = GetComponent <BoxCollider2D> ();

    //get the collider for the player
    //playerCollider = GetComponent<CapsuleCollider2D>();

    currSpeed = startRotSpeed; // start rotation

    collideTimer = 0.5f;

        startingPos = transform.position;

  }

  // Update is called once per frame
  void Update()
  {

    // if below minimum rotation speed
    if (currSpeed < startRotSpeed)
      currSpeed = startRotSpeed; // clamp

    if (PlayerNumber == 1)
    {
      // switch and slow rotation direction if the hammer hits something
      if (hammerCollider.IsTouchingLayers(collidableLayers))
      {

        if (collideTimer <= 0)
        {

          rotationDirection *= -1;

          currSpeed /= 2;

          collideTimer = 0.5f;

        }

      }

      // otherwise continue upping rotation speed
      else
      {
        currSpeed += Time.deltaTime;

        // clamp if it gets above the set max
        if (currSpeed > capRotSpeed)
          currSpeed = capRotSpeed;

      }

    }

    // same as above but for player 2
    else if (PlayerNumber != 1)
    {
      if (hammerCollider.IsTouchingLayers(collidableLayers))
      {

        if (collideTimer <= 0)
        {

          rotationDirection *= -1;

          currSpeed /= 2;

          collideTimer = 0.5f;

        }

      }

      else
      {
        currSpeed += Time.deltaTime;

        if (currSpeed > capRotSpeed)
          currSpeed = capRotSpeed;
      }

    }

    // set the rotation based on the above
    transform.Rotate(0, 0,((currSpeed + (multiplier * Time.deltaTime)) * rotationDirection));

    Vector2 direction; // create an empty vector for direction

    // get different axis based on player
    if (PlayerNumber == 1)
    {
      direction.x = Input.GetAxis("Horizontal"); // Get the horizontial direction based on input

      direction.y = Input.GetAxis("Vertical"); // Get the vertical direction based on input
    }

    else
    {

      direction.x = Input.GetAxis("Horizontal2"); // Get the horizontial direction based on input

      direction.y = Input.GetAxis("Vertical2"); // Get the vertical direction based on input

    }

    direction.Normalize(); // Normalize the vector to make diagonal movement as fast as normal movement

    playerBody.velocity = (direction * moveSpeed); // Multiply direction by the preset movespeed to set velocity

    collideTimer -= Time.deltaTime;

  }
    void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Hazard":
                transform.position = startingPos;
                break;
        }
    }
}
