﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

  public TwoPlayerController owner;
  public LayerMask collidible;

  void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.layer == collidible)
    {
      if (owner.collideTimer <= 0)
      {

        owner.rotationDirection *= -1;

        owner.currSpeed /= 2;

        owner.collideTimer = 0.5f;

      }
    }
  }

}
