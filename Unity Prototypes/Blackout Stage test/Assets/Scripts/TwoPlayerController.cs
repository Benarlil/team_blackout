﻿/****************************************************************************/
/*!
"Copyright (c) 2018 Jeremy McCarty <jeremymmccarty@gmail.com"
"Portions Copyright (c) 2009 Remi Gillig <remigillig@gmail.com>"
All rights reserved worldwide.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files 
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, 
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/****************************************************************************/

//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	TwoPlayerController.cs                                            //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// Desc: Two player controller management for purposes of prototyping.          //
//                                                                              //
// Copyright © 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

using InputManagerModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;
using UnityEngine.UI;

public class TwoPlayerController : MonoBehaviour {

  // The index of this player.
  public PlayerIndex CurrentPlayer = PlayerIndex.One;

  public float Health = 1.0f; // player health

  public Image HPBar;

  // Speed modifiers
  public float startRotSpeed = 5;
  public float capRotSpeed = 20;
  public float moveSpeed = 3;
  public float multiplier = 5;

  // Made public for the player weapon to use
  public float collideTimer = 0.5f;
  public int rotationDirection = 1;
  public float currSpeed = 5;

  // For internal use
  Rigidbody2D playerBody;
  Vector3 startingPos;

  // Use this for initialization
  void Start()
  {
    // get the player rigid body
    playerBody = GetComponent<Rigidbody2D>();

    currSpeed = startRotSpeed; // start rotation

    startingPos = transform.position; // get the start position
  }

  // What functions are bound to what inputs
  void OnEnable ()
  {
    InputManager.OnDpadMove += Movement; // Set movement based on Dpad axis
    InputManager.OnLeftJoystickMove += Movement; // Set movement based on joystick axis
  }

  // Update is called once per frame
  void Update()
  {
    // if below minimum rotation speed
    if (currSpeed < startRotSpeed)
      currSpeed = startRotSpeed; // clamp

    // otherwise keep building speed
    else
    {
      currSpeed += Time.deltaTime;

      // clamp if beyond max speed
      if (currSpeed > capRotSpeed)
        currSpeed = capRotSpeed;
    }

    // set the rotation based on the above
    transform.Rotate(0, 0, ((currSpeed + (multiplier * Time.deltaTime)) * rotationDirection));

    HPBar.fillAmount = Health;

    collideTimer -= Time.deltaTime; // decrement the collider timer regardless of collisions made

  }

  void OnCollisionEnter2D(Collision2D collision)
  {
    switch (collision.gameObject.tag)
    {
      case "Hazard":
        transform.position = startingPos;
        break;

      case "Weapon":
        Health -= 0.1f;
        break;

      case "Player":
        Health -= 0.05f;
        playerBody.velocity = playerBody.velocity * -1.2f;
        break;
    }
  }

private void Movement(PlayerIndex index, Vector2 axis)
  {
    // Return if this isn't the index being dispatched
    if (CurrentPlayer != index)
    {
      return;
    }

    playerBody.velocity = (axis * moveSpeed); // set the player's movement 
  }

}
