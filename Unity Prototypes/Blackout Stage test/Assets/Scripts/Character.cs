﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public float speed;

    public KeyCode left;
    public KeyCode right;
    public KeyCode up;
    public KeyCode down;

    Vector3 startingPos;

    void Start()
    {
        startingPos = transform.position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Hazard":
                transform.position = startingPos;
                break;
        }
    }

    void Update()
    {
       

        if(Input.GetKey(left))
        {
            transform.Translate(-speed * Time.deltaTime, 0, 0);
        }
        if(Input.GetKey(right))
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);
        }
        if(Input.GetKey(up))
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
        }
        if(Input.GetKey(down))
        {
            transform.Translate(0, -speed * Time.deltaTime, 0);
        }
    }
}
