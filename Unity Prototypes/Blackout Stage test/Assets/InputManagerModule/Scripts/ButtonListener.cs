﻿using InputManagerModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonListener : MonoBehaviour {

    public ActivateEvent ButtonEvent;

    public void ToggleActive()
    {
        // Toggle the active state of the scriptable object
        ButtonEvent.ToggleActive();
    }

    public void ToggleRumble()
    {
        // Calling rumble for 0.3 seconds at 0 strength in the left motor, 5 in the right.
        StartCoroutine(InputManager.Rumble(ButtonEvent.CurrentPlayer, 0.3f, 0, 5));
    }
  
}
