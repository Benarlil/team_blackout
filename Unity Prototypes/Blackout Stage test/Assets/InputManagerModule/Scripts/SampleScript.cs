﻿/****************************************************************************/
/*!
"Copyright (c) 2018 Jeremy McCarty <jeremymmccarty@gmail.com"
"Portions Copyright (c) 2009 Remi Gillig <remigillig@gmail.com>"
All rights reserved worldwide.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files 
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, 
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/****************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;
using InputManagerModule;

public class SampleScript : MonoBehaviour {

    // What player index should this be?
    public PlayerIndex CurrentPlayer = PlayerIndex.One;

    // OnEnable: Used to "sign up" to specific delegates and bind their functions. This function is called when the object becomes enabled and active. 
    void OnEnable()
    {
        //// Sample signature: InputManager.DELEGATE_ACTION += RESPONDING_FUNCTION (+= binds the responding function to the delegate action)
        //InputManager.OnFaceDownPressed += FaceDown;
        //InputManager.OnFaceDownReleased += FaceDownReleased;
        //InputManager.OnFaceDownHeld += FaceDownHeld;

        //InputManager.OnFaceLeftPressed += FaceLeft;
        //InputManager.OnFaceLeftReleased += FaceLeftReleased;
        //InputManager.OnFaceLeftHeld += FaceLeftHeld;

        //InputManager.OnFaceRightPressed += FaceRight;
        //InputManager.OnFaceRightReleased += FaceRightReleased;
        //InputManager.OnFaceRightHeld += FaceRightHeld;

        //InputManager.OnFaceUpPressed += FaceUp;
        //InputManager.OnFaceUpReleased += FaceUpReleased;
        //InputManager.OnFaceUpHeld += FaceUpHeld;

        //InputManager.OnSpecialLeftPressed += SpecialLeft;
        //InputManager.OnSpecialLeftReleased += SpecialLeftReleased;
        //InputManager.OnSpecialLeftHeld += SpecialLeftHeld;

        //InputManager.OnSpecialRightPressed += SpecialRight;
        //InputManager.OnSpecialRightReleased += SpecialRightReleased;
        //InputManager.OnSpecialRightHeld += SpecialRightHeld;

        //InputManager.OnRightShoulderPressed += RightShoulder;
        //InputManager.OnRightShoulderReleased += RightShoulderReleased;
        //InputManager.OnRightShoulderHeld += RightShoulderHeld;

        //InputManager.OnLeftShoulderPressed += LeftShoulder;
        //InputManager.OnLeftShoulderReleased += LeftShoulderReleased;
        //InputManager.OnLeftShoulderHeld += LeftShoulderHeld;

        //InputManager.OnRightTriggerPressed += RightTrigger;
        //InputManager.OnLeftTriggerPressed += LeftTrigger;

        //InputManager.OnDpadMove += Dpad;
        //InputManager.OnLeftJoystickMove += LeftJoystick;
        //InputManager.OnRightJoystickMove += RightJoystick;
    }

    // OnDisable: Used to clear the function from the delegate binding. This function is called when the behaviour becomes disabled () or inactive.
    void OnDisable()
    {
        //// Sample signature: InputManager.DELEGATE_ACTION -= RESPONDING_FUNCTION (-= unbinds the responding function from the delegate action)
        //InputManager.OnFaceDownPressed -= FaceDown;
        //InputManager.OnFaceDownReleased -= FaceDownReleased;
        //InputManager.OnFaceDownHeld -= FaceDownHeld;

        //InputManager.OnFaceLeftPressed -= FaceLeft;
        //InputManager.OnFaceLeftReleased -= FaceLeftReleased;
        //InputManager.OnFaceLeftHeld -= FaceLeftHeld;

        //InputManager.OnFaceRightPressed -= FaceRight;
        //InputManager.OnFaceRightReleased -= FaceRightReleased;
        //InputManager.OnFaceRightHeld -= FaceRightHeld;

        //InputManager.OnFaceUpPressed -= FaceUp;
        //InputManager.OnFaceUpReleased -= FaceUpReleased;
        //InputManager.OnFaceUpHeld -= FaceUpHeld;

        //InputManager.OnSpecialLeftPressed -= SpecialLeft;
        //InputManager.OnSpecialLeftReleased -= SpecialLeftReleased;
        //InputManager.OnSpecialLeftHeld -= SpecialLeftHeld;

        //InputManager.OnSpecialRightPressed -= SpecialRight;
        //InputManager.OnSpecialRightReleased -= SpecialRightReleased;
        //InputManager.OnSpecialRightHeld -= SpecialRightHeld;

        //InputManager.OnRightShoulderPressed -= RightShoulder;
        //InputManager.OnRightShoulderReleased -= RightShoulderReleased;
        //InputManager.OnRightShoulderHeld -= RightShoulderHeld;

        //InputManager.OnLeftShoulderPressed -= LeftShoulder;
        //InputManager.OnLeftShoulderReleased -= LeftShoulderReleased;
        //InputManager.OnLeftShoulderHeld -= LeftShoulderHeld;

        //InputManager.OnRightTriggerPressed -= RightTrigger;
        //InputManager.OnLeftTriggerPressed -= LeftTrigger;

        //InputManager.OnDpadMove -= Dpad;
        //InputManager.OnLeftJoystickMove -= LeftJoystick;
        //InputManager.OnRightJoystickMove -= RightJoystick;
    }

    // Test binding functions.

    #region Axis Bound Functions
    //// Axis bound functions require a player index and an axis
    //private void RightJoystick(PlayerIndex index, Vector2 axis)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Right Joystick " + axis + " from player index " + index);
    //}

    //private void LeftJoystick(PlayerIndex index, Vector2 axis)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Left Joystick " + axis + " from player index " + index);
    //}

    //private void Dpad(PlayerIndex index, Vector2 axis)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("DPAD " + axis + " from player index " + index);
    //}

    //// Pressure bound functions require a player index and an axis value
    //private void LeftTrigger(PlayerIndex index, float axisValue)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Left Trigger " + axisValue + " from player index " + index);
    //}

    //private void RightTrigger(PlayerIndex index, float axisValue)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Right Trigger " + axisValue + " from player index " + index);
    //}
    #endregion

    #region Button Down Bound Functions
    //// Button down bound functions require only a player index
    //private void LeftShoulder(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Left Shoulder from player index " + index);
    //}

    //private void RightShoulder(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Right Shoulder from player index " + index);
    //}

    //private void SpecialRight(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Special Right from player index " + index);
    //}

    //private void SpecialLeft(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Special Left from player index " + index);
    //}

    //private void FaceUp(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Up from player index " + index);
    //}

    //private void FaceRight(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Right from player index " + index);
    //}

    //private void FaceLeft(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Left from player index " + index);
    //}

    //private void FaceDown(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Down from player index " + index);
    //}
    #endregion

    #region Button Held Bound Functions
    //private void FaceDownHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Down held from player index " + index);
    //}

    //private void FaceLeftHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Left held from player index " + index);
    //}

    //private void FaceRightHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Right held from player index " + index);
    //}

    //private void FaceUpHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Up held from player index " + index);
    //}

    //private void SpecialLeftHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Special Left held from player index " + index);
    //}

    //private void SpecialRightHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Special Right held from player index " + index);
    //}

    //private void RightShoulderHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Right Shoulder held from player index " + index);
    //}

    //private void LeftShoulderHeld(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Left Shoulder held from player index " + index);
    //}

    #endregion

    #region Button Released Bound Functions
    //// Button down bound functions require only a player index
    //private void FaceDownReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Down released from player index " + index);
    //}

    //private void LeftShoulderReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Left Shoulder released from player index " + index);
    //}

    //private void RightShoulderReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Right Shoulder released from player index " + index);
    //}

    //private void SpecialRightReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Special Right released from player index " + index);
    //}

    //private void SpecialLeftReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Special Left released from player index " + index);
    //}

    //private void FaceUpReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Up released from player index " + index);
    //}

    //private void FaceRightReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Right released from player index " + index);
    //}

    //private void FaceLeftReleased(PlayerIndex index)
    //{
    //    // Return if this isn't the index being dispatched
    //    if (CurrentPlayer != index) return;

    //    print("Face Left released from player index " + index);
    //}
    #endregion

    #region User Functions
    // User helper functions for the given index
    //private void Rumble()
    //{
    //    // Calling rumble for 0.3 seconds at 0 strength in the left motor, 5 in the right.
    //    StartCoroutine(InputManager.Rumble(CurrentPlayer, 0.3f, 0, 5));
    //}

    //private void Deactivate()
    //{
    //    // Deactivate this input.
    //    InputManager.SetInputActive(CurrentPlayer, false);
    //}
    #endregion
}
