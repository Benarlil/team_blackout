﻿

using InputManagerModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;



public class PlayerController : MonoBehaviour {

    // Basic input vars
    public PlayerIndex CurrentPlayer = PlayerIndex.One;
    private float MoveSpeed = 10;

    private void OnEnable()
    {
        // Subscribe to the event
        InputManager.OnRightJoystickMove += MovementUpdate;
    }

    private void OnDisable()
    {
        // Unsibscribe to the event
        InputManager.OnRightJoystickMove -= MovementUpdate;
    }

    private void MovementUpdate(PlayerIndex index, Vector2 axis)
    {
        // Return if this isn't the index being dispatched
        if (CurrentPlayer != index) return;

        // Update velocity
        GetComponent<Rigidbody2D>().velocity = axis * MoveSpeed;
    }
}
