﻿using InputManagerModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

[CreateAssetMenu(fileName ="NewActivateEvent")]
public class ActivateEvent : ScriptableObject {

    // Variables for the scriptable object state
    public PlayerIndex CurrentPlayer;
    public bool ActiveStatus;

    public void Init()
    {
        // Set initial activation status
        ActiveStatus = (GamePad.GetState(CurrentPlayer).IsConnected) ? true : false;
    }

    public void ToggleActive()
    {
        // Toggle between active and inactive with the input manager
        ActiveStatus = !ActiveStatus;
        InputManager.SetInputActive(CurrentPlayer, ActiveStatus);
    }
}
