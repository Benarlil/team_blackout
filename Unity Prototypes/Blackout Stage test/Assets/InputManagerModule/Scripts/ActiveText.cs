﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveText : MonoBehaviour {

    public ActivateEvent ButtonEvent;

    // Use this for initialization
    void Start ()
    {
        // Init the scriptable objects
        ButtonEvent.Init();
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Text>().text = "Controller active: " + ButtonEvent.ActiveStatus;
	}
}
