//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Stub.h                                                            //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "GameStateManager.h"
class Stub : public Engine::GameState
{
public:
  void Load()  override;
  void Init()  override;
  void Update(float dt)  override;
  void Unload()  override;
  void Shutdown()  override;
  void OnAction(sf::Event& event, float dt) override;
};
