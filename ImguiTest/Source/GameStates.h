//------------------------------------------------------------------------------
//
// File Name:	GameStates.h
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
REGISTER_GAMESTATE(Quit)
REGISTER_GAMESTATE(Invalid)
REGISTER_GAMESTATE(Restart)
REGISTER_GAMESTATE(Color)
REGISTER_GAMESTATE(HardR)
REGISTER_GAMESTATE(Sandbox)
REGISTER_GAMESTATE(SplashScreens)
REGISTER_GAMESTATE(MainMenu)
