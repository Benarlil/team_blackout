#pragma once
#include "stdafx.h"
#include "ColliderLine.h"



LineSegment::LineSegment()
{
	_Points[0] = { 0.0f , 0.0f};
	_Points[1] = { 0.0f , 0.0f};
}

LineSegment::LineSegment(glm::vec2 point1, glm::vec2 point2)
{
	_Points.push_back(point1);
	_Points.push_back(point2);
}

LineSegment::~LineSegment()
{

}

glm::vec2 LineSegment::operator[](const int index)
{
	if (index < 2)
	{
		return _Points[index];
	}
	else
		return glm::vec2(0.0f);

}

	ColliderLine::ColliderLine() : Collider(ColliderTypeLine)
	{
		_Lines.push_back(LineSegment());
	}

	ColliderLine::~ColliderLine()
	{
		
	}

	void ColliderLine::AddLine(glm::vec2 point1, glm::vec2 point2)
	{
		_Lines.push_back(LineSegment(point1, point2));
	}


