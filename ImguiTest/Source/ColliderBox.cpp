//------------------------------------------------------------------------------
//
// File Name:	ColliderBox.h
// Author(s):	William Patrick
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include "ColliderBox.h"

ColliderBox::ColliderBox() : Collider(ColliderTypeBox)
{
	_Width = 0.f;
	_Height = 0.f;
}
ColliderBox::ColliderBox(float width, float height) : Collider(ColliderTypeBox)
{
	_Width = width;
	_Height = height;

}

ColliderBox::ColliderBox(bool inverseStatus, float width, float height) : Collider(ColliderTypeBox)
{
	_IsInverse = inverseStatus;
	_Width = width;
	_Height = height;
}

ColliderBox::ColliderBox(bool inverseStatus) : Collider(ColliderTypeBox)
{
	_IsInverse = inverseStatus;
	_Width = 0.f;
	_Height = 0.f;
}

ColliderBox::ColliderBox(ColliderBox& original) : Collider(ColliderTypeBox)
{
	_Width = original.GetWidth();
	_Height = original.GetHeight();

}

ColliderBox::~ColliderBox()
{
}
float ColliderBox::GetWidth()
{
	return _Width;
}
float ColliderBox::GetHeight()
{
	return _Height;
}
void ColliderBox::SetWidth(float width)
{
	if (width != _Width)
	{
		_Width = width;
	}
}
void ColliderBox::SetHeight(float height)
{
	if (height != _Height)
	{
		_Height = height;
	}
}
void ColliderBox::SetInverseStatus(bool status)
{
	_IsInverse = status;
}

void ColliderBox::SwapInverseStatus()
{
	if (_IsInverse == true)
	{
		_IsInverse = false;
	}
	else
	{
		_IsInverse = true;
	}
}
bool ColliderBox::GetInverseStatus()
{
	return _IsInverse;
}