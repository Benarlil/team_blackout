//------------------------------------------------------------------------------
//
// File Name:	Gui.cpp
// Author(s):	Jack Klein (jack.klein)
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Imgui/imgui.h"
#include "imgui-SFML.h"
#include "Gui.h"
#include "ObjectGui.h"
#include "Graphics.h"
#include "Engine.h"
#include "GameStateManager.h"

#include "Serialize.h"
#include "GameObjectManager.h"
#include "DebugObject.h"

#include <sstream>

#include "Imgui/imgui.h"
#include "Imgui/imgui-SFML.h"
#include "Imgui/imgui_impl_opengl3.h"


std::vector<int> isObjectOpen;
static constexpr int MAX_NAME_LENGTH = 50;
static constexpr int LENGTH_OF_JSON_PATH = 7;
static constexpr int LENGTH_OF_SOUND_PATH = 9;
static constexpr int LENGTH_OF_ART_PATH = 9;
static constexpr int NUM_OF_COMPONENTS = 6;

bool lockInput = false;

/*
 * Initializes ImGui
 * args: none
 * outputs: None
 */
void ImGuiInit(void)
{
	/* Initialize SFML ImGui */
	ImGui::SFML::Init(Graphics::GetMainWindow());

	const char* glsl_version = "#version 130";

	/* Set up ImGui binding */
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls


	ImGui_ImplOpenGL3_Init(glsl_version);
}

/*
 * Updates ImGui
 * args: imGuiFlag - a flag to trigger imGui
		 clock - a clock to get dt from
		 objectList - List of object names to give to debug
 * outputs: None
 */
void ImGuiUpdate(bool imGuiFlag, sf::Clock clock, Serializer & serializer)
{
	/* Push back objects onto object bool list */
	isObjectOpen.resize(serializer.GetObjectList().size(), false);

	ImGui::SFML::Update(Graphics::GetMainWindow(), clock.getElapsedTime());

	if (imGuiFlag)
	{
		CreateDebugGui(serializer, clock);
	}
  else
  {
    lockInput = false;
  }

	ImGui::SFML::Render(Graphics::GetMainWindow());
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

/*
 * Shuts down ImGui
 * args: None
 * outputs: None
 */
void ImGuiShutdown(void)
{
	ImGui::SFML::Shutdown();
}

/*
 * Processes ImGui event
 * args: event - The window event ImGui will process
 * outputs: None
 */
void ImGuiProcessEvent(sf::Event event)
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui::SFML::ProcessEvent(event);
}

bool onStart = true; /* Another bool to make sure debug objects are only created once */

/*
 * Creates an ImGui window for the base debug functionality
 * args: serializer - the serializer to use for the object list
		 clock - the clock used to get dt
 * outputs: None
 */
static void CreateDebugGui(Serializer & serializer, sf::Clock clock)
{
	ImGui::Begin("Debug Window");

  ImGui::SetWindowFontScale(1.5f);

	/* Lock input if window is focused */
	if (ImGui::IsAnyWindowFocused())
	{
		lockInput = true;
	}
	else
	{
		lockInput = false;
	}

	ImGui::Text("Frame rate: %3f", ImGui::GetIO().Framerate);
	
	ImGui::Text("Mouse position x: %i", sf::Mouse::getPosition().x);
	ImGui::Text("Mouse position y: %i", sf::Mouse::getPosition().y);

	if (onStart)
	{
		/* Loop through all the objects and make a debug draw for them */
    for (unsigned int i = 0; i < serializer.GetObjectList().size(); i++)
    {
      /* But only if it has a collider & physics */
      if (GameObjectManagerFindObject(serializer.GetObjectList()[i]).Get(Component::ComponentTypes::ctCollider))
      {
        CreateDebugDraw(serializer.GetObjectList()[i]);

        onStart = false;
      }
    }

	}

	/* Loop through and make bools for each object */
	bool addNewObject = false; /* Special bool not made in loop */
	if (ImGui::BeginMenu("Objects"))
	{
    ImGui::SetWindowFontScale(1.5f);
		for (unsigned int i = 0; i < isObjectOpen.size(); ++i)
		{
			ImGui::MenuItem(serializer.GetObjectList()[i].c_str(), nullptr, reinterpret_cast<bool*>(&(isObjectOpen[i])));
		}

		/* Last menu item for adding new object */
		ImGui::MenuItem("Add New Object", nullptr,  reinterpret_cast<bool*>(&(addNewObject)));

		ImGui::EndMenu();
	}

	/* Switching levels */
	std::vector<int> jsonBools;
	if (ImGui::BeginMenu("Levels"))
	{
    ImGui::SetWindowFontScale(1.5f);
		/* Look into directory json for all levels */
		std::experimental::filesystem::path jsonDirectory = ".\\json";
		int serializerCount = 0;
		for (auto it : std::experimental::filesystem::directory_iterator(jsonDirectory))
		{
			std::ostringstream oss;
			oss << it;
			std::string path = oss.str();

			if (path.substr(LENGTH_OF_JSON_PATH, path.size() - LENGTH_OF_JSON_PATH) != "Input")
			{
				/* Create ImGui menu for each file */
				jsonBools.push_back(false);
				ImGui::MenuItem((path.substr(LENGTH_OF_JSON_PATH, path.size() - LENGTH_OF_JSON_PATH)).c_str(), nullptr, reinterpret_cast<bool*>(&(jsonBools[serializerCount])));

				/* Change the level if json file selected */
				if (jsonBools[serializerCount])
				{
					serializer.ClearObjectList();
					serializer.SetName(path.substr(LENGTH_OF_JSON_PATH, path.size() - LENGTH_OF_JSON_PATH));
					if (Engine::GameStateManager::GetCurrentName() == "Color")
					{
            onStart = true;
						Engine::GameStateManager::Go(Engine::GAMESTATES::HardR);
					}
					else
					{
            onStart = true;
						Engine::GameStateManager::Go(Engine::GAMESTATES::Color);
					}
				}

				serializerCount++;
			}
		}


		ImGui::EndMenu();
	}

	/* Loop through and create a gui for each object */
	for (unsigned int i = 0; i < serializer.GetObjectList().size(); ++i)
	{
		GameObject& gO = GameObjectManagerFindObject(serializer.GetObjectList()[i].c_str());

		if (isObjectOpen[i])
		{
      ImGui::SetNextWindowPos(ImVec2(1000, 0));
			ImGui::SetNextWindowSize(ImVec2(600, 1000));
			CreateObjectGui(gO, serializer, clock);
		}
	}

	/* Toggle all debug draw */
	if (ImGui::Button("Toggle All Debug Draw"))
	{
			ToggleAllDebugObjects();
	}

	/* Serialize all objects */
	if (ImGui::Button("Serialize All Objects"))
	{
		/* Loop through all objects and serialize them */
		for (unsigned int i = 0; i < serializer.GetObjectList().size(); i++)
		{
			serializer.WriteObject(GameObjectManagerFindObject(serializer.GetObjectList()[i]));
		}

		serializer.Serialize();
	}

	if (ImGui::Button("Clone level"))
	{
		ImGui::OpenPopup("Clone Level");
	}

	if (ImGui::Button("Delete level"))
	{
		ImGui::OpenPopup("Delete Level?");
	}

	/* Adding new object */
	static bool newObjectErrorFlag = false; /* Flag for the new object error */
	if (addNewObject)
	{
		/* Loop through all objects and check if a new object already exists */
		for (unsigned int i = 0; i < serializer.GetObjectList().size(); i++)
		{
			if (serializer.GetObjectList()[i] == "New Object")
			{
				newObjectErrorFlag = true;
			}
		}
		/* If new object doesn't already exist, make it */
		if (!newObjectErrorFlag)
		{
			GameObjectManagerCreateObject("New Object");
			serializer.addToObjectList("New Object");
		}
	}

	/* Error message popup */
	if (newObjectErrorFlag)
	{
		ImGui::OpenPopup("Error: Can't Add New Object");
		newObjectErrorFlag = false;
	}

	/* If we tried to add a new object without changing the last one throw an error */
	if (ImGui::BeginPopupModal("Error: Can't Add New Object", nullptr))
	{
		ImGui::Text("Error! Cannot add new object without renaming previously added object!");
		if (ImGui::Button("OK"))
		{
			ImGui::CloseCurrentPopup();
		}

		ImGui::EndPopup();
	}

	/* ImGui popup for cloning a level */
	if (ImGui::BeginPopupModal("Clone Level", nullptr))
	{
		ImGui::SetWindowSize(ImVec2(400, 100));
		static char newLevelName[MAX_NAME_LENGTH] = { 0 };
		ImGui::InputText("New Level Name", newLevelName, MAX_NAME_LENGTH);

		if (ImGui::Button("Create Level"))
		{
			extern Serializer serializerLevel1;
			std::string newFileName(newLevelName);
			newFileName += ".json";
			serializerLevel1.CreateJSON(newFileName);
			serializer.ClearObjectList();
			serializer.SetName(newFileName);
			if (Engine::GameStateManager::GetCurrentName() == "Color")
			{
				Engine::GameStateManager::Go(Engine::GAMESTATES::HardR);
			}
			else
			{
				Engine::GameStateManager::Go(Engine::GAMESTATES::Color);
			}
			ImGui::CloseCurrentPopup();
		}

    ImGui::SameLine();

    if (ImGui::Button("Actually don't"))
    {
      ImGui::CloseCurrentPopup();
    }

		ImGui::EndPopup();
	}

	/* Delete Level popup */
	if (ImGui::BeginPopupModal("Delete Level?", nullptr))
	{
		ImGui::Text("Are you sure you want to delete this level? (This will also quit the game!)");
		if (ImGui::Button("Yeah, delete the level"))
		{
			extern Serializer serializerLevel1;
			std::string fileToRemove = ".\\json\\" + serializerLevel1.GetName();
			remove(fileToRemove.c_str());
			Engine::GameStateManager::Go(Engine::GAMESTATES::Quit);
		}
		ImGui::SameLine();
		if (ImGui::Button("Actually wait no, don't do that"))
		{
			ImGui::CloseCurrentPopup();
		}

		ImGui::EndPopup();
	}

	ImGui::End();
}


