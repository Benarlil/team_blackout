//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorIcon.cpp                                                  //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "BehaviorIcon.h"
#include "GameObjectManager.h"
#include "Transform.h"
#include "Sprite.h"
#include "Animation.h"

BehaviorIcon::BehaviorIcon()
{
  parentBehavior = std::shared_ptr<BehaviorPlayer>(NULL);
  healthCurr = 0;
  prevHealth = prevHealth = healthCurr;
}

BehaviorIcon::BehaviorIcon(std::string parent)
{
  GameObject& Parent = GameObjectManagerFindObject(parent);
  parentName = parent;
  parentBehavior = std::dynamic_pointer_cast<BehaviorPlayer>(Parent.Get(Component::ctBehavior));
  healthCurr = parentBehavior->GetHealth();;
  prevHealth = 0;
}

void BehaviorIcon::SetDeathIcon(std::string iconPath)
{
  deadIcon = iconPath;
}

std::string BehaviorIcon::GetDeathIcon(void)
{
  return deadIcon;
}


void BehaviorIcon::SetParent(std::string parent)
{
  GameObject& Parent = GameObjectManagerFindObject(parent);
  parentName = parent;
  parentBehavior = std::dynamic_pointer_cast<BehaviorPlayer>(Parent.Get(Component::ctBehavior));
  healthCurr = parentBehavior->GetHealth();
  prevHealth = healthCurr;
}

void BehaviorIcon::Update(float dt)
{
  GameObject& UI = GameObjectManagerFindObject(Parent().GetName());
  std::shared_ptr<Animation> pAnimation = std::dynamic_pointer_cast<Animation>(UI.Get(Component::ctAnimation));
  healthCurr = parentBehavior->GetHealth();
  if (healthCurr > prevHealth)
  {
    if (healthCurr >= 30)
    {
      std::vector<AnimationFrame> frameList = { { 0, 0.1 }, {1, 0.1}, { 2, 0.1 }, {3, 0.1}, {4, 0.1} };
      AnimationSequence sequence(5, frameList, true);
      pAnimation->PlaySequence(sequence);
    }
  }
    if (healthCurr < prevHealth)
    {
      std::vector<AnimationFrame> frameList = { { 5, .25 }, {6, .25} };
      AnimationSequence sequence(2, frameList, false);

      sequence.OnComplete = [&]()  -> void
      {
        GameObject& parent = GameObjectManagerFindObject(parentName);
        std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>((parent.Get(Component::ctTransform)));
        std::shared_ptr<Animation> pAnimation = std::dynamic_pointer_cast<Animation>(UI.Get(Component::ctAnimation));
        if (healthCurr > 30)
        {
          std::vector<AnimationFrame> frameList = { { 0, 0.1 }, {1, 0.1}, { 2, 0.1 }, {3, 0.1}, {4, 0.1} };
          AnimationSequence sequence(5, frameList, true);
          pAnimation->PlaySequence(sequence);
        }
        else if (healthCurr <= 30 && healthCurr > 0)
        {
          std::vector<AnimationFrame> frameList = { { 10, 0.1 }, {11, 0.1}, { 12, 0.1 }, {13, 0.1}, {14, 0.1} };
          AnimationSequence sequence(5, frameList, true);
          pAnimation->PlaySequence(sequence);
        }
        else if (healthCurr <= 0)
        {
          std::shared_ptr<Sprite> pSprite = std::dynamic_pointer_cast<Sprite>((UI.Get(Component::ctSprite)));
          pSprite->SetMesh("UImesh1x1");
          pSprite->SetTexture(/*deadIcon.substr(15,deadIcon.size() - 15)*/deadIcon, deadIcon, true, pSprite->Parent().GetComponent(Transform)->GetMatrix());
          pSprite->SetUVOffset(glm::vec2(0, 0));
          pAnimation->SetFrameCount(1);
          pAnimation->SetFrameIndex(0);
          //UI.RemoveComponent(Component::ctBehavior);
          //UI.RemoveComponent(Component::ctAnimation);
          //ptransform->SetTranslation(glm::vec3(ptransform->GetTranslation().x - 1.0f, ptransform->GetTranslation().y, ptransform->GetTranslation().z));
        }
      };
      pAnimation->PlaySequence(sequence);
  }
    prevHealth = healthCurr;
}