//------------------------------------------------------------------------------
//
// File Name:	DebugObject.h
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once
#include "GameObject.h"

void DebugVelocity(GameObject& parent);

void DebugCollider(GameObject& parent);

void DebugCollider(std::string parent);

void DebugVelocity(std::string parent);

void ToggleAllVelocityDebugObjects(std::string objectName);

void ToggleVelocityDebugObject(std::string objectName);

void ToggleAllColliderDebugObjects(std::string objectName);

void ToggleColliderDebugObject(std::string objectName);

void CreateDebugDraw(std::string objectName);

void ToggleDebugObjects(std::string objectName);

void ToggleAllDebugObjects();

void DestroyDebugObjects(std::string objectName);

void DestroyAllDebugObjects();

void DestroyAllColliderDebugObjects();

void DestroyColliderDebugObject(std::string objectName);

void DestroyAllVelocityDebugObjects();

void DestroyVelocityDebugObject(std::string objectName);