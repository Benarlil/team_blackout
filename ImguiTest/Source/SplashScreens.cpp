//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	SplashScreens.cpp                                                 //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// The pre-menu Splash Screens                                                  //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "stdafx.h"
#include "SplashScreens.h"
#include "DebugLog.h"
#include "GameObjectManager.h"
#include "Sprite.h"
#include "Transform.h"
#include "Texture.h"
#include "MusicManager.h"

// create the DigiPen logo game object
static void CreateDigiLogo()
{
  GameObject& gO = GameObjectManagerCreateObject("DigiPenLogo");
  glm::vec3 vecc(0.0f, 0.0f, 0.0f);

  Transform transform = Transform(vecc.x, vecc.y, 0.0f);
  transform.SetScale(glm::vec3(2.0f, 2.0f, 0.0f));

  gO.Add(std::make_shared<Transform>(transform));

  gO.Add(std::make_shared<Sprite>("UImesh1x1", "DigiPenLogo", "Assets/UI/UI_MainMenu_Intro_1.PNG"));
}

// create our team logo
static void CreateBlackoutLogo()
{
  GameObject& gO = GameObjectManagerCreateObject("BlackoutLogo");
  glm::vec3 vecc(0.0f, 0.0f, 0.0f);

  Transform transform = Transform(vecc.x, vecc.y, 0.0f);
  transform.SetScale(glm::vec3(2.0f, 2.0f, 0.0f));

  gO.Add(std::make_shared<Transform>(transform));

  gO.Add(std::make_shared<Sprite>("UImesh1x1", "BlackoutLogo", "Assets/UI/UI_MainMenu_Intro_2.PNG"));

  GameObjectManagerFindObject("BlackoutLogo").GetComponent(Sprite)->SetAlpha(0.0f);
}

namespace Levels
{
  // load in the song 
  void SplashScreens::Load()
  {
    DebugMessage("Gamestate SplashScreens Loading");
    MusicManagerSetSong("Menu"); // load the menu music
  }

  // start playing the song and make the game objects
  void SplashScreens::Init()
  {
    DebugMessage("Gamestate SplashScreens Initializing");

    MusicManagerPlaySong(); // Play the menu music

    CreateDigiLogo(); // create the digipen logo
    CreateBlackoutLogo(); // create the team logo

  }

  // go through the splash screens
  void SplashScreens::Update(float dt)
  {

    static bool DigiPenLogo = true; // digipen logo is the one being shown
    static bool BlackoutLogo = false; // show the blackout logo after

    static float DigiTimer = 0.0f; // set the digipen timer to 0
    static float BlackoutTimer = 0.0f; // set the blackout timer to 0

    // if the digipen logo is the one being shown
    if (DigiPenLogo)
    {
      DigiTimer += dt; // increase the timer by dt

      // once 4 seconds has passed
      if (DigiTimer >= 4.0f)
      {
        GameObjectManagerFindObject("DigiPenLogo").GetComponent(Sprite)->SetAlpha(0.0f); // hide the DigiPen logo
        GameObjectManagerFindObject("BlackoutLogo").GetComponent(Sprite)->SetAlpha(1.0f); // show the team logo
        DigiPenLogo = false; // digipen logo is no longer going
        BlackoutLogo = true; // team logo is now showing
      }
    }

    // if the team logo is showing
    if (BlackoutLogo)
    {
      BlackoutTimer += dt; // increment the timer by dt

      // once four seconds has passed
      if (BlackoutTimer >= 4.0f)
      {
        Engine::GameStateManager::Go(Engine::GAMESTATES::MainMenu); // go to the menu
      }
    }


  }

  void SplashScreens::Unload()
  {
    DebugMessage("Gamestate SplashScreens Unloading");
    
  }

  // remove the splash objects
  void SplashScreens::Shutdown()
  {
    DebugMessage("Gamestate SplashScreens Shutting down");

    GameObjectManagerShutdown();
  }

  void SplashScreens::OnAction(sf::Event& event, float dt)
  {
  }
}