//------------------------------------------------------------------------------
//
// File Name:	Level1.h
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

//////////////////////////////////////////////////////////
#ifndef STUB_H
#define STUB_H
//////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameState.h"
#include "DebugLog.h"
#include "Input.h"
#include "ControllerInput.h"
//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------

class Texture;

namespace Levels
{
  class Color : public Engine::GameState
  {
  public:
    void Load()  override;
    void Init()  override;
    void Update(float dt)  override;
    void Unload()  override;
    void Shutdown()  override;
    void OnAction(sf::Event& event, float dt) override;
  private:
    //std::vector<Texture&> level_textures;
    ControllerSystem* cInput;
  };


  class Quit : public Engine::GameState
  {
  public:
    void Load() { return; }
    void Init() override;
    void Update(float dt) { return; }
    void Unload() { DebugMessage("Shutting down"); }
    void Shutdown() { return; }
    //void OnAction(sf::Event& event, float dt);
  };
}
#endif //STUB_H
