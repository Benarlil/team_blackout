#pragma once
#include "Behavior.h"
#include "GameObject.h"


#define DEFAULT_HANDLE_LENGTH 3.0f;
class BehaviorWeapon : public BehaviorBase
{



	glm::vec3 _PrevDirection;

	float _MaxAccel;
	float _MaxVel;
	float _HandleLength;
	int _StateCurr;
	int _StateNext;
  bool _GoesOnLeft;

public:

	enum States
	{
		bwInvalid,
		bwIdle,
		bwHeld,
		bwThrown,
	};

	BehaviorWeapon();
	BehaviorWeapon(float length);
	BehaviorWeapon(float length, bool goesOnLeft, GameObject& Owner, GameObject* parent);
	BehaviorWeapon(BehaviorWeapon& original);
	~BehaviorWeapon();


	float GetHandleLength();
	void SetHandleLength(float length);
	void SetStateNext(States state);
	int GetStateCurr();
	glm::vec3 GetPrevDirection();
	void SetHandler(GameObject& object);
	void WeaponCollide(GameObject& object);
	void Init();
	void Update(float dt);
	void Exit();
}; 