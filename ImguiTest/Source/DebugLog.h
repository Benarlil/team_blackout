//------------------------------------------------------------------------------
//
// File Name:	DebugLog.h
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
//
// Copyright © 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#pragma once
#include "stdafx.h"

// Initialize the Debug logging module.
void DebugInit();

// Output a message to the debug file.
void DebugMessage(std::string formatString);

// Output a message to the logging file. This is overloaded to take a second string as a "variable"
void DebugMessage(std::string message, std::string variable);

// Output a message to the logging file. This is overloaded to take a float as a "variable"
void DebugMessage(std::string message, float number);

// Shutdown the debug module. (close the file)
void DebugShutdown();



