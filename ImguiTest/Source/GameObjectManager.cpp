//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	GameObjectManager.cpp                                             //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "stdafx.h"
#include "GameObjectManager.h"
#include "DebugLog.h"
#include "Collider.h"
#include "Behavior.h"

static std::vector<GameObjectPtr> GameObjectList; // The vector to keep track of all active game objects
static std::vector<GameObjectPtr> ObjectArchetypeList; // The vector to keep track of archetypes


static bool checkDeadObject(GameObjectPtr& check)
{
	return (*check).IsToDestroy(); // check from the game object if it will be destroyed
}

// Finds a Game Object in the active list when given a name and returns it
GameObject& GameObjectManagerFindObject(std::string objectName)
{
  std::vector<GameObjectPtr>::iterator it = GameObjectList.begin();
  
  while (it != GameObjectList.end())
  {
    // If the game object name matches the one being searched for
    if ((**it).IsNamed(objectName))
    {
	    DebugMessage("Object Found: ", objectName); //Print that the object was found and can be used

	    return **it; // return the object
    }
    ++it;
  }

  DebugMessage("No Object Named", objectName); //Print that the object was not found;

  GameObjectList.emplace_back(std::make_unique<GameObject>()); // Create a dummy object that will be destroyed next loop

  return *GameObjectList.back(); // return NULL when no object is found
}



// Update every active game object and delete any set to be destroyed
void GameObjectManagerUpdate(float dt)
{
	//DebugMessage("Updating Game Objects"); // Send debug message

	std::vector<GameObjectPtr>::iterator it = GameObjectList.begin(); // Set the iterator for the list

  // go through the list
	while (it != GameObjectList.end())
	{
    // skip updating the object if it is a child
    if((*it)->isChild() == false)
		  (**it).Update(dt); // update the current object

		++it; // continue through the list
	}

  // Move all objects set to be destroyed to the end of the list
	std::vector<GameObjectPtr>::iterator deleted = std::remove_if(GameObjectList.begin(), GameObjectList.end(), checkDeadObject);

	GameObjectList.erase(deleted, GameObjectList.end()); // erase all the objects that were set to be so

  GameObjectManagerCheckCollisions();
}

void GameObjectManagerCheckCollisions()
{
  std::vector<GameObjectPtr>::iterator it = GameObjectList.begin(); // Set the iterator for the list

  // go through the list
  while (it != GameObjectList.end())
  {
    std::vector<GameObjectPtr>::iterator checkAhead = it; // Set the iterator for the list

    ++checkAhead;

    // check every object after the current one for collision
    while (checkAhead != GameObjectList.end())
    {

      /* If current objects have parents */
      if ((*checkAhead)->isChild())
      {
        // if the objects have a parent/child relationship, 
        if ((*checkAhead)->GetParent().IsNamed((*it)->GetName()))
        {
          ++checkAhead;
          continue;
        }
      }

      else if ((*it)->isChild())
      {
        if ((*it)->GetParent().IsNamed((*checkAhead)->GetName()))
        {
          ++checkAhead;
          continue;
        }
      }

      if ((*it)->GetComponent(Collider) && (*checkAhead)->GetComponent(Collider))
      {
        IsColliding(*(*it)->GetComponent(Collider), *(*checkAhead)->GetComponent(Collider)); // check the collision between these two objects
      }

      ++checkAhead; // continue through the list
    }
    ++it; // continue through the list
  }
}

// Here for future use
void GameObjectManagerShutdown()
{
  GameObjectList.clear(); // clear the game object list
}

GameObject& GameObjectManagerCreateObject(std::string ObjectName)
{
  GameObject& check = GameObjectManagerFindObject(ObjectName);

  if (check.IsNamed(ObjectName))
  {
    DebugMessage("That object already exists! Rename the object you were attempting to create!");

    return GameObject::GetDummy();
  }

  GameObjectList.push_back(std::make_unique<GameObject>(ObjectName)); // Create the object and add it to the object list

  return *GameObjectList.back(); // return the newly created object from the back of the list
}

void GameObjectManagerAddObject(GameObject& object)
{

  GameObjectList.push_back(std::make_unique<GameObject>(object));

}

typedef BehaviorBase Behavior;

// Given a behavior type and a list to populate, returns the names of objects with the desired behavior type
void GameObjectManagerPopulateByBehaviorName(std::vector<std::string>& toPopulate, BehaviorTypes behavior)
{
  //DebugMessage("Finding Game Objects by Behavior"); // Send debug message

  std::vector<GameObjectPtr>::iterator it = GameObjectList.begin(); // Set the iterator for the list

  // go through the list
  while (it != GameObjectList.end())
  {
    if ((*it)->GetName() != "DUMMY") {
      auto& b = (*it)->GetComponent(Behavior);
      if (b) {
        // if the component of this object matches the type being asked for
        if (b->GetType() == behavior)
        {
          toPopulate.push_back((*it)->GetName()); // add the name of the game object to the vector of names
        }
      }
    }

    ++it; // continue through the list
  }
}

// Given a behavior type, creates and returns a list of game object names with the desired object names
std::vector<std::string> GameObjectManagerPopulateByBehaviorName(BehaviorTypes behavior)
{
  std::vector<GameObjectPtr>::iterator it = GameObjectList.begin(); // Set the iterator for the list

  std::vector<std::string> objectsWithBehavior; // make the list of objects to return

  // go through the list
  while (it != GameObjectList.end())
  {
    if ((*it)->GetComponent(Behavior))
    {
      // if the component of this object matches the type being asked for
      if ((*it)->GetComponent(Behavior)->GetType() == behavior)
      {
        objectsWithBehavior.push_back((*it)->GetName()); // add the name of the game object to the vector of names
      }
    }

    ++it; // continue through the list
  }

  return objectsWithBehavior; // return the new list
}


/////////////////////////////// Archetype Functions //////////////////////////////////////

void GameObjectManagerArchetypeInitialize()
{
  ObjectArchetypeList.push_back(std::make_unique<GameObject>("Default"));
}

GameObject& GameObjectManagerCreateArchetype(std::string name)
{
  ObjectArchetypeList.push_back(std::make_unique<GameObject>(name)); // add the new archetype to the list

  return *ObjectArchetypeList.back(); // return the archettype for editing
}

void GameObjectManagerAddArchetype(GameObject& archetype)
{
  ObjectArchetypeList.push_back(std::make_unique<GameObject>(archetype)); // add the new archetype to the list
}

GameObject& GameObjectManagerFindArchetype(std::string archetypeName)
{
  std::vector<GameObjectPtr>::iterator it = ObjectArchetypeList.begin();

  while (it != ObjectArchetypeList.end())
  {
    // If the game object name matches the one being searched for
    if ((**it).IsNamed(archetypeName))
    {
      DebugMessage("Archetype Found: ", archetypeName); //Print that the object was found and can be used

      return **it;
    }
    ++it;
  }

  DebugMessage("No Archetype Named", archetypeName); //Print that the object was not found;

  return *ObjectArchetypeList.front();
}

GameObject& GameObjectManagerObjectFromArchetype(std::string archetypeName, std::string objectName)
{
  GameObject& archetypeToObject = GameObjectManagerFindArchetype(archetypeName);

  GameObject toAdd = GameObject(archetypeToObject);

  toAdd.Rename(objectName);

  GameObjectList.push_back(std::make_unique<GameObject>(toAdd));

  return *GameObjectList.back();
}

// Creates a Game Object archetype from an object and adds it to the archetype list
void CreateArchetypeFromObject(GameObject& object)
{
  // Check to ensure the archetype does not already exist before adding it again
  GameObject& check = GameObjectManagerFindArchetype(object.GetName());

  // if there is no archetype with that name already
  if (check.IsNamed("Default"))
  {
    DebugMessage("Archetpye added to list: ", check.GetName());
    GameObjectManagerAddArchetype(object); // create the new archetype and add it to the archetype list
  }

  else
  {
    DebugMessage("Archetype already exists!");
  } 
}

// Creates a copy of an already existing object and RETURNS IT, not adds it to the list
GameObject& CloneObject(GameObject& object, std::string newName)
{
  GameObjectList.push_back(std::make_unique<GameObject>(object)); // clones the object and adds it to the list

  GameObjectList.back()->Rename(newName); // rename the object

  return *GameObjectList.back(); // return the newly created object from the back of the list
}

// Creates a copy of an already existing object and RETURNS IT, not adds it to the list
GameObject& CloneObject(const char * objectName, std::string newName)
{

  // find the game object, make a clone of it, and add it to the list
  GameObjectList.push_back(std::make_unique<GameObject>(GameObjectManagerFindObject(objectName)));

  GameObjectList.back()->Rename(newName); // rename the object

  return *GameObjectList.back(); // return the newly created object from the back of the list
}