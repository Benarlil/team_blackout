#include "Spy.h"



Spy::Spy() 
{
	_Target = NULL;
	_RoundCount = 0;

	_TotalDamageTaken = 0;
	_TotalDamageDealt = 0;
	_TotalHealing = 0;
	 _TotalAttacks = 0;
	 _TotalTimesAttacked = 0;

	 _TotalPropCollisions = 0;
	 _TotalPlayerCollisions = 0;
	 _TotalCrabCollisions = 0;
}

Spy::Spy(GameObject& target)
{
	_Target = &target;
	_RoundCount = 0;
	_Name = target.GetName();

	_TotalDamageTaken = 0;
	_TotalDamageDealt = 0;
	_TotalHealing = 0;
	_TotalAttacks = 0;
	_TotalTimesAttacked = 0;

	_TotalPropCollisions = 0;
	_TotalPlayerCollisions = 0;
	_TotalCrabCollisions = 0;
}


Spy::~Spy()
{
}

void Spy::IncrementRound()
{
	_RoundCount++;
}

void Spy::AquireTarget(GameObject& target)
{
	_Target = &target;
	_Name = target.GetName();
}
GameObject* Spy::ReturnTarget()
{
	return _Target;
}
void Spy::GatherData(RoundData& report)
{
	_RoundData.push_back(report);
	IncrementRound();

	_TotalDamageTaken += report.GetDamageTaken();
	_TotalDamageDealt += report.GetDamageDealt();
	_TotalHealing += report.GetHealing();
	_TotalAttacks += report.GetAttackCount();
	_TotalTimesAttacked += report.GetTimesAttacked();

	_TotalPropCollisions += report.GetPropCollisions();
	_TotalPlayerCollisions += report.GetPlayerCollisions();
	_TotalCrabCollisions += report.GetCrabCollisions();
}