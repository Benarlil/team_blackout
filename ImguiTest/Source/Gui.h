#pragma once
//------------------------------------------------------------------------------
//
// File Name:	Gui.cpp
// Author(s):	Jack Klein (jack.klein)
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "GameObject.h"
#include "Serialize.h"

/* Set up  ImGui window and binding and magic things */
void ImGuiInit(void);

void ImGuiUpdate(bool imGuiFlag, sf::Clock clock, Serializer & serializer);

void ImGuiShutdown(void);

/* Process ImGui window event */
void ImGuiProcessEvent(sf::Event event);

/* Creates an ImGui window for base debug behavior */
static void CreateDebugGui(Serializer & serializer, sf::Clock clock);


