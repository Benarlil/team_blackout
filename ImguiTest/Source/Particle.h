//------------------------------------------------------------------------------
//
// File Name:	Particle.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------
#pragma once

#include "Emitter.h"

//---------------------------FORWARD DECLARATIONS-------------------------------
class Mesh;
class Texture;
class Transform;
class ParticleBase;
using ParticleIndex = unsigned int;
//------------------------------------------------------------------------------

//-----------------------------Particle classes---------------------------------

class Particle {
public:
  Particle(ParticleBase& prt_base, glm::vec3 displacement = { 0.f, 0.f, 0.f }, float lifetime = 1.f);
  Particle(ParticleBase& new_base, const Particle& prt);
  ~Particle();

  float Update(float dt);

  void Lifetime(float time);
  void Offset(glm::vec3 new_offset);
  void Scale(glm::vec3 scale) = delete;
  void Rotate(float radians) = delete;
  void Color(glm::vec3 color);

  float Lifetime() const;
  glm::vec3 Offset() const;
  glm::vec3 Scale() const = delete;
  float Rotate() const = delete;
private:
  ParticleBase& base;
  bool visible = true;
  bool moved = true;
  GLuint render_id;
  ParticleIndex id;
  float lifetime;
  glm::vec3 offset;
  glm::vec4 color = { 1.f, 1.f, 1.f, 1.f };
};

//-----------------------------ParticleBase class-------------------------------

using ParticleContainer = std::vector<Particle>;

class ParticleBase {
public:
  ParticleBase(EmitterTypes type, 
               std::string prt_mesh, 
               std::string prt_texture, 
               const Transform& emitter_transform,
               unsigned int max_particles, 
               float default_time = -1.f,
               bool animated = false);
  ~ParticleBase();

  virtual void Update(float dt) {};
  virtual void Emit(bool looping = false, float time = -1.f) {};
  virtual void Stop() {};

  virtual ParticleBase& UpCast() { return *this; };
  template<class Derived>
  Derived& DownCast()
  {
    return static_cast<Derived&>(*this);
  }
  template<class Derived>
  const Derived& DownCast() const
  {
    return static_cast<const Derived&>(*this);
  }

  EmitterTypes Type();

  void SetDefaultLifetime(float prt_lifetime);
protected:
  /* Private constructor used in Emitter when an invalid ParticleBase is needed */
  ParticleBase(const Transform& emitter_transform);

  Mesh& mesh;
  const Texture& texture;
  const Transform& emtr_transform;
  bool animated;
  bool emitting = false;
  bool looping;
  unsigned int max_particles;
  float default_lifetime;
  EmitterTypes type;
  ParticleContainer particles;
  friend class Particle;
  friend const ParticleBase& Emitter::GetIndexedParticleBase(ParticleIndex index) const;
};

//-----------------------ParticleBase derived classes---------------------------

class ParticleBaseRadius : public ParticleBase {
public:
  ParticleBaseRadius(std::string prt_mesh, 
                     std::string prt_texture, 
                     const Transform& emitter_transform, 
                     unsigned int max_particles,
                     float emission_radius, 
                     float default_time, 
                     bool animated = false);
  ParticleBaseRadius(const ParticleBaseRadius& base);
  void Update(float dt) override;
  void Emit(bool looping = false, float time = -1.f) override;
  void Stop() override;
  ParticleBase& UpCast() override;
private:
  float set_lifetime;
  float radius;
};
#define ToBaseRadius(base) Down�ast<ParticleBase, ParticleBaseRadius, EmitterTypes>(base, emRadius)