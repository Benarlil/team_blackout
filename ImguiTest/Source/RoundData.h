#pragma once
#include "stdafx.h"
#include "Component.h"

class Spy;
class RoundData : public Component
{
	Spy *_Owner;

	int _DamageDealt;
	int _DamageTaken;
	int _Healing;
	int _Rotations;

	int _AttackCount;
	int _TimesAttacked;

	int _PropCollisions;
	int _PlayerCollisions;
	int _CrabCollisions;
	int _HazardCollisions;

public:
	RoundData();
	RoundData(Spy& owner);
	~RoundData();
	Spy* GetOwner();
	void SetOwner(Spy& owner);
	void TookDamage(int damage);
	void DealtDamage(int damage);
	void Healed(int heal);
	void Rotated();
	void CollidedProp();
	void HitCrab();
	void RoundData::CollidedHazard();

	int GetDamageDealt();
	int GetDamageTaken();
	int GetHealing();
	int GetRotations();
	int GetAttackCount();
	int GetTimesAttacked();
	int GetPropCollisions();
	int GetPlayerCollisions();
	int GetCrabCollisions();
	int GetHazardCollisions();


};

