//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	MainMenu.cpp                                                      //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// The main menu for the game                                                   //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//


//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "MainMenu.h"
#include "DebugLog.h"
#include "GameObjectManager.h"
#include "Sprite.h"
#include "Transform.h"
#include "BehaviorCursor.h"
#include "BehaviorButton.h"
#include "ColliderCircle.h"
#include "ColliderBox.h"
#include "Texture.h"
#include "MusicManager.h"
#include "Text.h"
#include "Font.h"

//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------
extern sf::Event gEvent;

static void CreateCursor()
{
  GameObject& gO = GameObjectManagerCreateObject("CursorObject");
  glm::vec3 vecc(0.0f, 0.0f, 0.0f);

  Transform transform = Transform(vecc.x, vecc.y, 0.0f);
  transform.SetScale(glm::vec3(0.15f, 0.20f, 0.0f));

  gO.Add(std::make_shared<Transform>(transform));

  gO.Add(std::make_shared<Sprite>("UImesh1x1", "zcursor", "Assets/UI/UI_Cursor.PNG"));

  gO.Add(std::make_shared<ColliderCircle>(0.f, glm::vec2(0.03, -0.05), false));

  gO.Add(std::make_shared<BehaviorCursor>());

}

static void CreateQuitButton()
{

  GameObject& gO = GameObjectManagerCreateObject("QuitButton");
  glm::vec3 vecc(-0.45f, -0.25f, 0.0f);

  Transform transform = Transform(vecc.x, vecc.y, 0);
  transform.SetScale(glm::vec3(0.6f, 0.4f, 0.0f));

  gO.Add(std::make_shared<Transform>(transform));

  //gO.Add(std::make_shared<ColliderCircle>(transform.GetScale().x/3));

  gO.Add(std::make_shared<ColliderBox>(transform.GetScale().x, transform.GetScale().y));

  gO.Add(std::make_shared<BehaviorButton>());

  gO.Add(std::make_shared<Sprite>("UImesh1x1", "quitbuttondefault", "Assets/UI/MainMenu/UI_MainMenu_Button_Exit.PNG"));

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setDefaultTexture("quitbuttondefault", "Assets/UI/MainMenu/UI_MainMenu_Button_Exit.PNG");

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setSelectedTexture("quitbuttonselect", "Assets/UI/MainMenu/UI_MainMenu_ButtonSelect_Exit.PNG");

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setOnClick(std::bind(Engine::GameStateManager::Go, Engine::GAMESTATES::Quit));

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setColliderHandler();
}

static void CreateStartButton()
{
  GameObject& gO = GameObjectManagerCreateObject("StartButton");
  glm::vec3 vecc(-0.45f, 0.2f, 0.0f);

  Transform transform = Transform(vecc.x, vecc.y, 0);
  transform.SetScale(glm::vec3(0.6f, 0.4f, 0.0f));

  gO.Add(std::make_shared<Transform>(transform));

  gO.Add(std::make_shared<Sprite>("UImesh1x1", "startbuttondefault", "Assets/UI/MainMenu/UI_MainMenu_Button_Fight.PNG"));

  gO.Add(std::make_shared<ColliderBox>(transform.GetScale().x, transform.GetScale().y));

  gO.Add(std::make_shared<BehaviorButton>());

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setDefaultTexture("startbuttondefault", "Assets/UI/MainMenu/UI_MainMenu_Button_Fight.PNG");

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setSelectedTexture("startbuttonselect", "Assets/UI/MainMenu/UI_MainMenu_ButtonSelect_Fight.PNG");

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setOnClick(std::bind(Engine::GameStateManager::Go, Engine::GAMESTATES::Color));

  gO.RawGetComponent<BehaviorButton>(Component::ctBehavior)->setColliderHandler();
}

static void CreateBackground()
{
  GameObject& gO = GameObjectManagerCreateObject("Background");
  glm::vec3 vecc(0.0f, 0.0f, 0.0f);

  Transform transform = Transform(vecc.x, vecc.y, 0);
  transform.SetScale(glm::vec3(2.0f, 2.0f, 0.0f));

  gO.Add(std::make_shared<Transform>(transform));

  gO.Add(std::make_shared<Sprite>("UImesh1x1", "background", "Assets/UI/MainMenu/UI_MainMenu_Background.PNG"));

}
static void ToggleControllerTextOn(std::string contollerID)
{
  Sleep(60);
  if (atoi(contollerID.c_str()) <= 4)
  {

    GameObject& gO = GameObjectManagerFindObject("zP" + contollerID + "ConnectedText");
    if (gO.GetName() != "DUMMY")
    {
      auto& t = gO.RawGetComponent<Text>(Component::ctSprite);
      t->SetAlpha(1.0f);

    }
  }
}

static void ToggleControllerTextOff(std::string contollerID)
{
  Sleep(60);
  if (atoi(contollerID.c_str()) <= 4)
  {

    GameObject& gO = GameObjectManagerFindObject("zP" + contollerID + "ConnectedText");
    if (gO.GetName() != "DUMMY")
    {
      auto& t = gO.RawGetComponent<Text>(Component::ctSprite);
      t->SetAlpha(0.0f);
    }
  }
}
static int controllerCount = 0;
static void CreateControllerTexts()
{
  static Font font(".\\Fonts\\CrimsonText-Regular.ttf", 60);


  /* CONTROLLER 1 */
  GameObject& gO = GameObjectManagerCreateObject("zP1ConnectedText");
  glm::vec3 vecc1(-0.7f, -0.8f, 0.0f);

  Transform transform1 = Transform(vecc1.x, vecc1.y, 0);
  transform1.SetScale(glm::vec3(0.06f, 0.06f, 0.00f));

  gO.Add(std::make_shared<Transform>(transform1));
  gO.Add(std::make_shared<Text>(font, "UImesh1x1"));
  auto& t1 = gO.RawGetComponent<Text>(Component::ctSprite);
  t1->SetString("p1 connected!", true);
  t1->SetColor(glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));

  /* CONTROLLER 2*/
  GameObject& gO2 = GameObjectManagerCreateObject("zP2ConnectedText");
  glm::vec3 vecc2(-0.2f, -0.8f, 0.0f);

  Transform transform2 = Transform(vecc2.x, vecc2.y, 0);
  transform2.SetScale(glm::vec3(0.06f, 0.06f, 0.00f));

  gO2.Add(std::make_shared<Transform>(transform2));
  gO2.Add(std::make_shared<Text>(font, "UImesh1x1"));
  auto& t2 = gO2.RawGetComponent<Text>(Component::ctSprite);
  t2->SetString("p2 connected!", true);
  t2->SetColor(glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));


  /* CONTROLLER COUNT */
  GameObject& gO3 = GameObjectManagerCreateObject("zControllerCount");
  glm::vec3 vecc3(-0.45f, -0.67f, 0.0f);

  Transform transform3 = Transform(vecc3.x, vecc3.y, 0);
  transform3.SetScale(glm::vec3(0.06f, 0.06f, 0.00f));

  gO3.Add(std::make_shared<Transform>(transform3));
  gO3.Add(std::make_shared<Text>(font, "UImesh1x1"));
  auto& t3 = gO3.RawGetComponent<Text>(Component::ctSprite);
  t3->SetString("CONTROLLER COUNT:", true);
  t3->SetColor(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

}

static void UpdateControllerCount(std::string controllers)
{
  if (atoi(controllers.c_str()) <= 4)
  {

    GameObject& gO = GameObjectManagerFindObject("zControllerCount");
    if (gO.GetName() != "DUMMY")
    {
      auto& t = gO.RawGetComponent<Text>(Component::ctSprite);
      std::string currentString = t->GetString();
      currentString += controllers;
      t->SetString(currentString);
    }
  }
}


namespace Levels
{
  void MainMenu::Load()
  {
    DebugMessage("Gamestate MainMenu Loading");
    //MusicManagerSetSong("Menu");
  }

  void MainMenu::Init()
  {
    DebugMessage("Gamestate MainMenu Initializing");
    
   // MusicManagerPlaySong();

    CreateBackground();
    CreateQuitButton();
    CreateStartButton();
    CreateCursor();
    CreateControllerTexts();
  }


  void MainMenu::Update(float dt)
  {
    //static int prevCount = 0;
    if (gEvent.type == sf::Event::EventType::JoystickConnected)
    {
      char buffer[sizeof(gEvent.joystickConnect.joystickId)];
      std::string controllerID(itoa(gEvent.joystickConnect.joystickId + 1, buffer, sizeof(gEvent.joystickConnect.joystickId)));
      ToggleControllerTextOn(controllerID);
      controllerCount++;
    }
    if (gEvent.type == sf::Event::EventType::JoystickDisconnected)
    {
      char buffer[sizeof(gEvent.joystickConnect.joystickId)];
      std::string controllerID(itoa(gEvent.joystickConnect.joystickId + 1, buffer, sizeof(gEvent.joystickConnect.joystickId)));
      ToggleControllerTextOff(controllerID);
      controllerCount--;
    }
 /*   if (controllerCount != prevCount)
    {
      char buffer[4];
      std::string controllerCountS(itoa(controllerCount, buffer, 4));
      UpdateControllerCount(controllerCountS);
    }
    prevCount = controllerCount;*/
  }

  void MainMenu::Unload()
  {
    DebugMessage("Gamestate MainMenu Unloading");
    MusicManagerStopSong();
  }

  void MainMenu::Shutdown()
  {
    DebugMessage("Gamestate MainMenu Shutting down");

    GameObjectManagerShutdown();
  }

  void MainMenu::OnAction(sf::Event& event, float dt)
  {
  }
}