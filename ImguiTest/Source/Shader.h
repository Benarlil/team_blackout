//------------------------------------------------------------------------------
//
// File Name:	Shader.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

class Shader {
public:
  Shader() {}
  Shader(std::string shader_name, const char* vertexShaderPath, const char* fragmentShaderPath);
  Shader& operator=(const Shader& s);
  ~Shader();

  void Use(int state = -1);

  GLuint GetProgramID() { return program; }
  std::string &GetName() { return name; }
private:
  std::string name;
  GLuint program;

  std::string ReadShaderFile(const char* filename);
};

typedef std::unique_ptr<Shader> ShaderPtr;