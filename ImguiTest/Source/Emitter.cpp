//------------------------------------------------------------------------------
//
// File Name:	Emitter.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Emitter.h"
#include "GameObject.h"
#include "DebugLog.h"
#include "Particle.h"

Emitter::Emitter(GameObject& parent) :
  Component(ctEmitter),
  prnt_transform(parent.GetComponent(Transform))
{
  Parent(parent);
}

Emitter::Emitter(GameObject& parent, const Emitter &em) :
  Component(ctEmitter),
  prnt_transform(parent.GetComponent(Transform))
{
  for (auto& ptr : em.particles) {
    switch (ptr->Type()) {
    case emRadius: {
      particles.emplace_back(std::make_unique<ParticleBaseRadius>(ptr->DownCast<ParticleBaseRadius>()));
    } break;
    }
  }
}

Emitter::Emitter(GameObject& parent, Emitter &&em) :
  Component(ctEmitter),
  prnt_transform(parent.GetComponent(Transform))
{
  for (auto& ptr : em.particles)
    particles.emplace_back(std::move(ptr));
}

void Emitter::Update(float dt)
{
  for (auto& prt_b : particles) {
    switch (prt_b->Type()) {
    case emRadius: {
      prt_b->DownCast<ParticleBaseRadius>().Update(dt);
    } break;
    }
  }
}

ParticleIndex Emitter::EmitParticlesInRadius(std::string mesh_name, 
                                             std::string texture_name, 
                                             unsigned int max_particles, 
                                             float radius, 
                                             bool loop, 
                                             float time)
{
  particles.emplace_back(std::make_unique<ParticleBaseRadius>(mesh_name, texture_name, *prnt_transform, max_particles, radius, time));
  ParticleBaseRadius& prt_b = particles.back()->DownCast<ParticleBaseRadius>();
  prt_b.Emit(loop);
  return particles.size() - 1;
}

ParticleIndex Emitter::EmitParticlesInLine(std::string mesh_name,
                                  std::string texture_name,
                                  unsigned int max_particles,
                                  glm::vec3 direction,
                                  bool loop,
                                  float time)
{
  return 0;
}

const ParticleBase& Emitter::GetIndexedParticleBase(ParticleIndex index) const
{
  try
  {
    return *particles.at(index);
  }
  catch (std::out_of_range excp)
  {
    auto err_str = excp.what();
    DebugMessage(err_str);
    std::cout << err_str;
    return ParticleBase(*prnt_transform);
  }
}
