//------------------------------------------------------------------------------
//
// File Name:	Engine.h
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

//////////////////////////////////////////////////////////
#ifndef BUTTON_H
#define BUTTON_H
//////////////////////////////////////////////////////////
#include "GameObject.h"
//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------
  void ButtonInit(char * objectName);

  void ButtonUpdate(sf::Event& event, char * objectName);

  void MouseHover(GameObject& button, sf::Vector2i& mousePos);

  void ButtonShutdown();



#endif //ENGINE_H