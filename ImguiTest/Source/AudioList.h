//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	AudioList.h                                                       //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// Desc: The manager for the list of audio files used in the game.              //
// Keeps track of all of them, provides them to components, and                 //
// makes sure we don't go over the SFML audio limit (256 files).                //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include <string>

void AudioListAdd(std::string);

void AudioListRemove(std::string);

sf::SoundBuffer& AudioListFind(std::string);

void AddAllSounds();