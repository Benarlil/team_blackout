
#include "BehaviorWeapon.h"
#include "BehaviorEnemy.h"
#include "BehaviorPlayer.h"
#include "Component.h"
#include "Collider.h"
#include "Transform.h"
#include "Physics.h"
#include "RoundData.h"

static void* CollisionHandler(BehaviorWeapon& bp, GameObject& object);


static float maxvel = 7.0f;
	BehaviorWeapon::BehaviorWeapon() : BehaviorBase(btWeapon)
	{
		_StateCurr = bwIdle;
		_StateNext = bwIdle;
		_HandleLength = DEFAULT_HANDLE_LENGTH;	
		_PrevDirection = { 0.0f, 0.0f, 0.0f };
    _GoesOnLeft = false;
	}

	BehaviorWeapon::BehaviorWeapon(float length) : BehaviorBase(btWeapon)
	{
		_StateCurr = bwIdle;
		_StateNext = bwIdle;
		_HandleLength = length;
		_PrevDirection = { 0.0f, 0.0f, 0.0f };
    _GoesOnLeft = false;
	}

	BehaviorWeapon::BehaviorWeapon(float length, bool goesOnLeft, GameObject& Owner,GameObject* parent) : BehaviorBase(btWeapon)
	{
		_StateCurr = bwHeld;
		_StateNext = bwHeld;
		_HandleLength = length;
    _GoesOnLeft = goesOnLeft;
		_PrevDirection = { 0.0f, 0.0f, 0.0f };

		if (parent)
		{
			Owner.AddParent(parent);
			
		}
	}

	BehaviorWeapon::BehaviorWeapon(BehaviorWeapon& original) : BehaviorBase(btWeapon)
	{
		_StateCurr = bwInvalid;
		_StateNext = original.GetStateCurr();
		_HandleLength = original.GetHandleLength();
		_PrevDirection = original.GetPrevDirection();
		_GoesOnLeft = false;
	}

	BehaviorWeapon::~BehaviorWeapon()
	{

	}

	float BehaviorWeapon::GetHandleLength()
	{
		return _HandleLength;

	}
	void BehaviorWeapon::SetHandleLength(float length)
	{
		_HandleLength = length;
	}

	void BehaviorWeapon::SetStateNext(States state)
	{
		_StateNext = state;
	}

	int BehaviorWeapon::GetStateCurr()
	{
		return _StateCurr;
	}

	glm::vec3 BehaviorWeapon::GetPrevDirection()
	{
		return _PrevDirection;
	}

	void BehaviorWeapon::SetHandler(GameObject& object)
	{
		if (Parent().GetComponent(Collider))
		{
			std::function<void*(void)> f = [this, &object]() { 
				CollisionHandler(*this, object);
				return nullptr; };
			Parent().GetComponent(Collider)->SetCollisionHandler(f);
		}
	}

	void BehaviorWeapon::WeaponCollide(GameObject& object)
	{
		std::shared_ptr<BehaviorBase> behavior = std::dynamic_pointer_cast<BehaviorBase>(object.Get(Component::ctBehavior));
		std::shared_ptr<RoundData> data = std::dynamic_pointer_cast<RoundData>(object.Get(Component::ctData));

		if (behavior->GetType() == btWeapon)
		{
			std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>((Parent().GetParent()).Get(Component::ctTransform));
			std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>((Parent().GetParent()).Get(Component::ctPhysics));

			ptransform->SetRotation(pphysics->GetOldRot());
			ptransform->SetTranslation(pphysics->GetOldPos());

			pphysics->SetRotationalVel(pphysics->GetRotationalVel() * -0.8f);
			pphysics->SetRotationalAccel(-pphysics->GetRotationalVel());

		}

		if (behavior->GetType() == btPlayer)
		{
			std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>((Parent().GetParent()).Get(Component::ctTransform));

			std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>((Parent().GetParent()).Get(Component::ctPhysics));

			ptransform->SetRotation(pphysics->GetOldRot());
			ptransform->SetTranslation(pphysics->GetOldPos());

			int force = pphysics->GetRotationalVel() * this->GetHandleLength();

			if (force < 0)
			{
				force *= -1;
			}

				if (data)
				{
					data->DealtDamage(force);
				}

			pphysics->SetRotationalVel(pphysics->GetRotationalVel() * -1.25f);
			//pphysics->AddRotationalAccel(pphysics->GetRotationalVel() * 1.8f);
		}
    if (behavior->GetType() == btProp)
    {
      std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>((Parent().GetParent()).Get(Component::ctTransform));

      std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>((Parent().GetParent()).Get(Component::ctPhysics));

      ptransform->SetRotation(pphysics->GetOldRot());
      ptransform->SetTranslation(pphysics->GetOldPos());

      pphysics->SetRotationalVel(pphysics->GetRotationalVel() * -1.25f);
      //pphysics->AddRotationalAccel(pphysics->GetRotationalVel() * 1.8f);

	  if (data)
	  {
		  data->CollidedProp();
	  }

    }
}

	

	void BehaviorWeapon::Init()
	{		

	}
	void BehaviorWeapon::Update(float dt)
	{
    _StateCurr = _StateNext;
		
		if (_StateCurr == bwIdle)
		{

		}
		if (_StateCurr = bwHeld)
		{
			if (0)
			{
				////** Notes on the below code
				////**	The weapon's calculations are in three phases.  
				////**		1. rotate to face the player
				////**		2. move to the player according to the length of the weapon's handle
				////**		3. calculate the new direction of the weapon's velocity and apply force to it
				////**	
				////**	Naming conventions.
				////**	First letter corresponds to either the weapon or the player in most cases
				////**		ex: wphysics = weapon physics, pphysics = player physics
				////** 
				//	
				//	//get the physics and transform components of the weapon and the player
				//	std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(((Parent().GetParent()).Get(Component::ctTransform)));
				//	std::shared_ptr<Transform> wtransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));
				//	std::shared_ptr<Physics> wphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));
				//	std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>((Parent().GetParent()).Get(Component::ctPhysics));

				//	// if the components exist
				//	if (pphysics && ptransform && wphysics && wtransform)
				//	{
				//		// get the translations of the weapon and player
				//		glm::vec3 ptranslation = ptransform->GetTranslation();
				//		glm::vec3 wtranslation = wtransform->GetTranslation();
				//		
				//		// get the velocity of the weapon, oldvelocity is used to store the previous
				//		// velocity
				//		glm::vec3 wvelocity = wphysics->GetVelocity();
				//		glm::vec3 woldvelocity = wphysics->GetVelocity();
				//		
				//		// get the previous position of the player
				//		glm::vec3 poldpos = pphysics->GetOldPos();

				//		// set z values to 0 so we don't get crazy numbers in the z direction
				//		ptranslation.z = 0.0f;
				//		wtranslation.z = 0.0f;
				//		wvelocity.z = 0.0f;
				//		poldpos.z = 0.0f;
				//  
				////*************************
			//    //  STAGE 1: ROTATION
			//    //*************************
				//		// calculate the vector between the weapon and the player
				//		glm::vec3 direction = wtranslation - ptranslation;

				//		// calculate the angle of the direction
				//		glm::vec2 angle(direction.x / direction.length(), direction.y / direction.length());

				//		// set the new rotation to the angle
				//		float newrotation = atan2f(angle.y, angle.x);

				//		// give that angle to the weapon
				//		wtransform->SetRotation(newrotation);

				////*************************
				////  STAGE 2: TRANSLATION
				////*************************

				//		// get the length of the vector between the weapon and the player
				//		float directionlen = glm::length(direction);
				//		
				//		// create the point that the weapon needs to move to
				//		glm::vec3 handlevec = (direction / directionlen) * _HandleLength;

				//		// set the translation of the weapon to the point in the orbit around the player
				//		wtransform->SetTranslation(ptranslation + handlevec);
		  //     
				////****************************************
			//    //  STAGE 3: VELOCITY (KINDA BROKEN)
			//    //****************************************
				//		
				//		// if the weapon is not moving already and the weapon has moved
				//		if ((wvelocity.x == 0.0f && wvelocity.y == 0.0f) && direction != _PrevDirection)
				//		{
				//			// get the initial velocity based on the change in position around the player
				//			wvelocity = direction - _PrevDirection;
				//		}

				//		// add the player's movement into the weapon's velocity
				//		wvelocity += ((pphysics->GetOldPos()) - (ptransform->GetTranslation()));

				//		// calculate the tangent of the direction
				//		glm::vec3 tangent = { -direction.y, direction.x, 0.0f };
				//		
				//		// get the length of the tangent
				//		float tanlen = glm::length(tangent);
				//		
				//		// get the dot product of tangent and velocity
				//		float tandot = glm::dot((tangent / tanlen), wvelocity);

				//		//project the dot product onto the tangent
				//		wvelocity = ((((tandot))) * ((tangent) / tanlen) * 1.f);

				//		// calculate the new velocity from the projected velocity and the old velocity
				//		glm::vec3 wresultvelocity = wvelocity - woldvelocity;

				//		// if the weapon's velocity exceeds the speed limit
				//		if (glm::length(wresultvelocity) > maxvel)
				//		{
				//			// scale the vector to be the max length
				//			wresultvelocity = (wresultvelocity / glm::length(wresultvelocity)) * maxvel;
				//		}
		  //      
		  //      
				//		//if the player is stationary, slow the weapon's velocity and acceleration
				//		//	note: does not work currently
				//		if (ptranslation == poldpos)
				//		{
				//			wphysics->SetVelocity(wphysics->GetVelocity() *0.99f);
				//			wphysics->SetAcceleration(wphysics->GetAcceleration() *0.99f);
		  //      }
		  //      else {
		  //        // add the forces onto the weapon's acceleration
		  //        wphysics->AddAcceleration(wresultvelocity);
		  //      }
		  //      
				//		//store the previous direction
				//		_PrevDirection = direction;
		  //      

				//	}
			}

				//get the physics and transform components of the weapon and the player
					std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(((Parent().GetParent()).Get(Component::ctTransform)));
					std::shared_ptr<Transform> wtransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));
					std::shared_ptr<Physics> wphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));
					std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>((Parent().GetParent()).Get(Component::ctPhysics));

					// if the components exist
					if (pphysics && ptransform && wphysics && wtransform)
					{
						// get the translations of the weapon and player
						glm::vec3 ptranslation = ptransform->GetTranslation();
						glm::vec3 wtranslation = wtransform->GetTranslation();

						// get the velocity of the weapon, oldvelocity is used to store the previous
						// velocity
						glm::vec3 wvelocity = wphysics->GetAcceleration();
						glm::vec3 woldvelocity = wphysics->GetVelocity();

						// get the previous position of the player
						glm::vec3 poldpos = pphysics->GetOldPos();

						// set z values to 0 so we don't get crazy numbers in the z direction
						ptranslation.z = 0.0f;
						wtranslation.z = 0.0f;
						wvelocity.z = 0.0f;
						poldpos.z = 0.0f;

						//*************************
						//  STAGE 1: ROTATION
						//*************************

						float rotation = ptransform->GetRotation();

						// give that angle to the weapon
						wtransform->SetRotation(rotation);

						//*************************
						//  STAGE 2: TRANSLATION
						//*************************

						// create the point that the weapon needs to move to
						glm::vec3 handlevec = { _HandleLength * cos(rotation), _HandleLength * sin(rotation), 0.f };

						// set the translation of the weapon to the point in the orbit around the player
            if(_GoesOnLeft == true)
						  wtransform->SetTranslation(ptranslation - handlevec);
            else
              wtransform->SetTranslation(ptranslation + handlevec);





						//wphysics->SetRotationalVel(wphysics->GetRotationalVel() * 0.99f);
						//wphysics->SetRotationalAccel(wphysics->GetRotationalAccel() * 0.99f);

					}
		}
		_StateCurr = _StateNext;
	}
	void BehaviorWeapon::Exit()
	{
	}

	static void* CollisionHandler(BehaviorWeapon& bp, GameObject& object)
	{
		bp.WeaponCollide(object);
		return nullptr;
	}