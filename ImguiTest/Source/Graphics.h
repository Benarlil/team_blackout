//------------------------------------------------------------------------------
//
// File Name:	Graphics.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

#define MAIN_WINDOW_WIDTH 1600
#define MAIN_WINDOW_HEIGHT 900
#define MAX_Z_DEPTH 1.0f
#define MIN_Z_DEPTH -1.0f

class Mesh;
class Camera;
class Shader;
class Texture;
class Sprite;

class Graphics
{
public:
  static void Init();
  static void Update();
  static void Shutdown();

  static void Graphics::BindTexture(std::string texture_name);

  /* 
     Creates a new mesh with the specified name and shader and returns the reference to a new mesh.
     If a mesh with this name already exists, the function returns the reference to it. 
  */
  static std::shared_ptr<Mesh> CreateNewMesh(std::string mesh_name, std::string shader_name = "");
  static Shader& CreateNewShader(std::string shader_name, 
                                 const char* vertex_shader_path = "",
                                 const char* fragment_shader_path = "");
  static std::shared_ptr<Texture> CreateNewTexture(std::string tex_name, std::string tex_path = "");
  static std::shared_ptr<Texture> CreateNewTexture(sf::Image& image, std::string tex_name);

  static std::shared_ptr<Mesh> GetMesh(std::string name);
  static Shader& GetShader(std::string name);
  static std::shared_ptr<Texture> GetTexture(std::string name);

  static void ResizeWindow(sf::Event::SizeEvent size);
  static sf::RenderWindow &GetMainWindow();

  static Camera    camera;
  static glm::mat4 camera_matrix;
  static FT_Library ft_lib;
private:
  static sf::RenderWindow mainWindow;
  static std::vector<std::unique_ptr<Shader>> shaders;
  static std::vector<std::shared_ptr<Mesh>> meshes;
  static std::map<std::string, std::shared_ptr<Texture>> textures;

  static std::shared_ptr<Mesh>    dummy_mesh;
  static Shader  dummy_shader;
  static std::shared_ptr<Texture> dummy_texture;
};