//------------------------------------------------------------------------------
//
// File Name:	Collider.c
// Author(s):	william patrick (william.patrick)
// Project:		MyGame
// Course:		CS230SU18
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "Collider.h"
#include "Transform.h"
#include "Physics.h"
#include "ColliderCircle.h"
#include "Behavior.h"
#include "BehaviorButton.h"
#include "BehaviorPlayer.h"
#include "BehaviorPlayer.h"
#include "BehaviorEnemy.h"
#include "ColliderLine.h"
#include "ColliderBox.h"
#include "BehaviorWall.h"
#include "BehaviorProp.h"

Collider::Collider(ColliderTypes ctype) : Component()
{
	Type(Component::ctCollider);
	_ColliderType = ctype;
	_Handler = NULL;
}

// Deconstructor for collider base component
Collider::~Collider()
{
}

Collider::ColliderTypes Collider::GetType()
{
	return _ColliderType;
}

void Collider::SetType(ColliderTypes type)
{
	if (type >= ctMax || type <= ColliderTypeInvalid)
	{
		_ColliderType = ColliderTypeInvalid;
	}
	else
	{
		_ColliderType = type;
	}
}
// Set the collision event handler for a collider.
// Params:
//	 collider = Pointer to the collider component.
//	 handler = Pointer to the collision event handler (may be NULL).

void Collider::SetCollisionHandler(func_object func)
{
	if (func)
	{
		_Handler = func;
	}
}

// Check for collision between two circle colliders.
// Params:
//	 collider = Pointer to the first circle collider component.
//	 other = Pointer to the second circle collider component.
static bool Check(ColliderCircle& collider, ColliderCircle& other)
{
	std::shared_ptr<Transform> transform1 = std::dynamic_pointer_cast<Transform>((collider.Parent().Get(Component::ctTransform)));
	std::shared_ptr<Transform> transform2 = std::dynamic_pointer_cast<Transform>((other.Parent().Get(Component::ctTransform)));

	if (transform1 && transform2)
	{
		glm::vec2 point1 = { transform1->GetTranslation().x, transform1->GetTranslation().y };
		glm::vec2 point2 = { transform2->GetTranslation().x, transform2->GetTranslation().y };

		float rot1 = transform1->GetRotation();
		float rot2 = transform2->GetRotation();

		float radius1 = collider.GetRadius();
		float radius2 = other.GetRadius();

		glm::vec2 offset1 = collider.GetOffset();
		glm::vec2 offset2 = other.GetOffset();


		offset1 = { ((offset1.x * cos(rot1)) - (offset1.y * sin(rot1))), ((offset1.x * sin(rot1)) + (offset1.y * cos(rot1))) };
		offset2 = { ((offset2.x * cos(rot2)) - (offset2.y * sin(rot2))), ((offset2.x * sin(rot2)) + (offset2.y * cos(rot2))) };

		point1 += offset1;
		point2 += offset2;

		float distance = glm::distance((point1), (point2));

		if (collider.IsInverse() == false && other.IsInverse() == false)
		{
			if ((distance * distance) < (radius1 + radius2) * (radius1 + radius2))
			{
				return true;
			}
		}
		else
		{
			float larger;
			float smaller;

			if (radius1 > radius2)
			{
				larger = radius1;
				smaller = radius2;
			}
			else
			{
				larger = radius2;
				smaller = radius1;
			}

			
			if(distance > (larger - smaller))
			{
				return true;
			}
			else
			{
				return false;
			}
		

		}
	}
	if (collider.IsInverse() == false && other.IsInverse() == false)
	{
		return false;
	}
	else
	{
		return true;
	}
}

static bool Check(ColliderLine& collider, ColliderCircle& other)
{
	std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>((other.Parent().Get(Component::ctTransform)));
	std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>((other.Parent().Get(Component::ctPhysics)));

	if (ptransform && pphysics)
	{
		const glm::vec2 Bs = pphysics->GetOldPos();
		const glm::vec2 Be = ptransform->GetTranslation();

		for (unsigned i = 0; i < collider._Lines.size(); i++)
		{

			LineSegment segments = collider._Lines[i];
			glm::vec2 P0 = segments[0];
			glm::vec2 P1 = segments[1];

			glm::vec2 edge;
			edge = P0 - P1;

			glm::vec2 normal = edge;

			glm::vec2 V;
			V = { (Be.x - Bs.x), (Be.y - Bs.y) };

			glm::normalize(normal);

			if (glm::dot(V, normal) != 0)
			{
				if (!(glm::dot(normal, Bs) <= glm::dot(normal, P0) && glm::dot(normal, Be) < glm::dot(normal, P0)))
				{
					if (!(glm::dot(normal, Bs) >= glm::dot(normal, P0) && glm::dot(normal, Be) > glm::dot(normal, P0)))
					{
						float Ti = ((glm::dot(normal, P0) - glm::dot(normal, Bs)) / glm::dot(normal, V));

						glm::vec2 Bi;
						Bi = (V * Ti) + Bs;

						glm::vec2 temp1;
						glm::vec2 temp2;


						temp1 = P1 - P0;
						temp2 = Bi - P0;

						if (!(glm::dot(temp1, temp2) < 0))
						{
							temp1 = P0 - P1;
							temp2 = Bi - P1;

							if (!(glm::dot(temp1, temp2) < 0))
							{
								glm::vec2 I;
								I = Be - Bi;

								glm::vec2 S;
								S = normal * glm::dot(normal, I);

								glm::vec2 r;
								r = (S * -2.0f) + I;

								glm::vec2 u_r;
								u_r = glm::normalize(r);

								glm::vec2 Be_new;
								Be_new = Bi + r;

								glm::vec3 newtransform = ptransform->GetTranslation();

								newtransform.x = Be_new.x;
								newtransform.y = Be_new.y;

								ptransform->SetTranslation(newtransform);

								float speed = glm::length(pphysics->GetVelocity());

								glm::vec2 vel;

								vel = u_r * speed;

								pphysics->SetVelocity(vel.x, vel.y);

								return true;
							}
						}
					}
				}
			}
		}
	}
	return false;
}

static bool Check(ColliderBox& collider, ColliderCircle& other)
{
	std::shared_ptr<Transform> transform1 = std::dynamic_pointer_cast<Transform>((collider.Parent().Get(Component::ctTransform)));
	std::shared_ptr<Transform> transform2 = std::dynamic_pointer_cast<Transform>((other.Parent().Get(Component::ctTransform)));

	if (transform1 && transform2)
	{
		glm::vec2 point1 = { transform1->GetTranslation().x, transform1->GetTranslation().y };
		glm::vec2 point2 = { transform2->GetTranslation().x, transform2->GetTranslation().y };

		float rot1 = transform1->GetRotation();
		float rot2 = transform2->GetRotation();

		float width = collider.GetWidth();
		float height = collider.GetHeight();

		
		float radius = other.GetRadius();

		glm::vec2 offset = other.GetOffset();


		offset = { ((offset.x * cos(rot1)) - (offset.y * sin(rot1))), ((offset.x * sin(rot1)) + (offset.y * cos(rot1))) };

		point1 += offset;

		glm::vec2 distance(point1 - point2);

		if (distance.x < 0.f)
		{
			distance.x *= -1.0f;
		}

		if (distance.y < 0.f)
		{
			distance.y *= -1.0f;
		}


		if (distance.x > (width / 2 + radius)) 
		{ 
			return false;
		}
		if (distance.y > (height / 2 + radius)) 
		{ 
			return false; 
		}

		if (distance.x <= (width / 2))
		{ 
			return true; 
		}
		
		if (distance.y <= (height / 2)) 
		{ 
			return true; 
		}

		float corner = (distance.x - width / 2) + (distance.y - height / 2);

		corner *= corner;

		return (corner <= (radius * radius));

	}
	return false;
	
}

/*
 *
 *	Add the other check overloads for each type of collision
 *
 */

// Check for collision between two circle colliders.
// Params:
//	 collider = Pointer to the first circle collider component.
//	 other = Pointer to the second circle collider component.
void IsColliding(Collider& collider1, Collider& collider2)
{
	bool result = false;
	/*
	 Check the types of colliders and assign other references for them
	*/

	if (collider1.GetType() == Collider::ColliderTypeCircle && collider2.GetType() == Collider::ColliderTypeCircle)
	{
		result = Check((ColliderCircle&)collider1, (ColliderCircle&)collider2);
	}
	
	if (collider1.GetType() == Collider::ColliderTypeCircle && collider2.GetType() == Collider::ColliderTypeLine)
	{
		result = Check((ColliderLine&)collider2, (ColliderCircle&)collider1);
	}
	
	if (collider2.GetType() == Collider::ColliderTypeCircle && collider1.GetType() == Collider::ColliderTypeLine)
	{
		result = Check((ColliderLine&)collider1, (ColliderCircle&)collider2);
	}
	
	if (collider1.GetType() == Collider::ColliderTypeCircle && collider2.GetType() == Collider::ColliderTypeBox)
	{
		result = Check((ColliderBox&)collider2, (ColliderCircle&)collider1);
	}
	
	if (collider2.GetType() == Collider::ColliderTypeCircle && collider1.GetType() == Collider::ColliderTypeBox)
	{
		result = Check((ColliderBox&)collider1, (ColliderCircle&)collider2);
	}

		if(result)
		{
      std::shared_ptr<BehaviorBase> behavior1 = std::dynamic_pointer_cast<BehaviorBase>(collider1.Parent().Get(Component::ctBehavior));
      std::shared_ptr<BehaviorBase> behavior2 = std::dynamic_pointer_cast<BehaviorBase>(collider2.Parent().Get(Component::ctBehavior));

	  if (behavior1 && behavior2)
	  {
		  if (behavior1->GetType() == btPlayer)
		  {
			  ((BehaviorPlayer&)*behavior1).SetHandler(behavior2->Parent());
		  }

		  if (behavior1->GetType() == btWeapon)
		  {
			  ((BehaviorWeapon&)*behavior1).SetHandler(behavior2->Parent());
		  }

		  if (behavior2->GetType() == btPlayer)
		  {
			  ((BehaviorPlayer&)*behavior2).SetHandler(behavior1->Parent());
		  }

		  if (behavior2->GetType() == btWeapon)
		  {
			  ((BehaviorWeapon&)*behavior2).SetHandler(behavior1->Parent());
		  }

		  if (behavior1->GetType() == btEnemy)
		  {
			  ((BehaviorEnemy&)*behavior1).SetHandler(behavior2->Parent());
		  }

		  if (behavior2->GetType() == btEnemy)
		  {
			  ((BehaviorEnemy&)*behavior2).SetHandler(behavior1->Parent());
		  }

		  if (behavior1->GetType() == btWall)
		  {
			  //((BehaviorWall&)*behavior1).SetHandler(behavior2->Parent());
		  }

		  if (behavior2->GetType() == btWall)
		  {
			  //((BehaviorWall&)*behavior2).SetHandler(behavior1->Parent());
		  }

		  if (behavior1->GetType() == btProp)
		  {
			  //((BehaviorProp&)*behavior1).SetHandler(behavior2->Parent());
		  }

		  if (behavior2->GetType() == btProp)
		  {
			  //((BehaviorProp&)*behavior2).SetHandler(behavior1->Parent());
		  }

		  if (&collider1._Handler)
		  {
			  collider1._Handler();
		  }

		  if (&collider2._Handler)
		  {
			  collider2._Handler();
		  }
	  }
		}

}
