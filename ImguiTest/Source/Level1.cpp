//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Level1.cpp                                                        //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//


//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "Level1.h"
#include "Graphics.h"
#include "GameObjectManager.h"
#include "DebugLog.h"
#include "Serialize.h"
#include "Imgui/imgui.h"

#include "MusicManager.h"

Serializer serializerLevel1("level1.json");
Input input = Input();

//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------
namespace Levels
{
  void Color::Load()
  {
    serializerLevel1.CreateFromFile();

    MusicManagerSetSong("Battle");

    cInput = new ControllerSystem();

    DebugMessage("Gamestate Color Loading");
  }

  void Color::Init()
  {
    DebugMessage("Gamestate Color Initializing");

    MusicManagerPlaySong();
}


void Color::Update(float dt)
{
  // Call the corresponding action function with the key
  extern bool lockInput;
  if (!lockInput)
  {
    input.Exec();
    cInput->Exec();
  }
}

void Color::Unload()
{
  DebugMessage("Gamestate Color Unloading");
  GameObjectManagerShutdown();
  MusicManagerStopSong();
  input.ClearMap();
}

void Color::Shutdown()
{
  DebugMessage("Gamestate Color Shutting down");
}

void Color::OnAction(sf::Event& event, float dt)
{
  cInput->Update(event);
  input.Update(event);
}

void Quit::Init()
{
  Graphics::GetMainWindow().close();
}
}