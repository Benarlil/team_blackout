//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	MusicManager.cpp                                                  //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//


#include "stdafx.h"
#include "DebugLog.h"
#include "MusicManager.h"

// The one Music pointer needed.
static std::unique_ptr<sf::Music> Song = std::make_unique<sf::Music>();

// Set the song that will be played/manipulated
void MusicManagerSetSong(std::string songName)
{
  if (Song->getStatus() == sf::Sound::Status::Playing)
  {
    DebugMessage("Cannot set a song while one is still playing!");

    return;
  }

  std::string songPath; // make a string that will contain the entire song path

  songPath = "Sounds/Music/"; // set the music path here to make it easier for this function to be called

  songPath += songName; // add in the given song name

  songPath += ".wav"; // all music should be wav files
    
  bool success = Song->openFromFile(songPath); // open the song

  // if it opened
  if (success)
  {
    DebugMessage("Opening Music from file: ", songPath); // send out that it has been opened

    Song->setLoop(true); // Songs will loop by default

    sf::Music::TimeSpan defaultLoop(sf::seconds(0), Song->getDuration()); // create the default loop based on the open song

    Song->setLoopPoints(defaultLoop); // set the default loop times
  }

  // otherwise
  else
  {
    // output debug
    DebugMessage("Unable to open music file! Are you sure you had the right name and it is in the music folder? Expected Path: ", songPath);
  }
}

// Allows to set a loop other than the default by passing in the offset and length. Opening from file resets this loop
// both are in seconds, but that is subject to change
void MusicManagerSetLoopPoints(float startLoop, float loopLength)
{
  // if the passed in loop length or start time is greater than the song's length
  if ((loopLength || startLoop) > Song->getDuration().asSeconds())
  {

    DebugMessage("Invalid times used for song loop points. Either start or lenght are greater than time.");

    return; // this cannot be used as valid loop points
  }

  // if either lenght or time are less than 0
  if (startLoop < 0 || loopLength < 0)
  {
    DebugMessage("No negative values should be in loop points!");

    return;
  }
  
  sf::Time loopStartSeconds = sf::seconds(startLoop); // convert the passed in start into seconds
  sf::Time loopLengthSeconds = sf::seconds(loopLength); // convert the passed in length into seconds

  sf::Music::TimeSpan newLoop(loopStartSeconds, loopLengthSeconds); // Make the loop time based on the given input

  Song->setLoopPoints(newLoop); // set the loop times
}

// Set if the current song should loop or not
void MusicManagerSetLoop(bool toLoop)
{
  Song->setLoop(toLoop);
}

// Play or resume the song
void MusicManagerPlaySong()
{
  Song->play();

  DebugMessage("Playing Music");
}

// Pause the song.
void MusicManagerPauseSong()
{
  Song->pause();

  DebugMessage("Pausing Music");
}

// Set the volume of the song
void MusicManagerSetVolume(float volume)
{
  if (volume <= 100 && volume >= 0)
  {
    Song->setVolume(volume);

    DebugMessage("Music volume set to: ", volume);
  }

  else
  {
    DebugMessage("Music volume out of range. Set between 0 and 100. Volume: ", volume);
  }

}

// Increase the volume of the song relative to the current volume
void MusicManagerIncreaseVolume(float volume)
{
  float current = Song->getVolume();

  float change = volume - current;

  if (change >= 0 && change <= 100)
  {
    Song->setVolume(change);

    DebugMessage("Music volume set to: ", change);
  }

  else
  {
    DebugMessage("Music volume change out of range. Change:", volume);
    DebugMessage("and new volume would have been:", change);
  }
}

// Stops the song. DO THIS BEFORE SETTING IT TO A NEW SONG
void MusicManagerStopSong()
{
  Song->stop();

  DebugMessage("Stopping Music");
}

void MusicManagerToggleSong()
{
  if (Song->getStatus() == sf::Sound::Status::Playing)
  {
    MusicManagerPauseSong();
  }

  else if (Song->getStatus() == sf::Sound::Status::Paused)
  {
    MusicManagerPlaySong();
  }
}
