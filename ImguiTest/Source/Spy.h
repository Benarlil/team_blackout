#pragma once
#include "stdafx.h"
#include "Component.h"
#include "GameObject.h"
#include "RoundData.h"

class Spy
{
	GameObject* _Target;
	int _RoundCount;
	std::vector<RoundData> _RoundData;

	std::string _Name;
	int _TotalDamageTaken;
	int _TotalDamageDealt;
	int	_TotalHealing;
	int _TotalAttacks;
	int _TotalTimesAttacked;

	int _TotalPropCollisions;
	int _TotalPlayerCollisions;
	int _TotalCrabCollisions;

	void IncrementRound();

public:
	Spy();
	Spy(GameObject& target);
	~Spy();

	void AquireTarget(GameObject& target);
	GameObject* ReturnTarget();


	void GatherData(RoundData& report);
};

