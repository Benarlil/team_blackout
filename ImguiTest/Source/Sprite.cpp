//------------------------------------------------------------------------------
//
// File Name:	Sprite.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Sprite.h"
#include "Mesh.h"
#include "Texture.h"
#include "GameObject.h"
#include "Graphics.h"
#include "Transform.h"

GLuint Sprite::next_sprite_id = 0;

// SpriteSource class

SpriteSource::SpriteSource(int rows, int cols, std::string texture_name, std::string texture_path) : 
  tex_rows(rows),
  tex_cols(cols),
  tex_name(texture_name),
  tex_path(texture_path),
  texture(Graphics::CreateNewTexture(tex_name, tex_path))
{
}

int SpriteSource::rows() const
{
  return tex_rows;
}

int SpriteSource::cols() const
{
  return tex_cols;
}

void SpriteSource::rows(int val)
{
  tex_rows = val;
}

void SpriteSource::cols(int val)
{
  tex_cols = val;
}

int SpriteSource::frame_count() const
{
  return tex_cols * tex_rows;
}

std::string SpriteSource::texture_name() const
{
  return tex_name;
}

std::string SpriteSource::texture_path() const
{
  return tex_path;
}

void SpriteSource::swap_texture(std::string t_name, std::string t_path)
{
  //if (!t_name.empty()) {
    texture = Graphics::CreateNewTexture(t_name, t_path);
    tex_name = t_name;
    tex_path = t_path;
  //}
}

void SpriteSource::swap_texture(sf::Image& image, std::string t_name)
{
  if (!t_name.empty()) {
    texture = Graphics::CreateNewTexture(image, t_name);
    tex_name = t_name;
  }
}

void SpriteSource::get_uv(unsigned int frameIndex, float& u, float& v) const
{
  if (frameIndex < (unsigned)(tex_cols * tex_rows))
  {
    float uSize = 1.0f / tex_cols;
    float vSize = 1.0f / tex_rows;

    u = uSize * (frameIndex % tex_cols);
    v = vSize * (frameIndex / tex_cols);
  }
}

// Sprite class

/* Will crash the game if you wont initialize the Sprite created with this constructor
   with SetMesh and (optionally) SetTexture */
Sprite::Sprite() : 
  Component(),
  mesh(Graphics::GetMesh("dummy_mesh")),
  source()
{
  Type(Component::ctSprite);
  sprite_id = next_sprite_id;
  next_sprite_id++;
}

Sprite::Sprite(std::string mesh_name, std::string texture_name, std::string opt_tex_path) :
  Component(),
  mesh_name(mesh_name),
  mesh(Graphics::CreateNewMesh(mesh_name)),
  source(mesh->rows(), mesh->cols(), texture_name, opt_tex_path)
{
  Type(Component::ctSprite);
  sprite_id = next_sprite_id;
  next_sprite_id++;
}

Sprite::Sprite(const Sprite& s) : 
  Component(),
  mesh(s.mesh),
  mesh_name(s.mesh_name),
  source(s.source)
{
  Type(Component::ctSprite);

  current_color = s.current_color;
  uv_offset = s.uv_offset;

  sprite_id = next_sprite_id;
  next_sprite_id++;
}

Sprite::~Sprite() 
{
  mesh->unreferenced(source.texture_name(), sprite_id);
}

void Sprite::SetMesh(std::string m_name)
{
  if (!mesh_name.empty())
    mesh->unreferenced(source.texture_name(), sprite_id);
  mesh = Graphics::CreateNewMesh(m_name);
  mesh_name = mesh->GetName();
  source.rows(mesh->rows());
  source.cols(mesh->cols());
}

void Sprite::SetTexture(std::string t_name, std::string t_path, bool transform, glm::mat4 int_transform)
{
  if (!mesh_name.empty()) {
    std::string tex_name = source.texture_name();
    if (!tex_name.empty() && tex_name != t_name) {
      mesh->unreferenced(source.texture_name(), sprite_id);
      if (transform)
        mesh->SetTransform(t_name, sprite_id, int_transform);
    }
    if (source.texture_path() != t_path)
      source.swap_texture(t_name, t_path);
  }
}

void Sprite::SetTextTexture(sf::Image& image)
{
  if (!mesh_name.empty()) {
    if (!source.texture_name().empty())
      mesh->unreferenced(source.texture_name(), sprite_id);
  }
  source.swap_texture(image, Parent().GetName() + "_text");
}

void Sprite::SetUVOffset(glm::vec2 new_uv)
{
  uv_offset = new_uv;
  if (new_uv.x || new_uv.y)
    framed = false;
  else
    framed = true;
}

void Sprite::SetColor(glm::vec4 color)
{
  colored = true;
  current_color = color;
}

void Sprite::SetColor(float r, float g, float b, float a)
{
  colored = true;
  current_color = glm::vec4(r, g, b, a);
}

void Sprite::SetAlpha(float a)
{
  colored = true;
  current_color.a = a;
}

void Sprite::SetFrame(unsigned int frame)
{
  frameIndex = frame;
}

glm::vec4 Sprite::GetColor() const
{
  return current_color;
}

unsigned int Sprite::GetFrame() const
{
  return frameIndex;
}

glm::vec2 Sprite::GetUVOffset() const
{
  return uv_offset;
}

std::string Sprite::GetMeshName() const
{
  return mesh_name;
}

std::string Sprite::GetTextureName() const
{
  return source.texture_name();
}

std::string Sprite::GetTexturePath() const
{
  return source.texture_path();
}

const SpriteSource& Sprite::GetSource() const
{ 
  return source; 
}

void Sprite::Update(float dt)
{
  if (transform.expired()) {
    transform = std::dynamic_pointer_cast<Transform>(Parent().Get(ctTransform));
  }
  if (framed) {
    source.get_uv(frameIndex, uv_offset.x, uv_offset.y);
    mesh->SetUVOffeset(source.texture_name(), sprite_id, uv_offset);
  }
  const glm::mat4& mtx = transform.lock()->GetMatrix();
  if (prev_transform != mtx) {
    mesh->SetTransform(source.texture_name(), sprite_id, mtx);
    prev_transform = mtx;
  }
  if (colored) {
    mesh->SetColor(source.texture_name(), sprite_id, current_color);
    colored = false;
  }
}