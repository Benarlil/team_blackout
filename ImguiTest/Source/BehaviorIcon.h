//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorIcon .h                                                   //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "Behavior.h"
#include "BehaviorPlayer.h"
#include "Animation.h"

class BehaviorIcon : public BehaviorBase
{
private:
  int healthCurr;
  int healthMax;
  int prevHealth;
  std::string parentName;
  std::shared_ptr<BehaviorPlayer> parentBehavior;
  std::string deadIcon;
public:
  BehaviorIcon();
  BehaviorIcon(std::string parent);
  std::string GetDeathIcon(void);
  void SetParent(std::string parent);
  void SetDeathIcon(std::string deadIcon);
  void Update(float dt);
};