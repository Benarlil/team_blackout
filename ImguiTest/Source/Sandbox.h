//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Sandbox.h                                                         //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "GameStateManager.h"
namespace Levels
{
  class Sandbox : public Engine::GameState
  {
  public:
    void Load()  override;
    void Init()  override;
    void Update(float dt)  override;
    void Unload()  override;
    void Shutdown()  override;
    void OnAction(sf::Event& event, float dt) override;
  };
}
#pragma once
