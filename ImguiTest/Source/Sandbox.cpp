//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Sandbox.cpp                                                       //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "stdafx.h"
#include "Sandbox.h"
#include "GameObject.h"
#include "GameObjectManager.h"
#include "Transform.h"
#include "Sprite.h"
#include "Physics.h"
#include "ColliderCircle.h"
#include "DebugObject.h"
namespace Levels
{
  static int ShiftTimer = 0;
  //void CreateEnemy()
  //{
  //  GameObject& gO = GameObjectManagerCreateObject("enemy");

  //  gO.Add(std::make_shared<Transform>(Transform(0.0f, 0.0f, 0.0f)));

  //  gO.Add(std::make_shared<Physics>(Physics(5.0f)));

  //  gO.Add(std::make_shared<ColliderCircle>(ColliderCircle(gO.GetComponent(Transform)->GetScale().x / 2)));

  //  gO.Add(std::make_shared<Sprite>(Sprite("mesh1x1", 1, 1, "Enemy", "Assets\\Characters\\game_char_portrait_healthy.PNG")));
  //}
  void Sandbox::Load()
  {
   /* CreateEnemy();
    CreateDebugDraw("enemy");
    ToggleDebugObjects("enemy");*/
  }
  void Sandbox::Init()
  {

  }
  void Sandbox::Update(float dt)
  {
    GameObject& enemy = GameObjectManagerFindObject("enemy");
    PhysicsPtr physics = enemy.GetComponent(Physics);
    TransformPtr transform = enemy.GetComponent(Transform);
    double r = (double)rand() / RAND_MAX;
    double anotherR = (double)rand()/ RAND_MAX;
    //int randomFloat = distribution(generator);  // generates number in the range 0-1
    //int anotherRandomFloat = anotherDistribution(generator);
    ShiftTimer++;
    if (ShiftTimer == 300)
    {
      if (r > 0.5)
      {
        if (anotherR < 0.5)
        {
          //pos x pos y
          physics->SetVelocity(glm::vec3(1.0, 1.0, 0.0f));
          ShiftTimer = 0;
        }
        else
        {
          //neg x pos y
          physics->SetVelocity(glm::vec3(-1.0, 1.0, 0.0f));
          ShiftTimer = 0;
        }
      }
      else
      {
        if (anotherR > 0.5)
        {
          //pos x neg y
          physics->SetVelocity(glm::vec3(1.0, -1.0, 0.0f));
          ShiftTimer = 0;
        }
        else
        {
          //neg x neg y
          physics->SetVelocity(glm::vec3(-1.0, -1.0, 0.0f));
          ShiftTimer = 0;
        }
      }
    }
    //  physics->SetVelocity(enemySpeed);
    //if (transform->GetTranslation().x > 1.0f || transform->GetTranslation().x < -1.0f)
    //  physics->SetVelocity(glm::vec3(enemySpeed.x *= -1, enemySpeed.y, enemySpeed.z));
  }
  void Sandbox::Unload()
  {

  }
  void Sandbox::Shutdown()
  {

  }
  void Sandbox::OnAction(sf::Event& event, float dt)
  {
  }
}