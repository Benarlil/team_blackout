//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorCursor.h                                                  //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// The behavior for the cursor object for menus.                                //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "Behavior.h"
#include "Transform.h"

class BehaviorCursor : public BehaviorBase
{

public:

  // enum for the type of controller being used on the cursor
  enum ModeOfOperation
  {
    MOUSE,
    CONTROLLER,
    NONE
  };

  // Default constructor that sets the mode to none
  BehaviorCursor();

  ~BehaviorCursor();

  // Constructor that takes the mode
  BehaviorCursor(ModeOfOperation setMode);

  // Update function that will change to another button or press a button based on input
  void Update(float dt);

  // Add a menu button transform directly
  void AddMenuButton(GameObject& button);

  // Go to the next menu object based on input
  void GoToNextButton();

  // Go to the previous menu object based on input
  void GoToPreviousButton();

  //Change the mode of the cursor
  void ChangeMode(ModeOfOperation setMode);

private:
  
  void mouseMove();

  void controllerMove();

  // vector of menu button transforms
  std::vector<TransformPtr> menuButtons_;

  // current index/button
  unsigned int currentIndex_;

  // max index/the total number of buttons 
  unsigned int maxIndex_;

  // current mode of operation
  ModeOfOperation currentMode_;

  // the cursor's transform
  TransformPtr cursorTransform_;

};