//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	ControllerInput.cpp                                               //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "stdafx.h"
#include "Input.h"




Input::Input()
{
  /*
     For each key, initialize a data struct and insert
     it into the map
  */
  for (int i = 0; i < sf::Keyboard::Key::KeyCount; i++)
  {
    KeyData keyData;
    keyData.PressedActionPtr = NULL;
    keyData.ReleasedActionPtr = NULL;
    keyData.isPressed = false;
    keyData.modifier = NULL;
    keyMap.insert({ sf::Keyboard::Key(i), keyData });
  }
}

void Input::Update(sf::Event& event)
{
    /*
      -If the key is pressed, set the data flag to true
      -If the key is released, set the data flag to false
    */
    if (event.type == sf::Event::EventType::KeyPressed)
      keyMap[event.key.code].isPressed = true;
    else if (event.type == sf::Event::EventType::KeyReleased)
      keyMap[event.key.code].isPressed = false;


}

void * Input::GetArguments(sf::Keyboard::Key key)
{
  return keyMap[key].functionArg;
}

void Input::SetArguments(sf::Keyboard::Key key, void * args)
{
  keyMap[key].functionArg = args;
}

void Input::AssignPressedAction(sf::Keyboard::Key key, std::function<void(void*)>funcptr)
{
  keyMap[key].PressedActionPtr = funcptr;
}

void Input::AssignReleasedAction(sf::Keyboard::Key key, std::function<void(void*)>funcptr)
{
  keyMap[key].ReleasedActionPtr = funcptr;
}

std::map<sf::Keyboard::Key, Input::KeyData> Input::GetMap()
{
  return keyMap;
}

void Input::Exec()
{
  for (const auto k : keyMap)
  {
    const auto& key = k.first;
    const auto& keydata = k.second;
    

    if (keydata.isPressed == true && keydata.PressedActionPtr)
    {
       keydata.PressedActionPtr(keydata.functionArg);
    }

    if (keydata.isPressed == false && keydata.ReleasedActionPtr)
    {
      keydata.ReleasedActionPtr(keydata.functionArg);
      std::cout << "Releasing key:" << key << std::endl;
    }
  }
}

bool Input::GetFlag(sf::Keyboard::Key key)
{
  return keyMap[key].isPressed;
}

void Input::ClearMap(void)
{
	keyMap.clear();
}