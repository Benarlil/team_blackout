//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Movement.cpp                                                      //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "stdafx.h"
#include "Movement.h"
#include "Physics.h"
#include "GameObject.h"
#include "Engine.h"
#include "GameStateManager.h"
#include "Serialize.h"

#define ACCERLERATION 5.f
#define ROT_ACCELERATION .5f

std::vector<std::function<void(void *)>> inputFunctionList = {MovePlayerUp, MovePlayerDown, MovePlayerLeft, 
                                                              MovePlayerRight, HardReset, RotatePlayerLeft, 
                                                              RotatePlayerRight};
void MovePlayerUp(void * gameObject)
{
  std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
  //physics->SetVelocity(physics->GetVelocity() * 0.4f);
  physics->AddAcceleration(glm::vec3(0.0f, ACCERLERATION, 0.0f));
}

void MovePlayerDown(void * gameObject)
{
  std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
  //physics->SetVelocity(physics->GetVelocity() * 0.4f);
  physics->AddAcceleration(glm::vec3(0.0f, -ACCERLERATION, 0.0f));
}

void MovePlayerLeft(void * gameObject)
{
  std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
 // physics->SetVelocity(physics->GetVelocity() * 0.4f);
  physics->AddAcceleration(glm::vec3(-ACCERLERATION, 0.0f, 0.0f));
}

void MovePlayerRight(void * gameObject)
{
  std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
  //physics->SetVelocity(physics->GetVelocity() * 0.4f);
  physics->AddAcceleration(glm::vec3(ACCERLERATION, 0.0f, 0.0f));
}

void RotatePlayerLeft(void * gameObject)
{
	std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
	//physics->SetVelocity(physics->GetVelocity() * 0.4f);
	physics->AddRotationalAccel(ROT_ACCELERATION);
}

void RotatePlayerRight(void * gameObject)
{
	std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
	//physics->SetVelocity(physics->GetVelocity() * 0.4f);
	physics->AddRotationalAccel(-ROT_ACCELERATION);
}

void PlayerStopVertical(void * gameObject)
{
	std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
	physics->SetVelocity(physics->GetVelocity().x, physics->GetVelocity().y * 0.99f);
  physics->SetAcceleration(physics->GetAcceleration() * 0.99f);

}

void PlayerStopHorizontal(void * gameObject)
{
  std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((GameObject*)gameObject)->Get(Component::ctPhysics));
  physics->SetVelocity(physics->GetVelocity().x  * 0.99f, physics->GetVelocity().y);
  physics->SetAcceleration(physics->GetAcceleration() * 0.99f);
}

void HardReset(void * unused)
{
	extern Serializer serializerLevel1;
	serializerLevel1.ClearObjectList();
  extern bool onStart;
  onStart = true;
	if (Engine::GameStateManager::GetCurrentName() == "Color")
	{
		Engine::GameStateManager::Go(Engine::GAMESTATES::HardR);
    
	}
	else
	{
		Engine::GameStateManager::Go(Engine::GAMESTATES::Color);
	}
}
