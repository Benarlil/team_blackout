//------------------------------------------------------------------------------
//
// File Name:	Physics.h
// Author(s):	William Patrick (william.patrick)
// Project:		Flail Fighters
//
//		Note: Mass cannot be 0 because that will break calculations for
//			  acceleration
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

#include "stdafx.h"
#include "Transform.h"
#include "Component.h"
#include "GameObject.h"


typedef glm::vec3 vec3;
class Physics : public Component
{
	// velocity of the object, has an x, y, and z componenet.  z should not change
	vec3 _Velocity;

	// the position of the object on the previous frame
	vec3 _Oldposition;

	float _Oldrotation;

	// the acceleration of the object, used to affect the velocity
	vec3 _Acceleration;
	 
	// mass of the object, used for calculating intertia and force of impact
	// _Mass can never be 0 because that will break acceleration calculations
	float _Mass;

	float _RotationalVel;
	float _RotationalAccel;

  std::weak_ptr<Transform> transform;
public:

// default constructor for the physics class (no mass)
	Physics();

// Constructor for physics with a set mass 
// Param:
//		mass: the initial mass (used to slow acceleration)
	Physics(float mass);

	Physics(Physics& original);

// Destructor for physics class
	~Physics();

// A method to get the current velocity 
// Return:
//		vec3: vector that is the current velocity
	vec3 GetVelocity();


// A method to change the current Acceleration (slower for more massive objects)
// Param:
//      vec3: a vector to set the velocity to (make sure z = 0)
void SetVelocity(vec3 input);
	
// A method to change the current Acceleration (slower for more massive objects)
// Param:
//      x: the x velocity to be set to
//		y: the y velocity to be set to
void SetVelocity(float x, float y);

// A method to get the current velocity 
// Return:
//		vec3: vector that is the current acceleration
	vec3 GetAcceleration();

// A method to change the current Acceleration (slower for more massive objects)
// Param:
//      vec3: a vector to apply force to the object	
	void SetAcceleration(vec3 input);

// A method to change the current Acceleration (slower for more massive objects)
// Param:
//      x: force to apply to the object in the x direction
//      y: force to apply to the object in the y direction
	void SetAcceleration(float x, float y);


// A method to add to the current Acceleration (slower for more massive objects)
// Param:
//      vec3: a vector to apply force to the object	
	void AddAcceleration(vec3 input);

// A method to add to the current Acceleration (slower for more massive objects)
// Param:
//      x: force to apply to the object in the x direction
//      y: force to apply to the object in the y direction
	void AddAcceleration(float x, float y);

// Uses velocity and mass to determine the force of an impact
// Return:
//		float: how much force the object exerts on the impact
	float CalculateForce();


// A method to change the mass of an object
// Param:
//      float: the new mass
	void SetMass(float input);

// A method to get the mass of an object
// Param:
//      float: the new mass
	float GetMass();

	vec3 GetOldPos();

	float GetOldRot();


	float GetRotationalAccel();


	void SetRotationalAccel(float input);

	float GetRotationalVel();


	void SetRotationalVel(float input);

	void AddRotationalAccel(float input);

	void AddRotationalVel(float input);

// A method to update the component
// Param:
//		dt: change in time from last frame
	void Update(float dt);
};

typedef class std::shared_ptr<Physics> PhysicsPtr;
