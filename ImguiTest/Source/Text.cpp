//------------------------------------------------------------------------------
//
// File Name:	Text.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Text.h"
#include "Font.h"
#include "GameObject.h"
#include "Transform.h"

Text::Text(std::string font_path, int char_width, std::string mesh_name, int char_height) :
  Sprite(mesh_name),
  font(std::move(Font(font_path, char_width, char_height)))
{
  
}

Text::Text(Font& font, std::string mesh_name) :
  Sprite(mesh_name),
  font(font)
{

}

Text::Text(const Text& text_component)  :
  Sprite(static_cast<Sprite>(text_component).GetMeshName()),
  font(text_component.font)
{

}

void Text::Update(float dt)
{
  Sprite::Update(dt);
}

/*
   Creates a texture of the specified string.
   Note: this is an extremely costly operation and should
   idealy be performed rarely (during loading or initializations)
*/
void Text::SetString(std::string new_string, bool init)
{
  int max_height = 0;
  int max_width = 0;
  std::vector<sf::Image> glyphs;
  glyphs.reserve(new_string.size());
  string = new_string;
  for (auto ch : string) {
    GlyphRenderData data = font.CharTo32Bitmap(ch);
    sf::Image glyph;
    max_width += data.width;
    if (max_height < data.height)
      max_height = data.height;
    glyph.create(data.width, data.height, static_cast<sf::Uint8*>(data.pixels));
    glyphs.push_back(std::move(glyph));
  }
  sf::Image str_image;
  str_image.create(max_width, max_height, sf::Color(0, 0, 0, 0));
  int pen_x = 0;
  int pen_y = 0;
  for (auto& glyph : glyphs) {
    str_image.copy(glyph, pen_x, pen_y);
    pen_x += glyph.getSize().x;
  }
  //str_image.flipVertically();
  Sprite::SetTextTexture(str_image);
  TransformPtr transform = std::dynamic_pointer_cast<Transform>(Parent().Get(ctTransform));
  auto scale = transform->GetScale();
  float aspect = float(str_image.getSize().x) / str_image.getSize().y;
  if (init)
    transform->SetScale(glm::vec3(scale.x * aspect, scale.y, 1.0f));
}

std::string Text::GetString()
{
  return string;
}
