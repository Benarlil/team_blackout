#include "RoundData.h"
#include "Spy.h"



RoundData::RoundData() : Component(ctData)
{
	_Owner = NULL;

	_DamageDealt = 0;
	_DamageTaken = 0;
	_Healing = 0;
	_Rotations = 0;

	_AttackCount = 0;
	_TimesAttacked = 0;

	_PropCollisions = 0;
	_PlayerCollisions = 0;
	_CrabCollisions = 0;
	_HazardCollisions = 0;
}

RoundData::RoundData(Spy& owner)
{
	_Owner = &owner;

	_DamageDealt = 0;
	_DamageTaken = 0;
	_Healing = 0;
	_Rotations = 0;

	_AttackCount = 0;
	_TimesAttacked = 0;

	_PropCollisions = 0;
	_PlayerCollisions = 0;
	_CrabCollisions = 0;
	_HazardCollisions = 0;
}

RoundData::~RoundData()
{
	if (_Owner)
	{
		_Owner->GatherData(*this);
	}

}


Spy* RoundData::GetOwner()
{
	if (_Owner)
	{
		return _Owner;
	}
	else
	{
		return NULL;
	}
}

void RoundData::SetOwner(Spy& owner)
{
	_Owner = &owner;
}

void RoundData::TookDamage(int damage)
{
	_DamageTaken += damage;
	_TimesAttacked++;
}
void RoundData::DealtDamage(int damage)
{
	_DamageDealt += damage;
	_AttackCount++;
}
void RoundData::Healed(int heal)
{
	_Healing += heal;
}
void RoundData::Rotated()
{
	_Rotations++;
}
void RoundData::CollidedProp()
{
	_PropCollisions++;
}
void RoundData::HitCrab()
{
	_CrabCollisions++;
}
void RoundData::CollidedHazard()
{
	_HazardCollisions++;
}
int RoundData::GetDamageDealt()
{
	return _DamageDealt;
}
int RoundData::GetDamageTaken()
{
	return _DamageTaken;
}
int RoundData::GetHealing()
{
	return _Healing;
}
int RoundData::GetRotations()
{
	return _Rotations;
}
int RoundData::GetAttackCount()
{
	return _AttackCount;
}
int RoundData::GetTimesAttacked()
{
	return _TimesAttacked;
}
int RoundData::GetPropCollisions()
{
	return _PropCollisions;
}
int RoundData::GetPlayerCollisions()
{
	return _PlayerCollisions;
}
int RoundData::GetCrabCollisions()
{
	return _CrabCollisions;
}
int RoundData::GetHazardCollisions()
{
	return _HazardCollisions;
}