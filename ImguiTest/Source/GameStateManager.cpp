//------------------------------------------------------------------------------
//
// File Name:	GameStateManager.cpp
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "GameStateManager.h"
#include "Level1.h"
#include "DebugLog.h"
#include "Sandbox.h"
#include "SplashScreens.h"
#include "MainMenu.h"
#include "stdafx.h"
//------------------------------------------------------------------------------
// Private Variables:
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------


namespace Engine
{
  // Declare variables from the manager class
  std::vector<GameState*> GameStateManager::States;
  GameState* GameStateManager::current_;
  GameState* GameStateManager::next_;

  void GameStateManager::Init()
  {
    // Allocate a new level
    GameState * Color = new Levels::Color;
    GameState * Sandbox = new Levels::Sandbox;
    GameState * Restart = new Levels::Quit;
    GameState * Quit = new Levels::Quit;
    GameState * Invalid = new Levels::Quit;
	GameState * hardR = new Levels::Color;
  GameState * SplashScreens = new Levels::SplashScreens;
  GameState * MainMenu = new Levels::MainMenu;



    // Push level onto the vector
    States.push_back(Invalid);
    States.push_back(Restart);
    States.push_back(Quit);
    States.push_back(Color);
	  States.push_back(hardR);
    States.push_back(Sandbox);
    States.push_back(SplashScreens);
    States.push_back(MainMenu);
    


    // Set the current pointer to the new level
    current_ = States[GAMESTATES::Invalid];
    next_ = States[GAMESTATES::SplashScreens];
    
  }

  void GameStateManager::Update(float dt)
  {
    if (IsChanging())
    {
      // Shuts down the current game state
      if(IsInvalid() != true)
        current_->Shutdown();

      // Complete shut down of current level and load next level
            // Check if the level is restarting
      if (IsRestarting() == true)
      {
        next_ = current_;
		    DebugMessage("GameState Restarting");
      }
      else if (IsInvalid() == true)
      {
        current_ = next_;
		    current_->Load();
      }
      else
      {
        current_->Unload();

		    current_ = next_;
		    current_->Load();
      }
	  
      current_->Init();
    }
    current_->Update(dt);
  }

  GameState& GameStateManager::GetCurrent()
  {
    return *current_;
  }

  std::string GameStateManager::GetCurrentName()
  {
	  for (int i = 0; i < NUM_GAMESTATES; i++)
	  {
		  if (States[i] == current_)
		  {
			  return GAMESTATE_STRINGS[i];
		  }
	  }
  }
  


  bool GameStateManager::IsValid(GAMESTATES gameState)
  {
    // Return true if the game state is within bounds
    return ((3 <= gameState) && (gameState < GAMESTATES::NUM_GAMESTATES));
  }

  bool GameStateManager::IsSpecial(GAMESTATES gameState)
  {
    // Return true if the game state is restarting or quitting
    return ((gameState == GAMESTATES::Restart) || (gameState == GAMESTATES::Quit));
  }

  void* GameStateManager::Go(GAMESTATES gameState)
  {
    // If the game state is normal, set the next game state
    if (IsValid(gameState) || IsSpecial(gameState))
      next_ = States[gameState];
    // Add debug in case where gamestate is not valid
    return nullptr;
  }

  void GameStateManager::Shutdown()
  {
    int size = States.size();
    // Free the memory in game state manager
    for (int i = 0; i < size; i++)
      delete[] States[i];
  }

  bool GameStateManager::IsChanging()
  {
    // Returns true if gamestates are the same, false if vice versa
    return (current_ != next_);
  }

  bool GameStateManager::IsRestarting()
  {
    return next_ == States[GAMESTATES::Restart];
  }

  bool GameStateManager::IsInvalid()
  {
    return current_ == States[GAMESTATES::Invalid];
  }

  bool GameStateManager::IsRunning()
  {
    return current_ != States[GAMESTATES::Quit];
  }
  
}
