//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	SplashScreens.h                                                   //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// The pre-menu Splash Screens                                                  //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#pragma once
#include "GameStateManager.h"
namespace Levels
{
  class SplashScreens : public Engine::GameState
  {
  public:
    void Load()  override;
    void Init()  override;
    void Update(float dt)  override;
    void Unload()  override;
    void Shutdown()  override;
    void OnAction(sf::Event& event, float dt) override;
  };
}
