#pragma once
#include "stdafx.h"
#include "Behavior.h"
class BehaviorWall : public BehaviorBase
{
	int _StateCurr;
	int _StateNext;

public:

	enum States
	{
		bwInvalid = -1,
		bwIdle
	};

	BehaviorWall();
	~BehaviorWall();
	void Update(float dt);
};

