//------------------------------------------------------------------------------
//
// File Name:	Serialize.h
// Author(s):	Jack Klein
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"
#include "GameObject.h"
#include "Behavior.h"
#include <fstream>

using json = nlohmann::json;

class Serializer
{

public:
	/* Initializes serializer, reads JSON file */
	Serializer(std::string filename);

	/* Read a data value from JSON file 
	 * NOTE: Global data only needs dataName, leave objectName null 
	 * NOTE: I had to put the implementation below it because templates */
	/*
	* Read a value from the json
	* args: data - The name of data to read
	* output: The data value from the json
	*/
	template <typename T> inline T Read(char *objectName, char *dataName)
	{
		/* If the data is attached to an object, return it */
		if (objectName)
		{
			return (*configFile_)[objectName][dataName];
		}
		/* If the data is global, just straight pull the data */
		else
		{
			return (*configFile_)[dataName];
		}
	}

	/* Puts a data value into serializer 
	 * NOTE: Global data only needs dataName and data
	 * NOTE: dataName does not need to be already existing in the json
	 * NOTE: I had to put the implementation below it because templates * /
	/*
	 * Write a value to the json
	 * args: name - the name of the data in the json
			 data - the actual data to write to the json
	 * output: The data value from the json
	 */
	template <typename T> inline void Write(char *objectName, char *dataName, T data)
	{
		/* If there's an object name, write data to the object */
		if (objectName)
		{
			/* Write data to json */
			(*configFile_)[objectName][dataName] = data;
		}
		/* Otherwise, write data globally */
		else
		{
			(*configFile_)[dataName] = data;
		}
	}

	/* Reads an entire object from the JSON file */
	void ReadObject(GameObject & object);

	/* Writes an entire object to the JSON file */
	void WriteObject(GameObject &object) const;

	/* Rename an object in the serializer */
	void Delete(const std::string objectToDelete) const;

	/* Add an object to the serializer list */
	void addToObjectList(std::string newObjectName);

	/* Serializes data to JSON file */
	void Serialize(void);

	/* Creates objects based on what's in the JSON file */
	void CreateFromFile(void);

	/* Return the object list */
	std::vector<std::string> & GetObjectList();

	/* Set the name of the serializer */
	void SetName(std::string newName);

	/* Get the name of the serializer */
	std::string GetName(void);

	/* Reads the input functions with serializer */
	void ReadInput(std::string inputFile);

	/* Creates a new json file with a given name */
	void CreateJSON(std::string fileName);

	/* Clear the list of objects */
	void ClearObjectList(void);

	/* Cleans up serializer						
	 * NOTE: Does not serialize, pls call Write */
	~Serializer();


private:
	/* All the data */
	json *configFile_;
	std::string fileName_;
	std::vector<std::string> objectNames_;

};

/* Takes component enum and turns it to string */
std::string componentEnumToString(Component::ComponentTypes type);
/* Takes component string and turns it to enum */
Component::ComponentTypes componentStringToEnum(std::string string);
/* Takes behavior enum and turns it to string */
std::string BehaviorTypeEnumToString(BehaviorTypes type);
/* Takes string and gives behavior enum */
BehaviorTypes BehaviorTypeStringToEnum(std::string string);


