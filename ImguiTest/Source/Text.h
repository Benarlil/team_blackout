//------------------------------------------------------------------------------
//
// File Name:	Text.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

#include "Sprite.h"

class Font;

class Text : public Sprite {
public:
  Text(std::string font_path, int char_width, std::string mesh_name = "mesh1x1", int char_height = 0);
  Text(Font& font, std::string mesh_name = "mesh1x1");
  Text(const Text& text_component);

  void Update(float dt) override;

  /* 
     Creates a texture of the specified string. 
     Note: this is an extremely costly operation and should
     idealy be performed rarely (during loading or initializations)
  */
  void SetString(std::string new_string, bool init = false);
  std::string GetString();
private:
  Font& font;
  std::string string;
};