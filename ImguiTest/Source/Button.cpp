//------------------------------------------------------------------------------
//
// File Name:	ColliderCircle.cpp
// Author(s):	william patrick
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Button.h"
#include "GameObject.h"
#include "GameObjectManager.h"
#include "Physics.h"
#include "Mesh.h"
#include "Graphics.h"
#include "ColliderCircle.h"


void ButtonInit(char * objectName)
{
  GameObject& gO = GameObjectManagerCreateObject(objectName); // make the game object a shared pointer

  glm::vec3 vecc(0.0f, 1.0f, 0.0f);
  // !!TRANSFORM SHOULD BE ADDED BEFORE PHYSICS OR EVERYTHING CRASHES!!
  Transform transform = Transform(vecc.x, vecc.y);
  transform.SetScale(glm::vec3(1.0f, 1.0f, 0.0f));

  gO.Add(std::make_shared<Transform>(transform));

  gO.Add(std::make_shared<Physics>(Physics(200.0f)));

  Mesh mesh = Mesh(1.0f, 1.0f, 1.0f, "Source/w_square.png");
  gO.Add(std::make_shared<Mesh>(mesh));

  gO.Add(std::make_shared<ColliderCircle>(ColliderCircle(transform.GetScale().x / 2)));

  Graphics::addToGraphics(std::dynamic_pointer_cast<Mesh>(gO.Get(Component::ctMesh)));
}

void MouseHover(GameObject& button, sf::Vector2i& mousePos)
{
  std::shared_ptr<ColliderCircle> buttonCollider = std::dynamic_pointer_cast<ColliderCircle>(button.Get(Component::ctCollider));
  std::shared_ptr<Mesh> buttonMesh = std::dynamic_pointer_cast<Mesh>(button.Get(Component::ctMesh));
  if (Collider::Check(*buttonCollider, mousePos))
  {
    buttonMesh->setColor(buttonMesh, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
  }
}


void ButtonUpdate(sf::Event& event, char * objectName)
{
  sf::Vector2i mousePos;
  switch (event.type)
  {
    case sf::Event::MouseButtonPressed:
    {
     switch (event.mouseButton.button)
      {
      case sf::Mouse::Left:
      {
      }break;
      }
    }break;
    case sf::Event::MouseMoved:
    {
      mousePos = sf::Mouse::getPosition();
    }break;
  }
  GameObject& button = GameObjectManagerFindObject(objectName);
  MouseHover(button, mousePos);
}


void ButtonShutdown()
{

}