//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorEnemy.cpp                                                 //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "BehaviorEnemy.h"
#include "Component.h"
#include "Physics.h"
#include "Graphics.h"
#include "Camera.h"
#include "Input.h"
#include "Movement.h"
#include "BehaviorWeapon.h"
#include "BehaviorPlayer.h"
#include "GameObject.h"
#include "GameObjectManager.h"
#include "Sprite.h"

#define TIMER_MAX 300 // The number of loops before moving again
static void* CollisionHandler(BehaviorEnemy& bp, GameObject& object);

BehaviorEnemy::BehaviorEnemy() : BehaviorBase(btEnemy)
{
  _MaxAccel = EDEFAULT_MAX_ACCELERATION;
  _MaxVel = EDEFAULT_MAX_VELOCITY;
  _Health = ENEMY_DEFAULT_HEALTH;
  _StateCurr = bpInvalid;
  _StateNext = bpMoving;
  _ShiftTimer = 0;
}

BehaviorEnemy::BehaviorEnemy(int health) : BehaviorBase(btEnemy)
{
  _Health = health;
  _StateCurr = bpInvalid;
  _StateNext = bpMoving;
  _ShiftTimer = 0;
}

BehaviorEnemy::~BehaviorEnemy()
{

}
int BehaviorEnemy::GetHealth()
{
  return _Health;
}
void BehaviorEnemy::SetHealth(int health)
{
  _Health = health;
}
void BehaviorEnemy::TakeDamage(int damage)
{
  if (_Health > 0)
    _Health -= damage;
}

void BehaviorEnemy::EnemyCollide(GameObject& object)
{
  GameObject* parent = &Parent();
  GameObject* obj = &object;


  std::shared_ptr<BehaviorBase> behavior = std::dynamic_pointer_cast<BehaviorBase>(object.Get(Component::ctBehavior));

  if (behavior->GetType() == btWeapon)
  {
      std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));
      std::shared_ptr<Physics> wphysics = std::dynamic_pointer_cast<Physics>(object.Get(Component::ctPhysics));

      //int damage = (int)wphysics->CalculateForce();

      std::shared_ptr<BehaviorPlayer> pBehavior = std::dynamic_pointer_cast<BehaviorPlayer>(object.GetParent().Get(Component::ctBehavior));
      int playerHealth = pBehavior->GetHealth();
      if (playerHealth <= 75 && playerHealth != 0 && _Health != 0)
        playerHealth += 25;
      pBehavior->SetHealth(playerHealth);

      if(playerHealth <= 75 && playerHealth != 0 && _Health != 0)
        TakeDamage(20);


      pphysics->AddAcceleration(wphysics->GetVelocity());

  }

  if (behavior->GetType() == btPlayer)
  {
    std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));

    std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

    ptransform->SetTranslation(pphysics->GetOldPos());

  }
  if (behavior->GetType() == btProp)
  {
    std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));

    std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

    ptransform->SetTranslation(pphysics->GetOldPos());
  }
  if (behavior->GetType() == btWall)
  {
    std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));

    std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

    ptransform->SetTranslation(pphysics->GetOldPos());
  }
}

void BehaviorEnemy::SetStateNext(States state)
{
  _StateNext = state;
}

int BehaviorEnemy::GetStateCurr()
{
  return _StateCurr;
}

void BehaviorEnemy::Init()
{


}

void BehaviorEnemy::Update(float dt)
{
  PhysicsPtr physics = Parent().GetComponent(Physics);
  TransformPtr transform = Parent().GetComponent(Transform);
  static glm::vec3 move = { 0.0f,0.0f,0.0f };
  _StateCurr = _StateNext;

  if (_StateCurr == States::bpMoving)
  {
    double r = (double)rand() / RAND_MAX;
    double anotherR = (double)rand() / RAND_MAX;
    _ShiftTimer += dt;
    if (_ShiftTimer >= 1.0f)
    {
      if (r > 0.5)
      {
        if (anotherR < 0.5)
        {
          //pos x pos y
          move = glm::vec3(2.0, 2.0, 0.0f);
          _ShiftTimer = 0;
        }
        else
        {
          //neg x pos y
          move = glm::vec3(-2.0, 2.0, 0.0f);
          _ShiftTimer = 0;
        }
      }
      else
      {
        if (anotherR > 0.5)
        {
          //pos x neg y
          move = (glm::vec3(2.0, -2.0, 0.0f));
          _ShiftTimer = 0;
        }
        else
        {
          //neg x neg y
          move = (glm::vec3(-2.0, -2.0, 0.0f));
          _ShiftTimer = 0;
        }
      }
    }
    if (_Health <= 0)
      SetStateNext(bpDead);
  }
  if (_StateCurr == States::bpDead)
  {
    static float SpawnTimer = 0.0f;
    SpawnTimer += dt;
    std::shared_ptr<Sprite> sprite = std::dynamic_pointer_cast<Sprite>(Parent().Get(Component::ctSprite));
    std::shared_ptr<Transform> transform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));
    move = { 0.0f, 0.0f, 0.0f };
    transform->SetTranslation(glm::vec3(100.0f, 100.0f, 0.0f));
    sprite->SetAlpha(0);
    _Health = ENEMY_DEFAULT_HEALTH;
    if (SpawnTimer >= 5.0f)
    {
      transform->SetTranslation(glm::vec3(rand() % 10 - 12, rand() % 10 - 2, 0.0f));
      sprite->SetAlpha(1);
      SetStateNext(bpMoving);
      SpawnTimer = 0;
    }
  }
  physics->SetVelocity(move);
}

void BehaviorEnemy::Exit()
{

}

static void* CollisionHandler(BehaviorEnemy& bp, GameObject& object)
{
  bp.EnemyCollide(object);    
  return nullptr;
}

void BehaviorEnemy::SetHandler(GameObject& object)
{
  if (Parent().GetComponent(Collider))
  {
    std::function<void*(void)> f = [this, &object]() {
      CollisionHandler(*this, object);
      return nullptr; };
    Parent().GetComponent(Collider)->SetCollisionHandler(f);
  }
}