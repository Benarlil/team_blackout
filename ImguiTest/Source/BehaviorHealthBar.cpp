//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorHealthBar.cpp                                             //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "BehaviorHealthBar.h"
#include "GameObjectManager.h"
#include "Transform.h"
#include "Sprite.h"
#include "Animation.h"


BehaviorHealthBar::BehaviorHealthBar()
{
  _StateCurr = bpInvalid;
  _StateNext = bpInvalid;

  _ParentBehavior = std::shared_ptr<BehaviorPlayer>(NULL);
  _HealthMax = 0;
  _HealthCurr = 0;
  _scaleGoesLeft = true;
}

BehaviorHealthBar::BehaviorHealthBar(bool goesLeft, std::string gameObject)
{
  _StateCurr = bpInvalid;
  _StateNext = bpActive;

  GameObject& Parent = GameObjectManagerFindObject(gameObject);

  _ParentBehavior = std::dynamic_pointer_cast<BehaviorPlayer>(Parent.Get(Component::ctBehavior));
  _HealthMax = _ParentBehavior->GetHealth();
  _HealthCurr = _HealthMax;
  _scaleGoesLeft = goesLeft;
}

bool BehaviorHealthBar::GetIsLeft(void)
{
  return _scaleGoesLeft;
}

void BehaviorHealthBar::Update(float dt)
{
  std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>((Parent().Get(Component::ctTransform)));
  _StateCurr = _StateNext;
  _HealthCurr = _ParentBehavior->GetHealth();
  

  glm::vec3 scaleVec = ptransform->GetScale();
  if (_HealthMax != 0)
    scaleVec.x = ((float)(_HealthCurr) / (float)(_HealthMax));
  /* Comment your code, Alex, Jesus */
  /* Also you need a file header */

  /* Scale based on the beginning of the rectangle, not both sides */
  if (_scaleGoesLeft)
  {
    static glm::vec3 leftAffineScalePoint(-ptransform->GetScale().x / 2, 0.0f, 0.0f);
    ptransform->SetScale(scaleVec, leftAffineScalePoint);
  }

  else
  {
    static glm::vec3 rightAffineScalePoint(ptransform->GetScale().x / 2, 0.0f, 0.0f);
    ptransform->SetScale(scaleVec, rightAffineScalePoint);
  }
}
