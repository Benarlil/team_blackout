//------------------------------------------------------------------------------
//
// File Name:	SoundComponent.cpp
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "DebugLog.h"
#include "GameObject.h"
#include "SoundComponent.h"
#include "Transform.h"
#include <map>
#include "AudioList.h"

using AudioIterator = std::map<std::string, sf::Sound>::iterator; // iterator for the map of sounds

static sf::Time DUMMYTIME = sf::Time();

// Default constructor. Creates a sound component with default volume and pitch
Sound::Sound() : Component(Component::ctSound)
{
  volume_ = 100.0f; // Default volume of a Sound type is 100

  pitch_ = 1.0f; // Default pitch is 1

  DebugMessage("Default Sound Component created");
}

// Non-defualt constructor. Creates a sound component with a given volume and pitch. 
// Optionally, can pass in a position for the sound. It will default to (0,0,0).
Sound::Sound(float volume, float pitch, vec3 position = glm::vec3(0.0)) : Component(Component::ctSound)
{
  // if the volume given is between the accepted values
  if (volume >= 0 && volume <= 100)
    volume_ = volume;

  // otherwise set it to the default of 100
  else
    volume_ = 100.0f;

  // if the pitch given is between the accepted values
  if (pitch >= -3 && pitch <= 3)
    pitch_ = pitch;

  // otherwise set it to the default of 1
  else
    pitch_ = 1;

  // set the position
  position_ = position;

  DebugMessage("Sound Component created with volume = ", volume_);
  DebugMessage("and pitch = ", pitch_);
}

// Copy-constructor
Sound::Sound(Sound& copy) : Component(Component::ctSound)
{
  sounds_ = std::map<std::string, sf::Sound>(copy.sounds_); // Copy the map of sounds with the map copy constructor

  pitch_ = copy.pitch_; // The pitch to play each sound at

  volume_ = copy.volume_; // The volume to play each sound at

  position_ = copy.position_; // The position of the sound (for the listener to use, should we have one)

  DebugMessage("Sound Component copied with volume = ", volume_);
  DebugMessage("and pitch = ", pitch_);
}

// Destructor
Sound::~Sound()
{
  DebugMessage("Sound Component Destructor");

  sounds_.clear();
}

// Updates the position of the sound based on the game object
void Sound::Update(float dt)
{
  (dt); // reference dt

  TransformPtr transform; //setup a transform pointer for getting position

  // if the transform exists
  if (transform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform)))
  {
    position_ = transform->GetTranslation(); // set the position of the sound source to be that of the transform

    // for each sound in the component
    for (AudioIterator it = sounds_.begin(); it != sounds_.end(); ++it)
    {
      it->second.setPosition(position_.x, position_.y, position_.z); // set the position
    }
  }
  // if the transform could not be obtained, the sound will not move
}

// Add a sound to the vector of sounds within the component and return its ID
int Sound::Add(std::string audioFile)
{
  sounds_.insert(std::make_pair(audioFile, sf::Sound())); // Make the sound pair and add it to the map

  sounds_[audioFile].setBuffer(AudioListFind(audioFile));

  sounds_[audioFile].setPitch(pitch_); // Set the pitch of the sound to the component's set pitch

  sounds_[audioFile].setVolume(volume_); // Set the volume of the sound to the component's

  return (sounds_.size() - 1); // return the location of the sound as the ID (size - 1 as it starts from 0)
}

// Play one of the sounds in the vector using a given id
void Sound::Play(std::string audioFile)
{
  // Try to play the given sound
  try
  {
    sounds_[audioFile].play();
    DebugMessage("Playing Sound: ", audioFile);
  }

  // If the id was out of range, catch the exception and print the error
  catch (const std::out_of_range& except)
  {
    DebugMessage("Sound does not exist: ", audioFile);
    DebugMessage("Out of Range Error = ", except.what());
    DebugMessage("Could not play sound");
  }
}

// Set the volume of every sound in the vector
void Sound::SetVolumeAll(float volume)
{
  volume_ = volume; // set the component's volume

  // for each sound in the component
  for (AudioIterator it = sounds_.begin(); it != sounds_.end(); ++it)
  {
    (it)->second.setVolume(volume_); // set its individual volume
  }
}

// Set the volume of a single sound
void Sound::SetVolumeOne(float volume, std::string audioFile)
{
  // Try to set the volume of the sound
  try
  {
    sounds_[audioFile].setVolume(volume);

    DebugMessage("Setting Volume of sound: ", audioFile);
    DebugMessage("To Volume= ", volume);
  }

  // If the id was out of range, catch the exception and print the error
  catch (const std::out_of_range& except)
  {
    DebugMessage("Sound does not exist: ", audioFile);
    DebugMessage("Out of Range Error = ", except.what());
    DebugMessage("Could not set volume");
  }
}

// Set the pitch of every sound in the vector
void Sound::SetPitchAll(float pitch)
{
  pitch_ = pitch;

  for (AudioIterator it = sounds_.begin(); it != sounds_.end(); ++it)
  {
    (it)->second.setPitch(pitch_);
  }
}

// Set the pitch of a single sound
void Sound::SetPitchOne(float pitch, std::string audioFile)
{
  // Try to set the pitch of the sound
  try
  {
    sounds_[audioFile].setPitch(pitch);

    DebugMessage("Setting Pitch of sound: ", audioFile);
    DebugMessage("To Pitch= ", pitch);
  }

  // If the id was out of range, catch the exception and print the error
  catch (const std::out_of_range& except)
  {
    DebugMessage("Sound does not exist: ", audioFile);
    DebugMessage("Out of Range Error = ", except.what());
    DebugMessage("Could not set pitch");
  }
}

// set ONE SOUND to loop. There is no function to set every sound in a component to loop, as that could go very wrong very quickly
void Sound::SetLoop(bool loop, std::string audioFile)
{
  // Try to set the loop bool of the sound
  try
  {
    sounds_[audioFile ].setLoop(loop);

    DebugMessage("Setting Loop of sound: ", audioFile);
    DebugMessage("To ", loop);
  }

  // If the id was out of range, catch the exception and print the error
  catch (const std::out_of_range& except)
  {
    DebugMessage("Sound does not exist: ", audioFile);
    DebugMessage("Out of Range Error = ", except.what());
    DebugMessage("Could not set loop boolean");
  }
}

// Return the pitch of the component
float Sound::GetPitch()
{
  return pitch_;
}

// Return the volume of the component
float Sound::GetVolume()
{
  return volume_;
}

float Sound::GetVolumeOne(std::string name)
{
  return sounds_[name].getVolume();
}

float Sound::GetPitchOne(std::string name)
{
  return sounds_[name].getPitch();
}

// Gets every name from the component and returns them in a vector.
std::vector<std::string> Sound::GetNames()
{
  std::vector<std::string> soundNames;

  // for each sound in the component
  for (AudioIterator it = sounds_.begin(); it != sounds_.end(); ++it)
  {
    soundNames.push_back(it->first);
  }

  return soundNames;
}

// Gets how many sounds are in the component
unsigned int Sound::GetCount() 
{
  return sounds_.size();
}

sf::Time Sound::GetSoundLength(std::string audioFile)
{
  // Try to get the sound
  try
  {
    DebugMessage("Getting length of sound: ", audioFile); // print debug

    return sounds_[audioFile].getBuffer()->getDuration(); // get the buffer and then get the length of the sound
  }

  // If the id was out of range, catch the exception and print the error
  catch (const std::out_of_range& except)
  {
    DebugMessage("Sound does not exist: ", audioFile);
    DebugMessage("Out of Range Error = ", except.what());
    DebugMessage("Could not get sound length");
  }

  return DUMMYTIME;
}

void Sound::RemoveSound(std::string audioFile)
{
  // Try to remove the sound
  try
  {
    sounds_.erase(audioFile); // get the buffer and then get the length of the sound
  }

  // If the id was out of range, catch the exception and print the error
  catch (const std::out_of_range& except)
  {
    DebugMessage("Sound does not exist: ", audioFile);
    DebugMessage("Out of Range Error = ", except.what());
    DebugMessage("Could not remove sound");
  }
}