//------------------------------------------------------------------------------
//
// File Name:	ColliderCircle.cpp
// Author(s):	william patrick
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Collider.h"
#include "ColliderCircle.h"
#include "GameObject.h"


ColliderCircle::ColliderCircle() : Collider(ColliderTypeCircle)
{
	_Radius = 0.0f;
	_Offset = { 0.0f, 0.0f };
	_Inverse = false;
}


ColliderCircle::ColliderCircle(float radius, bool inverse) : Collider(ColliderTypeCircle)
{
	_Radius = radius;
	_Offset = { 0.0f, 0.0f };
	_Inverse = inverse;
}

ColliderCircle::ColliderCircle(glm::vec2 offset, bool inverse) : Collider(ColliderTypeCircle)
{
	_Radius = 0.f;
	_Offset = offset;
	_Inverse = inverse;
}

ColliderCircle::ColliderCircle(float radius, glm::vec2 offset, bool hasOffset) : Collider(ColliderTypeCircle)
{
	_Radius = radius;
	_Offset = offset;
	_HasOffset = hasOffset;
}

ColliderCircle::ColliderCircle(ColliderCircle& original) : Collider(ColliderTypeCircle)
{
	_Radius = original.GetRadius();
	_Offset = original.GetOffset();
	_HasOffset = original._HasOffset;
}

ColliderCircle::~ColliderCircle()
{

}

// Get the circle collider's radius.
// Returns:
//	 The radius of the Circle collider
float ColliderCircle::GetRadius()
{
	return _Radius;
}

glm::vec2 ColliderCircle::GetOffset()
{
	return _Offset;
}



// Set the circle collider's radius.
// Params:
//	 collider = Pointer to the circle collider component.
//   radius = the circle collider's new radius.
void ColliderCircle::SetRadius(float radius)
{
	_Radius = radius;
}

void ColliderCircle::SetOffset(glm::vec2 offset)
{
	_Offset = offset;
}

bool ColliderCircle::IsInverse()
{
	return _Inverse;
}

void ColliderCircle::SwapInverse(bool state)
{
	_Inverse = state;
}
