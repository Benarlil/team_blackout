//------------------------------------------------------------------------------
//
// File Name:	Font.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

struct GlyphRenderData {
  GlyphRenderData(sf::Uint8 *pixels, unsigned width, unsigned height);
  sf::Uint8 *pixels;
  unsigned width;
  unsigned height;
};

class Font {
public:
  /* 
    Constructor for the Font object. 
    font_path - relative path to the .ttf file 
    char_width - width of a character (if zero, gets set to be the same as height)
    char_height - height of a character (if zero, gets set to be the same as width)
  */
  Font(std::string font_path, int char_width, int char_height = 0);
  Font(Font&& other);
  ~Font();

  GlyphRenderData CharTo32Bitmap(FT_Char symbol);
private:
  static std::vector<sf::Uint8> pixels32bit;
  int ch_width;
  FT_Face facehdl;
};
