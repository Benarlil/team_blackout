//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorEnemy.h                                                   //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "Behavior.h"
#include "Collider.h"

#define ENEMY_DEFAULT_HEALTH 1;
#define EDEFAULT_MAX_VELOCITY 10.0f;
#define EDEFAULT_MAX_ACCELERATION 80.0f;

class BehaviorEnemy : public BehaviorBase
{
  float _MaxAccel;
  float _MaxVel;
  int _Health;
  int _StateCurr;
  int _StateNext;
  float _ShiftTimer;
public:

  enum States
  {
    bpInvalid,
    bpIdle,
    bpMoving,
    bpDead,
  };

  BehaviorEnemy();
  BehaviorEnemy(int health);
  ~BehaviorEnemy();
  int GetHealth();
  void SetHealth(int health);
  void TakeDamage(int damage);
  void EnemyCollide(GameObject& object);
  void SetStateNext(States state);
  int GetStateCurr();
  void Init();
  void Update(float dt);
  void SetHandler(GameObject& object);
  void Exit();
};