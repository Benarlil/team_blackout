//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	ControllerInput.cpp                                               //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "stdafx.h"
#include "ControllerInput.h"
#include "GameObjectManager.h"
#include <regex>
#include "Movement.h"
#include "Behavior.h"

#define DEAD_ZONE 20

ControllerSystem::ControllerData::ControllerData(const GameObject& player) : player(player)
{
  controllerID = -1; // initialize which controller this is
  availiability = true; // Set the controller to be availiable for binding to a player

  /*
   For each button and stick, initialize a data struct and insert
   it into the map
  */
  for (int i = 0; i < sf::Joystick::AxisCount; i++)
  {
    StickData axisData;
    if (i == ControllerSystem::Axis::LSTICK_X)
    {
      axisData.functionArg = (void*)(&player);
      axisData.PosPushedActionPtr = MovePlayerRight;
      axisData.NegPushedActionPtr = MovePlayerLeft;
      axisData.posDirection = false;
      axisData.negDirection = false;
    }
    else if (i == ControllerSystem::Axis::LSTICK_Y)
    {
      axisData.functionArg = (void*)(&player);
      axisData.PosPushedActionPtr = MovePlayerUp;
      axisData.NegPushedActionPtr = MovePlayerDown;
      axisData.posDirection = false;
      axisData.negDirection = false;
    }
    else if (i == ControllerSystem::Axis::LTRIG)
    {
      axisData.functionArg = (void*)(&player);
      axisData.PosPushedActionPtr = RotatePlayerLeft;
      axisData.NegPushedActionPtr = RotatePlayerRight;
      axisData.posDirection = false;
      axisData.negDirection = false;
    }
    else
    {
      axisData.PosPushedActionPtr = NULL;
      axisData.NegPushedActionPtr = NULL;
      axisData.posDirection = false;
      axisData.negDirection = false;
    }

    stickMap.insert({ Axis(i), axisData });
  }

  for (int i = 0; i < sf::Joystick::ButtonCount; i++)
  {
    ButtonData buttonData;
    buttonData.PressedActionPtr = NULL;
    buttonData.ReleasedActionPtr = NULL;
    buttonData.isPressed = false;
    buttonMap.insert({ Buttons(i), buttonData });
  }
}

ControllerSystem::ControllerSystem()
{
   // Get the names of all player objects
  std::vector<std::string> objectNameList; 
  // The list of player objects
  std::vector<const GameObject*> playerList;
  GameObjectManagerPopulateByBehaviorName(objectNameList, BehaviorTypes::btPlayer);

  // For each active object, if the player exists,
  // push it back into the list
  for (int i = 0; i < objectNameList.size(); i++)
  {
    GameObject& player = GameObjectManagerFindObject(objectNameList[i]);
    if (player.GetName() != "DUMMY")
      playerList.push_back(&player);
    else
    {
      throw(std::exception("Player not a valid object"));
    }
  }

  // For each player, initialize a controller
  std::for_each(playerList.begin(), playerList.end(), [this](const GameObject* g) { controllerDataList.push_back(ControllerData(*g)); });

  // For each controller, if the corresponding controller is connected,
  // update the ID of it as well as the availiability
  for (int i = 0; i < controllerDataList.size(); i++)
  {
    if (sf::Joystick::isConnected(i))
    {
      controllerDataList[i].availiability = false;
      controllerDataList[i].controllerID = i;
    }

  }
}

void ControllerSystem::Update(sf::Event& event)
{
  if (event.type == sf::Event::JoystickConnected)
  {
    for (int i = 0; i < controllerDataList.size(); i++)
    {
      if (controllerDataList[i].availiability == true)
      {
        controllerDataList[i].controllerID = event.joystickConnect.joystickId;
        controllerDataList[i].availiability = false;
        break;
      }
    }
  }

  for (int i = 0; i < controllerDataList.size(); i++)
  {
    if (event.joystickButton.joystickId == controllerDataList[i].controllerID)
    {
      if (event.type == sf::Event::EventType::JoystickButtonPressed)
      {
        /*
          -If the button is pressed, set the data flag to true
          -If the button is released, set the data flag to false
        */
        controllerDataList[i].buttonMap[(Buttons)event.joystickButton.button].isPressed = true;
      }
      else if (event.type == sf::Event::EventType::JoystickButtonReleased)
        controllerDataList[i].buttonMap[(Buttons)event.joystickButton.button].isPressed = false;

      /* Check the status of all of the sticks on the controller, and if the axis
         is greater than the dead zone area, set the respective flags so that the
         exec function can call the correct functionss */
      if (event.joystickMove.axis > -1 && event.joystickMove.axis < sf::Joystick::AxisCount)
      {

        if (sf::Joystick::getAxisPosition(event.joystickMove.joystickId, event.joystickMove.axis) > DEAD_ZONE)
        {
          if (event.joystickMove.axis == sf::Joystick::Axis::Y)
          {
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].posDirection = false;
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].negDirection = true;
          }
          else
          {
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].posDirection = true;
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].negDirection = false;
          }
        }
        else if (sf::Joystick::getAxisPosition(event.joystickMove.joystickId, event.joystickMove.axis) < -DEAD_ZONE)
        {
          if (event.joystickMove.axis == sf::Joystick::Axis::Y)
          {
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].posDirection = true;
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].negDirection = false;
          }
          else
          {
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].posDirection = false;
            controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].negDirection = true;
          }
        }
        if ((sf::Joystick::getAxisPosition(event.joystickMove.joystickId, event.joystickMove.axis) > -DEAD_ZONE) && (sf::Joystick::getAxisPosition(event.joystickMove.joystickId, event.joystickMove.axis) < DEAD_ZONE))
        {
          controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].posDirection = false;
          controllerDataList[i].stickMap[(Axis)event.joystickMove.axis].negDirection = false;
        }
      }
    }
  }
}


void * ControllerSystem::GetArguments(int controllerIndex, Axis axis)
{
  return controllerDataList[controllerIndex].stickMap[axis].functionArg;
}

void * ControllerSystem::GetArguments(int controllerIndex, Buttons button)
{
  return controllerDataList[controllerIndex].buttonMap[button].functionArg;
}

void ControllerSystem::SetArguments(int controllerIndex, Axis axis, void * args)
{
  controllerDataList[controllerIndex].stickMap[axis].functionArg = args;
}

void ControllerSystem::SetArguments(int controllerIndex, Buttons button, void * args)
{
  controllerDataList[controllerIndex].buttonMap[button].functionArg = args;
}

void ControllerSystem::AssignPosPushedAction(int controllerIndex, Axis axis, std::function<void(void*)>funcptr)
{
  controllerDataList[controllerIndex].stickMap[axis].PosPushedActionPtr = funcptr;
}

void ControllerSystem::AssignNegPushedAction(int controllerIndex, Axis axis, std::function<void(void*)>funcptr)
{
  controllerDataList[controllerIndex].stickMap[axis].NegPushedActionPtr = funcptr;
}

void ControllerSystem::AssignPressedAction(int controllerIndex, Buttons button, std::function<void(void*)>funcptr)
{
  controllerDataList[controllerIndex].buttonMap[button].PressedActionPtr = funcptr;
}

void ControllerSystem::AssignReleasedAction(int controllerIndex, Buttons button, std::function<void(void*)>funcptr)
{
  controllerDataList[controllerIndex].buttonMap[button].ReleasedActionPtr = funcptr;
}

void ControllerSystem::RemapPressedAction(Buttons button, std::function<void(void*)>funcptr)
{
  for (int i = 0; i < controllerDataList.size(); i++)
  {
    controllerDataList[i].buttonMap[button].PressedActionPtr = funcptr;
  }
}

void ControllerSystem::RemapReleasedAction(Buttons button, std::function<void(void*)>funcptr)
{
  for (int i = 0; i < controllerDataList.size(); i++)
  {
    controllerDataList[i].buttonMap[button].ReleasedActionPtr = funcptr;
  }
}

void ControllerSystem::RemapPosPressedAction(Axis axis, std::function<void(void*)>funcptr)
{
  for (int i = 0; i < controllerDataList.size(); i++)
  {
    controllerDataList[i].stickMap[axis].PosPushedActionPtr = funcptr;
  }
}

void ControllerSystem::RemapNegPressedAction(Axis axis, std::function<void(void*)>funcptr)
{
  for (int i = 0; i < controllerDataList.size(); i++)
  {
    controllerDataList[i].stickMap[axis].NegPushedActionPtr = funcptr;
  }
}






ControllerSystem::ControllerData& ControllerSystem::operator[](int controllerIndex)
{
  return controllerDataList[controllerIndex];
}


void ControllerSystem::Exec()
{
  for (int i = 0; i < controllerDataList.size(); i++)
  {
    for (const auto s : controllerDataList[i].stickMap)
    {
      const auto& stick = s.first;
      const auto& stickData = s.second;

      if (stickData.posDirection == true && stickData.PosPushedActionPtr)
      {
        stickData.PosPushedActionPtr(stickData.functionArg);
      }
      if (stickData.negDirection == true && stickData.NegPushedActionPtr)
      {
        stickData.NegPushedActionPtr(stickData.functionArg);
      }
    }

    for (const auto b : controllerDataList[i].buttonMap)
    {
      const auto& button = b.first;
      const auto& buttonData = b.second;

      if (buttonData.isPressed == true && buttonData.PressedActionPtr)
      {
        buttonData.PressedActionPtr(buttonData.functionArg);
      }

      if (buttonData.isPressed == false && buttonData.ReleasedActionPtr)
      {
        buttonData.ReleasedActionPtr(buttonData.functionArg);
      }
    }
  }
}

bool ControllerSystem::GetPressedFlag(int controllerIndex, Buttons button)
{
  return controllerDataList[controllerIndex].buttonMap[button].isPressed;
}

bool ControllerSystem::GetPressedFlag(int controllerIndex, Axis axis)
{
  return controllerDataList[controllerIndex].stickMap[axis].isPressed;
}

bool ControllerSystem::GetPosDirectionFlag(int controllerIndex, Axis axis)
{
  return controllerDataList[controllerIndex].stickMap[axis].posDirection;
}

bool ControllerSystem::GetNegDirectionFlag(int controllerIndex, Axis axis)
{
  return controllerDataList[controllerIndex].stickMap[axis].negDirection;
}