//------------------------------------------------------------------------------
//
// File Name:	ColliderBox.h
// Author(s):	William Patrick
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once
#include "Collider.h"

class ColliderBox : public Collider
{

	float _Width;
	float _Height;
	bool _IsInverse;

public:

	ColliderBox();

	ColliderBox(float width = 0.f, float height = 0.f);

	ColliderBox(bool inverseStatus, float width = 0.f, float height = 0.f);

	ColliderBox(bool inverseStatus);

	ColliderBox(ColliderBox& original);

	~ColliderBox();

	float GetWidth();

	float GetHeight();
	
	void SetWidth(float width);

	void SetHeight(float height);

	void SetInverseStatus(bool status);

	void SwapInverseStatus();

	bool GetInverseStatus();

};