// Flail_Fighters.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GameObject.h"
#include "DebugLog.h"
#include "Graphics.h"
#include "Mesh.h"
#include "Transform.h"
#include "GameObjectManager.h"
#include "GameStateManager.h"
#include "Physics.h"
#include "Engine.h"
#include "Serialize.h"
#include "Input.h"
#include "Level1.h"
#include "AudioList.h"
#include "SoundComponent.h"

#include "Gui.h"
#include "Shader.h"


sf::Event gEvent;

int main()
{

	sf::Clock deltaClock;

	Graphics::Init();

	Graphics::GetMainWindow().setActive();

	Engine::EngineInit();

	DebugInit();

	ImGuiInit();

	AddAllSounds();

	//GameObjectManagerAddObject(gO);

	//Serializer serializer("config.json");

	//serializer.CreateFromFile();

	bool imGuiFlag = false;

	

	//std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(((gO).Get(Component::ctPhysics)));
	while (Graphics::GetMainWindow().isOpen())
	{

		


		float dt = deltaClock.restart().asSeconds();
    Engine::EngineUpdate(dt);

		while (Graphics::GetMainWindow().pollEvent(gEvent))
		{

			ImGuiProcessEvent(gEvent);
	      Engine::GameStateManager::GetCurrent().OnAction(gEvent, dt);
			switch (gEvent.type) {
			case sf::Event::KeyPressed: {
        switch (gEvent.key.code) {
          //	case sf::Keyboard::Right: {
              //physics->SetAcceleration(vec3(100.0f, 0.0f, 0.0f));
            //} break;

        case sf::Keyboard::RControl: {
			if (imGuiFlag)
			{
				imGuiFlag = false;
			}
			else
			{
				imGuiFlag = true;
			}

        } break;
        }
			} break;
      case sf::Event::Resized: {
        Graphics::ResizeWindow(gEvent.size);
      } break;
			case sf::Event::Closed: {
        Engine::GameStateManager::Go(Engine::GAMESTATES::Quit);
				
			} break;
			}
		}


		GameObjectManagerUpdate(dt);

		Graphics::Update();

		extern Serializer serializerLevel1;
		ImGuiUpdate(imGuiFlag, deltaClock, serializerLevel1);

		Graphics::GetMainWindow().display();
	}

	ImGuiShutdown();
  GameObjectManagerShutdown();
	return 0;
}

//typedef int keycode;

//template<typename fnptr>
//void foo(fnptr func, keycode val)
//{
  //if (SFML.keypressed(val))
	//func();
//}
