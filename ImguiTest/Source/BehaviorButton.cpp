//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorButton.h                                                  //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// The behavior for the cursor object for menus.                                //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "BehaviorButton.h"
#include "stdafx.h"
#include "DebugLog.h"
#include "Sprite.h"
#include "Collider.h"
#include "FunctionHandler.h"
#include "ControllerInput.h"

extern sf::Event gEvent;


  static void DebugFunction(void * unused)
  {
    DebugMessage("On Click is working!");
  }

  static void* CollideHandler(BehaviorButton& bb)
  {
    bb.onCollide();
    return nullptr;
  }

  // Default constructor that sets the mode to mouse
  BehaviorButton::BehaviorButton()
  {
    DebugMessage("Base ButtonBehavior made. Change the function pointers!");
    //[this]() { this->onCollide(); }
  }

  void BehaviorButton::setColliderHandler()
  {
    Parent().GetComponent(Collider)->SetCollisionHandler(std::bind(CollideHandler, *this));
  }

  // Constructor that takes the Hover and Click actions
  BehaviorButton::BehaviorButton(func_object onClick)
  {
    onClick_ = onClick;
    DebugMessage("Button made and function set");
  }

  // Update function that will change to another button or press a button based on input
  void BehaviorButton::Update(float dt)
  {
    // Get the sprite
    SpritePtr buttonSprite = Parent().GetComponent(Sprite);

    // If the button sprite actually exists
    if (buttonSprite)
    {
      buttonSprite->SetTexture(defaultTexture_, defaultPath_, true, Parent().GetComponent(Transform)->GetMatrix()); // set the colors to default
    }

  }

  void BehaviorButton::setDefaultTexture(std::string texture, std::string path)
  {
    defaultTexture_ = texture;
    defaultPath_ = path;
  }

  void BehaviorButton::setSelectedTexture(std::string texture, std::string path)
  {
    selectedTexture_ = texture;
    selectedPath_ = path;
  }

  void* BehaviorButton::onCollide(void)
  {
     // Get the sprite
    SpritePtr buttonSprite = Parent().GetComponent(Sprite);

    // If the button sprite actually exists
    if (buttonSprite)
    {
      buttonSprite->SetTexture(selectedTexture_, selectedPath_, true, Parent().GetComponent(Transform)->GetMatrix()); // set highlight color
    }

    // if click
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) || gEvent.joystickButton.joystickId < 10 && (gEvent.joystickButton.button <= ControllerSystem::BUTTONCOUNT && sf::Joystick::isButtonPressed(gEvent.joystickButton.joystickId, 0)))
                                   
    {
      onClick_(); // do on click action
    }

    return nullptr; 
  }

  void BehaviorButton::setOnClick(func_object function)
  {
    DebugMessage("OnClick function set for object: ", Parent().GetName());
    onClick_ = function;
  }