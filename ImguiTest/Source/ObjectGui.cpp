//------------------------------------------------------------------------------
//
// File Name:	ObjectGui.cpp
// Author(s):	Jack Klein (jack.klein)
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Imgui/imgui.h"
#include "imgui-SFML.h"
#include "Gui.h"
#include "ObjectGui.h"
#include "Graphics.h"
#include "Engine.h"
#include "GameStateManager.h"

#include "Transform.h"
#include "Collider.h"
#include "ColliderCircle.h"
#include "Serialize.h"
#include "GameObjectManager.h"
#include "Physics.h"
#include "Sprite.h"
#include "SoundComponent.h"
#include "DebugObject.h"
#include "Animation.h"

#include "Behavior.h"
#include "BehaviorPlayer.h"
#include "BehaviorWeapon.h"
#include "BehaviorButton.h"
#include "BehaviorCursor.h"
#include "BehaviorEnemy.h"
#include "BehaviorHealthBar.h"
#include "BehaviorProp.h"
#include "BehaviorIcon.h"
#include "BehaviorWall.h"

#include <sstream>

#include "Imgui/imgui.h"
#include "Imgui/imgui-SFML.h"
#include "Imgui/imgui_impl_opengl3.h"


extern std::vector<int> isObjectOpen;
static constexpr int MAX_NAME_LENGTH = 50;
static constexpr int LENGTH_OF_JSON_PATH = 7;
static constexpr int LENGTH_OF_SOUND_PATH = 9;
static constexpr int LENGTH_OF_ART_PATH = 9;
static constexpr int NUM_OF_COMPONENTS = 7;



/*
 * Creates an ImGui window for a game object
 * args: object - game object to create the window for
     serializer - The serializer to use
     clock - the current clock to get dt
 * outputs: None
 */
void CreateObjectGui(GameObject & object, Serializer & serializer, sf::Clock clock)
{
  ImGui::Begin(object.GetName().c_str());

  ImGui::SetWindowFontScale(1.5f);

  /* Rename object */
  std::string oldName = object.GetName();
  static char newName[MAX_NAME_LENGTH];
  static bool onStart = true;
  if (onStart)
  {
    oldName.copy(newName, oldName.length());
    newName[oldName.length()] = 0;
    onStart = false;
  }

  static bool noRename = false;
  ImGui::InputText("Name", newName, MAX_NAME_LENGTH);
  if (ImGui::Button("Rename"))
  {

    /* Make sure name doesn't already exist */
    for (unsigned int i = 0; i < serializer.GetObjectList().size(); i++)
    {
      if (newName == serializer.GetObjectList()[i])
      {
        noRename = true;
        break;
      }
      else
      {
        noRename = false;
      }
    }

    if (!noRename)
    {
      /* Rename object */
      object.Rename(newName);

      /* Find the object in the list for renaming */
      for (std::vector<std::string>::iterator it = serializer.GetObjectList().begin(); it != serializer.GetObjectList().end(); ++it)
      {
        if (oldName == *it)
        {
          *it = newName;
        }
      }

      /* Rename the object for serialization */
      serializer.Delete(oldName);
    }
  }
  if (noRename)
  {
    /* Error message for renaming */
    ImGui::Text("Name already exists!");
  }

  /* Skip a line between components */
  ImGui::Separator();
  ImGui::Text("");

  int i = 0;
  int count = 0;
  /* Read through all components */
  while (count < object.GetComponentCount())
  {
    /* If there exists a component of a certain type */
    if (object.Get(static_cast<Component::ComponentTypes>(i)))
    {
      /* Read component data */
      switch (static_cast<Component::ComponentTypes>(i))
      {

        /* Transform component */
      case Component::ComponentTypes::ctTransform:
      {
        /* Get the transform */
        TransformPtr objectTransform = std::dynamic_pointer_cast<Transform>(object.Get(Component::ComponentTypes::ctTransform));

        /* Make the ImGui info */
        if (ImGui::TreeNode("Transform"))
        {

          /* Translation slider */
          float translation[3];
          glm::vec3 newTranslation;
          translation[0] = objectTransform->GetTranslation().x;
          translation[1] = objectTransform->GetTranslation().y;
          translation[2] = objectTransform->GetTranslation().z;
          ImGui::DragFloat3("Translation", translation, 0.01f, -10, 10);
          newTranslation.x = translation[0];
          newTranslation.y = translation[1];
          newTranslation.z = translation[2];
          objectTransform->SetTranslation(newTranslation);

          /* Rotation float */
          float newRotation = objectTransform->GetRotation();
          ImGui::DragFloat("Rotation", &newRotation, 0.1f, 0, 2 * 3.14f);
          objectTransform->SetRotation(newRotation);

          /* Scale float */
          float scale[2];
          glm::vec3 newScale;
          scale[0] = objectTransform->GetScale().x;
          scale[1] = objectTransform->GetScale().y;
          ImGui::DragFloat2("Scale", scale, 0.1f, 0.0f, 5.0f);
          newScale.x = scale[0];
          newScale.y = scale[1];
          newScale.z = 0;
          objectTransform->SetScale(newScale);

          /* Skip a line between components */
          ImGui::Separator();
          ImGui::Text("");

          ImGui::TreePop();
        }


        /* Increment component added count */
        count++;

        break;
      }

      case Component::ComponentTypes::ctSprite:
      {
        /* Get Sprite */
        SpritePtr objectSprite = std::dynamic_pointer_cast<Sprite>(object.Get(Component::ComponentTypes::ctSprite));

        /* Make the ImGui info */
        if (ImGui::TreeNode("Sprite"))
        {

          ImGui::Text(objectSprite->GetTextureName().c_str());

          if (ImGui::BeginMenu("Change Texture"))
          {
            ImGui::SetWindowFontScale(1.5f);
            //std::vector<int> subDirectoryBools;
            std::vector<int> artAssetBools;
            /* Look into directory sounds for all subdirectories */
            std::experimental::filesystem::path artDirectory = ".\\Assets";
            int subFileCount = 0;
            for (auto it : std::experimental::filesystem::directory_iterator(artDirectory))
            {
              std::ostringstream oss;
              oss << it;
              std::string path = oss.str();

              /* Create ImGui menu for each art directory */
              if (ImGui::BeginMenu((path.substr(LENGTH_OF_ART_PATH, path.size() - LENGTH_OF_ART_PATH)).c_str()))
              {
                ImGui::SetWindowFontScale(1.5f);
                /* Look into subdirectory for all art assets */
                std::experimental::filesystem::path artSubDirectory = it.path();
                int artAssetCount = 0;
                for (auto it2 : std::experimental::filesystem::directory_iterator(artSubDirectory))
                {
                  std::ostringstream oss2;
                  oss2 << it2;
                  std::string path2 = oss2.str();

                  /* Create ImGui menu for each art asset */
                  artAssetBools.push_back(false);
                  std::string assetName = path2.substr(path.size() + 1, path2.size() - path.size());

                  ImGui::MenuItem((assetName).c_str(), nullptr, reinterpret_cast<bool*>(&(artAssetBools[artAssetCount])));

                  if (artAssetBools[artAssetCount])
                  {
                    objectSprite->SetTexture(assetName, path2);
                  }

                  /* Increment art asset counter */
                  artAssetCount++;
                }

                ImGui::EndMenu();
              }

              /* Increment sub directory counter */
              subFileCount++;
            }

            ImGui::EndMenu();
          }

          /* Changing UV */
          static float uv[2] = { objectSprite->GetUVOffset()[0] , objectSprite->GetUVOffset()[1] };
          ImGui::DragFloat2("Change UV", uv, 0.1f, 0, 10);
          glm::vec2 newUV = { uv[0], uv[1] };
          objectSprite->SetUVOffset(newUV);

          /* Changing Rows and Columns */
          static int rowCols[2] = { objectSprite->GetSource().rows(), objectSprite->GetSource().cols() };
          ImGui::DragInt2("Change Rows and Columns", rowCols, 1.0f, 0, 10);
          glm::vec2 newRowCols = { rowCols[0], rowCols[1] };
          char temp[3];
          char temp2[3];
          itoa(rowCols[0], temp, 10);
          itoa(rowCols[1], temp2, 10);
          if (!objectSprite->GetMeshName().find_first_of("UI"))
          {
            objectSprite->SetMesh( std::string("UImesh") + temp + std::string("x") + temp2);
          }
          else
          {
            objectSprite->SetMesh( std::string("mesh") + temp + std::string("x") + temp2);
          }

          /* Set UI switch */
          bool isUI = (!objectSprite->GetMeshName().find_first_of("UI"));
          ImGui::Checkbox("UI Mesh", &isUI);

          /* If the ui box is checked, rename mesh and set it */
          if (isUI)
          {
            std::string newMeshName;
            std::string oldMeshName = objectSprite->GetMeshName();
            if (oldMeshName.find_first_of("UI"))
            {
              newMeshName = "UImesh" + oldMeshName.substr(oldMeshName.find_first_not_of("mesh"), oldMeshName.size() - oldMeshName.find_first_not_of("mesh"));
            }
            else
            {
              newMeshName = oldMeshName;
            }

            objectSprite->SetMesh(newMeshName);
          }
          /* UI box not checked, rename mesh without the UI and set it */
          else
          {
            std::string newMeshName;
            std::string oldMeshName = objectSprite->GetMeshName();

            if (oldMeshName.find_first_of("UI"))
            {
              newMeshName = oldMeshName;
            }
            else
            {
              newMeshName = "mesh" + oldMeshName.substr(oldMeshName.find_first_not_of("UImesh"), oldMeshName.size() - oldMeshName.find_first_not_of("UImesh"));
            }

            objectSprite->SetMesh(newMeshName);

          }


          /* Get color */
          glm::vec4 objectColor = objectSprite->GetColor();
          float colorValues[4] =
          {
            objectColor.r,
            objectColor.g,
            objectColor.b,
            objectColor.a
          };
          ImGui::ColorEdit4("Color", colorValues);

          /* Reset Color */
          objectSprite->SetColor(colorValues[0], colorValues[1], colorValues[2], colorValues[3]);

          /* Skip a line between components */
          ImGui::Separator();
          ImGui::Text("");

          ImGui::TreePop();
        }

        /* Increment component added count */
        count++;

        break;
      }

      case Component::ComponentTypes::ctAnimation:
      {
        /* Get Animation */
        AnimationPtr objectAnimation = std::dynamic_pointer_cast<Animation>(object.Get(Component::ComponentTypes::ctAnimation));
        /* Get Sprite too cause why not */
        SpritePtr objectSprite = std::dynamic_pointer_cast<Sprite>(object.Get(Component::ComponentTypes::ctSprite));

        /* Make the ImGui info */
        if (ImGui::TreeNode("Animation"))
        {
          /* Select animation from spritesheet list */
          /* TODO: Make a std::filesystem with all the spritesheets to select from/add in the future */

          /* Frame index int slider */
          int frameIndex = objectSprite->GetFrame();
          ImGui::DragInt("Frame Index", &frameIndex, 1.0f, 0, objectAnimation->GetFrameCount());
          if (!objectAnimation->IsRunning())
          {
            objectSprite->SetFrame(frameIndex);
          }

          /* Frame count int slider */
          int frameCount = objectAnimation->GetFrameCount();
          ImGui::DragInt("Frame Count", &frameCount, 1.0f, 1, 50);
          if (!objectAnimation->IsRunning())
          {
            objectAnimation->SetFrameCount(frameCount);
          }

          /* Frame delay float slider */
          float frameDelay = objectAnimation->GetFrameDelay();
          ImGui::DragFloat("Frame Delay", &frameDelay, .1f, 0, 3.0f);
          if (!objectAnimation->IsRunning())
          {
            objectAnimation->SetFrameDelay(frameDelay);
          }
          
          /* Frame duration float slider */
          float frameDuration = objectAnimation->GetFrameDuration();
          ImGui::DragFloat("Frame Duration", &frameDuration, .1f, 0, 3.0f);
          if (!objectAnimation->IsRunning())
          {
            objectAnimation->SetFrameDuration(frameDuration);
          }

          /* Looping checkbox */
          bool isLooping = objectAnimation->IsLooping();
          ImGui::Checkbox("Looping", &isLooping);
          objectAnimation->SetIsLooping(isLooping);

          /* Play button */
          if (ImGui::Button("Play animation"))
          {
            objectAnimation->Play(objectAnimation->GetFrameCount(), objectAnimation->GetFrameDuration(), objectAnimation->IsLooping());
          }
          ImGui::SameLine();
          if (ImGui::Button("Stop animation"))
          {
            objectAnimation->SetIsRunning(false);
          }

          /* Skip a line between components */
          ImGui::Separator();
          ImGui::Text("");

          ImGui::TreePop();
        }

        /* Increment component added count */
        count++;

        break;
      }

      case Component::ComponentTypes::ctPhysics:
      {
        /* Get physics */
        PhysicsPtr objectPhysics = std::dynamic_pointer_cast<Physics>(object.Get(Component::ComponentTypes::ctPhysics));

        /* Make the ImGui info */
        if (ImGui::TreeNode("Physics"))
        {

          /* Get mass */
          float objectMass = objectPhysics->GetMass();

          /* Get velocity */
          vec3 objectVelocity = objectPhysics->GetVelocity();
          float newObjectVelocity[2];
          newObjectVelocity[0] = objectPhysics->GetVelocity().x;
          newObjectVelocity[1] = objectPhysics->GetVelocity().y;

          /* Get acceleration */
          vec3 objectAcceleration = objectPhysics->GetAcceleration();
          float newObjectAcceleration[2];
          newObjectAcceleration[0] = objectPhysics->GetAcceleration().x;
          newObjectAcceleration[1] = objectPhysics->GetAcceleration().y;

          /* ImGui sets variables */
          ImGui::DragFloat("Mass", &objectMass, 10.0f, 0.0f, 500.0f);
          ImGui::DragFloat2("Velocity", newObjectVelocity, 1.0f, -50.0f, 50.0f);
          ImGui::DragFloat2("Acceleration", newObjectAcceleration, 1.0f, -50.0f, 50.0f);

          objectVelocity.x = newObjectVelocity[0];
          objectVelocity.y = newObjectVelocity[1];
          objectVelocity.z = 0;

          objectAcceleration.x = newObjectAcceleration[0];
          objectAcceleration.y = newObjectAcceleration[1];
          objectAcceleration.z = 0;

          objectPhysics->SetMass(objectMass);
          objectPhysics->SetVelocity(objectVelocity);
          objectPhysics->SetAcceleration(objectAcceleration);

          /* Skip a line between components */
          ImGui::Separator();
          ImGui::Text("");

          ImGui::TreePop();
        }

        /* Increment component added count */
        count++;

        break;
      }

      case Component::ComponentTypes::ctCollider:
      {
        /* Get collider */
        ColliderPtr tempObjectCollider = std::dynamic_pointer_cast<Collider>(object.Get(Component::ComponentTypes::ctCollider));

        /* Make the ImGui info */
        if (ImGui::TreeNode("Collider"))
        {

          /* Check collider type */
          Collider::ColliderType colliderType = tempObjectCollider->GetType();
          switch (colliderType)
          {

          case (Collider::ColliderType::ColliderTypeCircle):
          {
            /* Collider is circle, set radius */
            std::shared_ptr<ColliderCircle> objectCollider = std::dynamic_pointer_cast<ColliderCircle>(tempObjectCollider);
            float newRadius = objectCollider->GetRadius();
            ImGui::DragFloat("Collider Radius", &newRadius, 0.01f, 0, 50.0f);
            objectCollider->SetRadius(newRadius);
          }

          /* End switch */
          }

          /* Skip a line between components */
          ImGui::Separator();
          ImGui::Text("");

          ImGui::TreePop();
        }

        /* Increment component added count */
        count++;

        break;
      }

      case Component::ComponentTypes::ctSound:
      {
        /* Get Sound */
        SoundPtr objectSound = std::dynamic_pointer_cast<Sound>(object.Get(Component::ComponentTypes::ctSound));

        /* Make the ImGui info */
        if (ImGui::TreeNode("Sounds"))
        {
          /* ImGui sliders for volume and pitch */
          float newVolume = objectSound->GetVolume();
          ImGui::DragFloat("Volume", &newVolume, 1.0f, 0, 100);
          objectSound->SetVolumeAll(newVolume);

          float newPitch = objectSound->GetPitch();
          ImGui::DragFloat("Pitch", &newPitch, .05f, 0, 2);
          objectSound->SetPitchAll(newPitch);

          /* Get object sounds */
          std::vector<int> objectSoundBools;
          static float timeLeft = 0;
          for (unsigned int i = 0; i < objectSound->GetCount(); ++i)
          {
            /* ImGui menu for all sounds*/
            objectSoundBools.push_back(false);
            ImGui::MenuItem(objectSound->GetNames()[i].substr(LENGTH_OF_SOUND_PATH, objectSound->GetNames()[i].size() - LENGTH_OF_SOUND_PATH).c_str(), nullptr, reinterpret_cast<bool*>(&(objectSoundBools[i])));

            /* Play the sound if menu is selected */
            if (objectSoundBools[i])
            {
              /* Only play the sound if it another sound isn't playing */
              if (!timeLeft)
              {
                timeLeft = objectSound->GetSoundLength(objectSound->GetNames()[i]).asSeconds();
                objectSound->Play(objectSound->GetNames()[i]);
              }
            }
          }
          timeLeft -= clock.getElapsedTime().asSeconds();
          /* CLAMP */
          if (timeLeft <= 0)
          {
            timeLeft = 0;
          }

          std::vector<int> allSoundBools;
          static bool noAddSound = false; /* Bool for whether sound should be added or not */

          if (ImGui::BeginMenu("Add Sound"))
          {
            ImGui::SetWindowFontScale(1.5f);
            /* Look into directory sounds for all sounds */
            std::experimental::filesystem::path soundDirectory = ".\\Sounds";
            int soundCount = 0;
            for (auto it : std::experimental::filesystem::directory_iterator(soundDirectory))
            {
              std::ostringstream oss;
              oss << it;
              std::string path = oss.str();

              /* Create ImGui menu for each sound*/
              allSoundBools.push_back(false);
              ImGui::MenuItem((path.substr(LENGTH_OF_SOUND_PATH, path.size() - LENGTH_OF_SOUND_PATH)).c_str(), nullptr, reinterpret_cast<bool*>(&(allSoundBools[soundCount])));

              /* Add the sound to component if menu selected */
              if (allSoundBools[soundCount])
              {
                /* Make sure sound isn't already part of object */
                for (unsigned int i = 0; i < objectSound->GetCount(); i++)
                {
                  if (objectSound->GetNames()[i] == (path))
                  {
                    noAddSound = true;
                    break;
                  }
                  else
                  {
                    noAddSound = false;
                  }
                }

                if (!noAddSound)
                {
                  /* Add sound */
                  objectSound->Add(path);
                }
              }

              /* Increment sound counter */
              soundCount++;
            }

            ImGui::EndMenu();
          }
          if (noAddSound)
          {
            /* Error message */
            ImGui::Text("Sound already part of object!");
          }

          /* Subtract sounds */
          if (ImGui::BeginMenu("Delete Sound"))
          {
            ImGui::SetWindowFontScale(1.5f);
            /* Get object sounds */
            std::vector<int> objectSoundBoolsRemove;
            static float timeLeft = 0;
            for (unsigned int i = 0; i < objectSound->GetCount(); ++i)
            {
              /* ImGui menu for all sounds*/
              objectSoundBoolsRemove.push_back(false);
              ImGui::MenuItem(objectSound->GetNames()[i].substr(LENGTH_OF_SOUND_PATH, objectSound->GetNames()[i].size() - LENGTH_OF_SOUND_PATH).c_str(), nullptr, reinterpret_cast<bool*>(&(objectSoundBools[i])));

              if (objectSoundBools[i])
              {
                /* Remove the sound */
                objectSound->RemoveSound(objectSound->GetNames()[i]);
              }
            }

            ImGui::EndMenu();
          }

          /* Skip a line between components */
          ImGui::Separator();
          ImGui::Text("");

          ImGui::TreePop();
        }

        /* Increment component added count */
        count++;

        break;
      }

      case Component::ComponentTypes::ctBehavior:
      {
        /* Get behavior */
        BehaviorPtr behavior = std::dynamic_pointer_cast<BehaviorBase>(object.Get(Component::ComponentTypes::ctBehavior));

        /* Get behavior type */
        BehaviorTypes behaviorType = behavior->GetType();

        /* Make the ImGui info */
        if (ImGui::TreeNode("Behavior"))
        {

          switch (behaviorType)
          {

          case (BehaviorTypes::btPlayer):
          {
            /* Behavior is player, set health */
            std::shared_ptr<BehaviorPlayer> behaviorPlayer = std::dynamic_pointer_cast<BehaviorPlayer>(behavior);
            int newHealth = behaviorPlayer->GetHealth();
            ImGui::DragInt("Player Health", &newHealth, 1.0f, 0, 100);
            behaviorPlayer->SetHealth(newHealth);

            break;
          }
          case (BehaviorTypes::btWeapon):
          {
            /* Behavior is weapon, set length */
            std::shared_ptr<BehaviorWeapon> behaviorWeapon = std::dynamic_pointer_cast<BehaviorWeapon>(behavior);
            float newLength = behaviorWeapon->GetHandleLength();
            ImGui::DragFloat("Weapon Length", &newLength, 0.1f, 0, 5.0f);
            behaviorWeapon->SetHandleLength(newLength);

            break;
          }

          /* End switch */
          }

          /* Skip a line between components */
          ImGui::Separator();
          ImGui::Text("");

          ImGui::TreePop();
        }

        /* Increment component added count */
        count++;
        break;
      }

      /* End switch */
      }

      /* Go to next component type */
      i++;
    }
    else
    {
      /* Go to next component type */
      i++;
      continue;
    }

  }

  /* Adding some space to make it look better */
  ImGui::Separator();
  ImGui::Text("");

  if (ImGui::Button("Toggle Debug Draw"))
  {
    ToggleDebugObjects(object.GetName());
  }


  static bool addComponentErrorPopupFlag = false;
  static bool addPlayerBehaviorPopupFlag = false;
  static bool dontAddBehaviorPopupFlag = false;

  /* Adding components */
  if (ImGui::BeginMenu("Add Component"))
  {
    ImGui::SetWindowFontScale(1.5f);
    bool componentBools[NUM_OF_COMPONENTS] = { false }; /* Bool for each component for the menu */
    int currentMenuCount = 0;

    for (int i = 0; i < NUM_OF_COMPONENTS; i++)
    {
      /* If the component is not in the object, make a menu item for it */
      if (!object.Get(static_cast<Component::ComponentTypes>(i)) && static_cast<Component::ComponentTypes>(i) != Component::ComponentTypes::ctBehavior)
      {
        ImGui::MenuItem(componentEnumToString(static_cast<Component::ComponentTypes>(i)).c_str(), NULL, &(componentBools[i]));

        /* For each menu item created, make a component out of it if menu is pressed */
        if (componentBools[i])
        {
          switch (static_cast<Component::ComponentTypes>(i))
          {

          case Component::ComponentTypes::ctTransform:
          {
            /* Add base transform (to be overwritten) */
            glm::vec3 vecc(0.0f, 0.0f, 0.0f);
            (object).Add(std::make_shared<Transform>(vecc.x, vecc.y, vecc.z));
            break;
          }

          case Component::ComponentTypes::ctSprite:
          {
            /* Add base sprite only if transform exists */
            /*(gO).Add(std::make_shared<Sprite>(Sprite(Graphics::CreateNewMesh(), )));*/
            if (object.Get(Component::ComponentTypes::ctTransform))
            {
              object.Add(std::make_shared<Sprite>("mesh1x1"));
            }
            else
            {
              addComponentErrorPopupFlag = true;
            }

            break;
          }

          case Component::ComponentTypes::ctAnimation:
          {
            /* Add base animation only if sprite exists */
            if (object.Get(Component::ComponentTypes::ctSprite))
            {
              object.Add(std::make_shared<Animation>());
            }
            else
            {
              addComponentErrorPopupFlag = true;
            }

            break;
          }

          case Component::ComponentTypes::ctPhysics:
          {
            /* Add base physics only if transform exists */
            if (object.Get(Component::ComponentTypes::ctTransform))
            {
              (object).Add(std::make_shared<Physics>(200.0f));
              DebugVelocity(object);
            }
            else
            {
              addComponentErrorPopupFlag = true;
            }

            break;
          }

          case Component::ComponentTypes::ctCollider:
          {
            /* Add base collider */
            object.Add(std::make_shared<ColliderCircle>());
            /* Make collider debug object */
            DebugCollider(object);

            break;
          }

          case Component::ComponentTypes::ctSound:
          {
            /* Add base sound */
            object.Add(std::make_shared<Sound>());
            break;
          }

          case Component::ComponentTypes::ctBehavior:
          {

            break;
          }

          /* End switch */
          }
        }
        currentMenuCount++;
      }
      else if (!object.Get(static_cast<Component::ComponentTypes>(i)) && static_cast<Component::ComponentTypes>(i) == Component::ComponentTypes::ctBehavior)
      {
        if (ImGui::BeginMenu("Select Behavior Type"))
        {
          ImGui::SetWindowFontScale(1.5f);
          /* Bools for the adding behavior menu */
          std::vector<int> addBehaviorBools;

          /* Get a list of behavior types */
          for (int i = BehaviorTypes::btEmpty; i < BehaviorTypes::btMax; i++)
          {
            addBehaviorBools.push_back(false);
            if (BehaviorTypeEnumToString(static_cast<BehaviorTypes>(i)) != "Empty")
            {
              ImGui::MenuItem(BehaviorTypeEnumToString(static_cast<BehaviorTypes>(i)).c_str(), nullptr, reinterpret_cast<bool*>(&(addBehaviorBools[i])));
            }

            if (addBehaviorBools[i])
            {
              for (int j = BehaviorTypes::btEmpty; j < BehaviorTypes::btMax; j++)
              {
                if (static_cast<Component::ComponentTypes>(j) == Component::ComponentTypes::ctBehavior)
                {
                  /* Make sure the component has a transform */
                  if (!object.Get(Component::ComponentTypes::ctTransform))
                  {
                    addComponentErrorPopupFlag = true;
                  }

                  switch (static_cast<BehaviorTypes>(i))
                  {
                  case BehaviorTypes::btCursor:
                  {
                    object.Add(std::make_shared<BehaviorCursor>());
                    break;
                  }
                  case BehaviorTypes::btPlayer:
                  {
                    /*addPlayerBehaviorPopupFlag = true;*/
                    dontAddBehaviorPopupFlag = true;
                    break;
                  }
                  case BehaviorTypes::btWeapon:
                  {
                    object.Add(std::make_shared<BehaviorWeapon>());
                    break;
                  }
                  case BehaviorTypes::btEnemy:
                  {
                    object.Add(std::make_shared<BehaviorEnemy>());
                    break;
                  }
                  case BehaviorTypes::btHealthBar:
                  {
                    /*object.Add(std::make_shared<BehaviorHealthBar>());*/
                    dontAddBehaviorPopupFlag = true;
                    break;
                  }
                  case BehaviorTypes::btProp:
                  {
                    object.Add(std::make_shared<BehaviorProp>());
                    break;
                  }
                  case BehaviorTypes::btIcon:
                  {
                    /*object.Add(std::make_shared<BehaviorIcon>());*/
                    dontAddBehaviorPopupFlag = true;
                    break;
                  }
                  case BehaviorTypes::btWall:
                  {
                    object.Add(std::make_shared<BehaviorWall>());
                    break;
                  }

                  /* End switch */
                  }
                }
              }
            }
          }
          ImGui::EndMenu();
        }
      }
    }
    ImGui::EndMenu();
  }


  if (dontAddBehaviorPopupFlag)
  {
    ImGui::OpenPopup("Error Adding Behavior");
  }
  if (ImGui::BeginPopupModal("Error Adding Behavior", nullptr))
  {
    ImGui::Text("Can't Add Behavior Type!");
    if (ImGui::Button("OK"))
    {
      ImGui::CloseCurrentPopup();
      dontAddBehaviorPopupFlag = false;
    }

    ImGui::EndPopup();
  }

  if (addPlayerBehaviorPopupFlag)
    {
      ImGui::OpenPopup("Adding Player Behavior");
    }
    if (ImGui::BeginPopupModal("Adding Player Behavior", nullptr))
    {
      static int initialHealth = 100;
      ImGui::DragInt("Initial Player Health", &initialHealth, 1.0f, 0, 200);

      /* Set anchor */
      if (ImGui::BeginMenu("Set Second Camera Anchor"))
      {
        /* Copy the size of the bools already made to make new bools */
        static std::vector<int> isObjectOpenCopy = { false };
        isObjectOpenCopy.resize(isObjectOpen.size());

        ImGui::SetWindowFontScale(1.5f);
        /* Make menu for all objects */
        for (unsigned int i = 0; i < isObjectOpen.size(); ++i)
        {
          ImGui::Selectable(serializer.GetObjectList()[i].c_str(), reinterpret_cast<bool*>(&(isObjectOpenCopy[i])));

          /* If bool is selected make it the  */
          if (isObjectOpenCopy[i])
          {

          }
        }

        ImGui::EndMenu();
      }

      if (ImGui::Button("Create Player Behavior"))
      {
        ImGui::CloseCurrentPopup();
      }
      ImGui::SameLine();
      if (ImGui::Button("Just kidding don't do that"))
      {
        ImGui::CloseCurrentPopup();
      }

      ImGui::EndPopup();
    }


    if (addComponentErrorPopupFlag)
    {
      ImGui::OpenPopup("Error: No Transform");
      addComponentErrorPopupFlag = false;
    }

    /* If we tried to add a transform dependent component throw error */
    if (ImGui::BeginPopupModal("Error: No Transform", nullptr))
    {
      ImGui::Text("Error! Cannot add component without having transform!");
      if (ImGui::Button("OK"))
      {
        ImGui::CloseCurrentPopup();
      }

      ImGui::EndPopup();
    }

    /* Adding some space to make it look better */
    ImGui::Separator();
    ImGui::Text("");

    static bool removeComponentErrorPopupFlag = false;
    static bool dontDeleteBehaviorPopupFlag = false;

    /* Remove components */
    if (ImGui::BeginMenu("Remove Component"))
    {
      ImGui::SetWindowFontScale(1.5f);
      std::vector<int> componentsToRemove; /* Bool for all components to remove */
      int componentToRemoveCount = 0; /* Number of components to remove for bool checking */

      for (int i = 0; i < NUM_OF_COMPONENTS; i++)
      {
        /* If the object has the component, add to menu for removal */
        if (object.Get(static_cast<Component::ComponentTypes>(i)))
        {
          componentsToRemove.push_back(false);
          ImGui::MenuItem(componentEnumToString(static_cast<Component::ComponentTypes>(i)).c_str(), nullptr, reinterpret_cast<bool*>(&componentsToRemove[componentToRemoveCount]));

          if (componentsToRemove[componentToRemoveCount])
          {
            /* If it is a physics or collider remove debug object first */
            if (static_cast<Component::ComponentTypes>(i) == Component::ComponentTypes::ctPhysics)
            {

              DestroyVelocityDebugObject(object.GetName());
            }
            if (static_cast<Component::ComponentTypes>(i) == Component::ComponentTypes::ctCollider)
            {
              DestroyColliderDebugObject(object.GetName());
            }

            /* If it's a behavior do a bunch of cleaning first */
            if (static_cast<Component::ComponentTypes>(i) == Component::ComponentTypes::ctBehavior)
            {
              switch (static_cast<BehaviorTypes>(i))
              {
              case BehaviorTypes::btCursor:
              {
                /* None */
                break;
              }
              case BehaviorTypes::btPlayer:
              {
                /* Do some things */
                break;
              }
              case BehaviorTypes::btWeapon:
              {
                /* None */
                break;
              }
              case BehaviorTypes::btEnemy:
              {
                /* None */
                break;
              }
              case BehaviorTypes::btHealthBar:
              {
                /* Do some things */
                dontDeleteBehaviorPopupFlag = true;
                break;
              }
              case BehaviorTypes::btProp:
              {
                /* None */
                break;
              }
              case BehaviorTypes::btIcon:
              {
                /* None */
                break;
              }
              case BehaviorTypes::btWall:
              {
                /* None */
                break;
              }

              /* End switch */
              }
            }

            /* If it isn't transform just delete it */
            if (static_cast<Component::ComponentTypes>(i) != Component::ComponentTypes::ctTransform && !dontDeleteBehaviorPopupFlag)
            {
              object.RemoveComponent(static_cast<Component::ComponentTypes>(i));
            }
            else
            {
              /* Check to see if there is no sprite or physics */
              if (!object.Get(Component::ComponentTypes::ctSprite) && !object.Get(Component::ComponentTypes::ctPhysics))
              {
                object.RemoveComponent(Component::ComponentTypes::ctTransform);
              }
              else
              {
                removeComponentErrorPopupFlag = true;
              }
            }
          }

          /* Increment menu counter */
          componentToRemoveCount++;
        }

      }

      ImGui::EndMenu();
    }

    if (dontDeleteBehaviorPopupFlag)
    {
      ImGui::OpenPopup("Error Deleting Behavior");
    }
    if (ImGui::BeginPopupModal("Error Deleting Behavior", nullptr))
    {
      ImGui::Text("Can't Delete Behavior Type!");
      if (ImGui::Button("OK"))
      {
        ImGui::CloseCurrentPopup();
        dontDeleteBehaviorPopupFlag = false;
      }

      ImGui::EndPopup();
    }
    else
    {
      object.RemoveComponent(static_cast<Component::ComponentTypes>(i));
    }

    if (removeComponentErrorPopupFlag)
    {
      ImGui::OpenPopup("Error: Can't Remove Component");
      removeComponentErrorPopupFlag = false;
    }

    /* If we tried to add a transform dependent component throw error */
    if (ImGui::BeginPopupModal("Error: Can't Remove Component", nullptr))
    {
      if (object.Get(Component::ComponentTypes::ctBehavior))
      {
        ImGui::Text("Error! Cannot remove behavior!");
        if (ImGui::Button("OK"))
        {
          ImGui::CloseCurrentPopup();
        }
      }
      else
      {
        ImGui::Text("Error! Cannot remove transform without first removing physics and sprite!");
        if (ImGui::Button("OK"))
        {
          ImGui::CloseCurrentPopup();
        }
      }

      ImGui::EndPopup();
    }

    /* Adding some space to make it look better */
    ImGui::Separator();
    ImGui::Text("");


    /* Cloning objects */
    if (ImGui::Button("Clone Object"))
    {
      /* Make sure name doesn't already exist */
      static bool noRename = false;
      std::string newName = (object.GetName() + " Clone");
      for (unsigned int i = 0; i < serializer.GetObjectList().size(); i++)
      {
        if (newName == serializer.GetObjectList()[i])
        {
          noRename = true;
          break;
        }
        else
        {
          noRename = false;
        }
      }

      /* Make sure new name doesn't exceed word limit */
      if (newName.size() > MAX_NAME_LENGTH)
      {
        noRename = true;
      }

      if (!noRename)
      {
        /* Clone object and add it to object list */
        CloneObject(object, newName);
        serializer.GetObjectList().push_back(newName);
        isObjectOpen.push_back(false);
      }
    }

    /* Deleting objects */
    if (ImGui::Button("Delete Object"))
    {
      /* Confirm deletion */
      ImGui::OpenPopup("Delete?");
    }
    bool toDelete = false;

    /* Deleting confirmation popup */
    if (ImGui::BeginPopupModal("Delete?", nullptr))
    {

      ImGui::Text("Are you sure you want to delete this object?");
      if (ImGui::Button("DELETE IT"))
      {
        toDelete = true;
        ImGui::CloseCurrentPopup();
      }

      ImGui::SameLine();

      if (ImGui::Button("Cancel"))
      {
        toDelete = false;
        ImGui::CloseCurrentPopup();
      }

      ImGui::EndPopup();
    }

    if (toDelete)
    {
      /* If it's a child, disown it */
      if (object.isChild())
      {
        object.GetParent().Disown(object);
      }

      /* Find the children in the list for deletion */
      int numChildren = object.GetChildCount();
      for (int i = 0; i < numChildren; i++)
      {
        for (std::vector<std::string>::iterator it = serializer.GetObjectList().begin(); it != serializer.GetObjectList().end(); ++it)
        {
          if (object.GetChild(i).GetName() == *it)
          {
            serializer.GetObjectList().erase(it);
            break;
          }
        }
      }

      /* Find the object in the list for deletion */
      for (std::vector<std::string>::iterator it = serializer.GetObjectList().begin(); it != serializer.GetObjectList().end(); ++it)
      {
        if (oldName == *it)
        {
          serializer.GetObjectList().erase(it);
          break;
        }
      }

      /* Delete all children from serializer and destroy */
      for (int i = 0; i < numChildren; i++)
      {
        serializer.Delete(object.GetChild(i).GetName());
        object.GetChild(i).SetToDestroy();
      }

      /* Delete object from serializer and destroy */
      serializer.Delete(object.GetName());
      serializer.Serialize();
      object.SetToDestroy();

      /* Restart level so it doesn't screw up input */
      if (Engine::GameStateManager::GetCurrentName() == "Color")
      {
        onStart = true;
        Engine::GameStateManager::Go(Engine::GAMESTATES::HardR);
      }
      else
      {
        onStart = true;
        Engine::GameStateManager::Go(Engine::GAMESTATES::Color);
      }
    }

    if (ImGui::Button("Serialize Object"))
    {
      /* Write object to serializer */
      serializer.WriteObject(object);
      serializer.Serialize();
    }

  ImGui::End();
}
