//------------------------------------------------------------------------------
//
// File Name:	Emitter.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------
#pragma once

#include "Component.h"
#include "Transform.h"

class ParticleBase;
class Particle;
class GameObject;

using ParticleIndex = unsigned int;
using ParticleBasePtr = std::unique_ptr<ParticleBase>;

using EmitterTypes = enum EmitterTypes {
  emRadius,
  emLine
};

class Emitter : public Component {
public:
  using ParticleBaseContainer = std::vector<ParticleBasePtr>;

  Emitter(GameObject& parent);
  
  Emitter(GameObject& parent, const Emitter &em);
  Emitter& operator=(const Emitter &) = delete;
  
  Emitter(GameObject& parent, Emitter &&em);
  Emitter &operator=(Emitter &&other) = delete;

  void Update(float dt) override;

  ParticleIndex EmitParticlesInRadius(std::string mesh_name, 
                                      std::string texture_name, 
                                      unsigned int max_particles,
                                      float radius, bool loop,
                                      float time = -1.f);

  ParticleIndex EmitParticlesInLine(std::string mesh_name,
                                    std::string texture_name,
                                    unsigned int max_particles,
                                    glm::vec3 direction,
                                    bool loop,
                                    float time = -1.f);
  const ParticleBase& GetIndexedParticleBase(ParticleIndex index) const;
private:
  ParticleBaseContainer particles;
  TransformPtr prnt_transform;
};