//------------------------------------------------------------------------------
//
// File Name:	GameObject.h
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
//
// Copyright © 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#pragma once // make sure Component and Game Object don't recurr

#include <vector> // The list of components in the GameObject will be stored as a vector
#include <memory> // Shared Pointer class
#include "Component.h" // The components attached to a game object

typedef class std::shared_ptr<Component> ComponentPtr; // Shared pointer for Components within the game object

// The Game Object Class
class GameObject
{
  public:

    // Default constructor. Gives the game object the name "DUMMY"
    GameObject(); 

    // Non-default constructor. Gives the game object the passed in name
    GameObject(std::string name);

    // Copy Constructor
    GameObject(GameObject& toClone);

    // Destructor
    ~GameObject();

    static GameObject& GetDummy();

    // update the game object and its components
    void Update(const float dt); 

    // Check if the Game Object is named the passed in name. (For finding in the Manager)
    bool IsNamed(std::string name) const;

    // Set the Game Object to be destroyed. FOR DEBUG ONLY, THIS WILL BE DONE THROUGH BEHAVIOURS LATER
    void SetToDestroy();

    // Check if the game object is set to the destroyed or not.
    bool IsToDestroy() const; 

    // Set the new name of a game object
    void Rename(std::string newName);

    /////////////////////// Component functions /////////////////////// 

    // Add a component to the game object
    void Add(ComponentPtr component);

    // Get a component from the game object
    ComponentPtr Get(Component::ComponentTypes type) const;

    template<typename type>
    std::shared_ptr<type> RawGetComponent(Component::ComponentTypes cType) const
    {
      return std::dynamic_pointer_cast<type>(Get(cType));
    }

    #define GetComponent(type) RawGetComponent<type>(Component::ct##type)

    /////////////////////// For Serialization ///////////////////////

    // Get the number of components in the object
    int GetComponentCount() const;

    // Get the name of the object
    const std::string& GetName() const;

    void RemoveComponent(Component::ComponentTypes);

    int GetChildCount() const;

    /////////////////////// Game Object Child Functions ///////////////////////

    bool isChild() const; // returns if the object is a child of another object or not

    void AddParent(GameObject* parent); // adds a parent to the game object if it is marked as a child

    void Disown(GameObject& child); // removes a child from the game object and sets all necessary flags

    GameObject& GetChild(unsigned id) const; // Get the child of the passed in game object

    GameObject& GetParent() const; // Get the parent of the passed in game object

    /////////////////////// Operator Overloads ///////////////////////

    GameObject& operator=(GameObject& other);

  private:

    void AddChild(GameObject& child); // adds a child to the game object

    std::string name_; // The object's name

    bool toDestroy_; // Flag for this object's destruction

    bool isChild_; // Flag for whether or not the object is a child

    std::vector<ComponentPtr> components_; // the vector of components in the object

    std::vector<GameObject*> children_; // the vector of child objects in the object

    GameObject* parent_; // The parent of the game object if it has one.
};

