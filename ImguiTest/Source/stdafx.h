#pragma once

#define GLEW_INCLUDES
  #include <GL\glew.h>

#define FREETYPE_INCLUDES
  #include <ft2build.h>
  #include FT_FREETYPE_H

#define SFML_INCLUDES
  #include <SFML\OpenGL.hpp>
  #include <SFML\Graphics.hpp>
  #include <SFML\Window.hpp>
  #include <SFML\Audio.hpp>


#define GLM_INCLUDES
  #include <glm\glm.hpp>
  #include <glm/gtc/matrix_transform.hpp>

#define JSON_INCLUDES
  #include <json.hpp>

#define IMGUI_INCLUDES
  #include <imgui-SFML.h>

#define GENERAL_INCLUDES
#include <cstdlib>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <fstream>
#include <iostream>
#include <memory>
#include <regex>
#include <experimental/filesystem>
#include <filesystem>
#include <random>
#include "DebugLog.h"
namespace filesystem = std::experimental::filesystem::v1;