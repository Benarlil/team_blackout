//------------------------------------------------------------------------------
//
// File Name:	Mesh.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Mesh.h"
#include "Graphics.h"
#include "Shader.h"

//-- glBuffer class ---------------------------------------------------------------------//

glBuffer::glBuffer(glBufferType buffer_type)
{
  type = buffer_type;
}

glBuffer& glBuffer::operator=(const glBuffer& b)
{
  type = b.type;
  ID = b.ID;
  size = b.size;

  return *this;
}

glBuffer::~glBuffer()
{

}

void glBuffer::Bind()
{
  if (!ID) {
    switch (type) {
    case bfVertexArray:
      glGenVertexArrays(1, &ID);
      break;
    case bfArray:
    case bfIndexArray:
      glGenBuffers(1, &ID);
      break;
    default:
      return;
    }
  }
  switch (type) {
  case bfVertexArray:
    glBindVertexArray(ID);
    break;
  case bfArray:
    glBindBuffer(GL_ARRAY_BUFFER, ID);
    break;
  case bfIndexArray:
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
    break;
  default:
    return;
  }
}

void glBuffer::Unbind(glBufferType type)
{
  switch (type) {
  case bfVertexArray:
    glBindVertexArray(0);
    break;
  case bfArray:
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    break;
  case bfIndexArray:
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    break;
  default:
    return;
  }
}

void glBuffer::SetData(std::vector<float>& data, int first_element)
{
  GLenum buffer;
  switch (type) {
  case bfArray:
    buffer = GL_ARRAY_BUFFER;
    break;
  case bfIndexArray:
    buffer = GL_ELEMENT_ARRAY_BUFFER;
    break;
  default:
    return;
  }
  if (size < data.size()) {
    /*if (size) {
      glDeleteBuffers(1, &ID);
      glGenBuffers(1, &ID);
      glBindBuffer(buffer, ID);
    }*/
    glBufferData(buffer, data.size() * sizeof(float), data.data() + first_element, GL_STATIC_DRAW);
    size = data.size();
  }
  else {
    glBufferSubData(buffer, 0, data.size() * sizeof(float), data.data() + first_element);
  }
}

//-- Mesh class ------------------------------------------------------------------------//
Mesh::Mesh() : shader(Graphics::GetShader("std_shader")), name("dummy_mesh")
{
}

Mesh::Mesh(std::string mesh_name, Shader& mesh_shader, int rows, int cols) :
  name(mesh_name),
  shader(mesh_shader),
  gl_vao(glBuffer(glBufferType::bfVertexArray)),
  gl_vbo(glBuffer(glBufferType::bfArray)),
  gl_ebo(glBuffer(glBufferType::bfIndexArray)),
  gl_transforms_buffer(glBuffer(glBufferType::bfArray)),
  gl_colors_buffer(glBuffer(glBufferType::bfArray)),
  gl_uv_offset_buffer(glBuffer(glBufferType::bfArray)),
  r(rows),
  c(cols)
{
  gl_vao.Bind();

  vertices = std::vector<float>({ -0.5f, 0.5f, 1.0f,     0.0f, 0.0f, 
                                  -0.5f, -0.5f, 1.0f,    0.0f, 1.0f / r,
                                   0.5f, -0.5f, 1.0f,    1.0f / c, 1.0f / r,
                                   0.5f, 0.5f, 1.0f,     1.0f / c, 0.0f
                                });
  gl_vbo.Bind();
  gl_vbo.SetData(vertices);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  gl_colors_buffer.Bind();
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
  glVertexAttribDivisor(1, 1);

  gl_uv_offset_buffer.Bind();
  glEnableVertexAttribArray(7);
  glVertexAttribPointer(7, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
  glVertexAttribDivisor(7, 1);

  gl_transforms_buffer.Bind();
  int vec4Size = 4 * sizeof(float);
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
  glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(vec4Size));
  glEnableVertexAttribArray(5);
  glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
  glEnableVertexAttribArray(6);
  glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));
  glVertexAttribDivisor(3, 1);
  glVertexAttribDivisor(4, 1);
  glVertexAttribDivisor(5, 1);
  glVertexAttribDivisor(6, 1);

  glBuffer::Unbind(bfArray);

  indices = std::vector<unsigned>({ 0, 1, 2,
                                    0, 2, 3
                                  });

  GLuint ebo;
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned), indices.data(), GL_STATIC_DRAW);
  gl_ebo.SetID(ebo);

  glBuffer::Unbind(bfVertexArray);
  glBuffer::Unbind(bfIndexArray);
}

Mesh::~Mesh()
{
  transforms.clear();
  colors.clear();
  uvoffsets.clear();
  vertices.clear();
  indices.clear();
}


void Mesh::SetTransform(std::string texture_name, GLuint id, glm::mat4 transform)
{
  transforms[texture_name][id] = transform;
}

void Mesh::SetColor(std::string texture_name, GLuint id, glm::vec4 color)
{
  colors[texture_name][id] = color;
}

void Mesh::SetUVOffeset(std::string texture_name, GLuint id, glm::vec2 offset)
{
  uvoffsets[texture_name][id] = offset;
}

const std::string &Mesh::GetName() const
{ 
  return name; 
}

glBuffer Mesh::GetVArrayID() const
{ 
  return gl_vao; 
}

glBuffer Mesh::GetVglBufferID() const
{ 
  return gl_vbo; 
}

glBuffer Mesh::GetEglBufferID() const
{
  return gl_ebo; 
}

int Mesh::cols() const
{
  return c;
}

int Mesh::rows() const
{
  return r;
}

void Mesh::PrepareForDraw()
{
  shader.Use();
  if (shader.GetName() == "std_shader")
  {
    GLint uniform_var_loc = glGetUniformLocation(shader.GetProgramID(), "camera");
    if (uniform_var_loc >= 0) {
      glUniformMatrix4fv(uniform_var_loc, 1, GL_FALSE, &Graphics::camera_matrix[0][0]);
    }
    else {
      std::exit(EXIT_FAILURE);
    }
  }
  gl_vao.Bind();

  gl_ebo.Bind();
}

void Mesh::Draw()
{
  //send_data(instances, offset);
  if (!transforms.empty()) {
    for (auto& pair : transforms) {
      Graphics::BindTexture(pair.first);
      process_transforms(pair.second);
      process_colors(pair.first);
      process_uv_offsets(pair.first);
      glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, pair.second.size());
    }
  }
}

void Mesh::DrawCleanup()
{
  glBuffer::Unbind(glBufferType::bfVertexArray);
  glBuffer::Unbind(glBufferType::bfIndexArray);
  glBuffer::Unbind(glBufferType::bfArray);
  shader.Use(0);
}
void Mesh::unreferenced(std::string tex_name, GLuint id)
{
  transforms[tex_name].erase(id);
  colors[tex_name].erase(id);
  uvoffsets[tex_name].erase(id);
}

// helper functions //
void Mesh::process_transforms(const std::map<GLuint, glm::mat4>& matrices)
{
  
  if (!matrices.empty()) {
    std::vector<float> buffer;
    buffer.reserve(matrices.size() * sizeof(glm::mat4));
    for (auto& matrix : matrices) {
      for (int i = 0; i < 4; i++) {
        glm::vec4 row = matrix.second[i];
        buffer.push_back(row.x);
        buffer.push_back(row.y);
        buffer.push_back(row.z);
        buffer.push_back(row.w);
      }
    }
    gl_transforms_buffer.Bind();
    gl_transforms_buffer.SetData(buffer);
  }
}

void Mesh::process_colors(const std::string& tex_name)
{
  const auto& matrices = colors[tex_name];
  if (!matrices.empty()) {
    std::vector<float> buffer;
    for (auto &vec4 : matrices) {
      buffer.push_back(vec4.second.x);
      buffer.push_back(vec4.second.y);
      buffer.push_back(vec4.second.z);
      buffer.push_back(vec4.second.w);
    }
    gl_colors_buffer.Bind();
    gl_colors_buffer.SetData(buffer);
  }
}

void Mesh::process_uv_offsets(const std::string& tex_name)
{
  const auto& matrices = uvoffsets[tex_name];
  if (!matrices.empty()) {
    std::vector<float> buffer;
    for (auto &vec2 : matrices) {
      buffer.push_back(vec2.second.x);
      buffer.push_back(vec2.second.y);
    }
    gl_uv_offset_buffer.Bind();
    gl_uv_offset_buffer.SetData(buffer);
  }
}
