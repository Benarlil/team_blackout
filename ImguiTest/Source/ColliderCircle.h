//------------------------------------------------------------------------------
//
// File Name:	ColliderCircle.h
// Author(s):	William Patrick
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once
#include "Collider.h"

class ColliderCircle : public Collider
{
	// Radius of the circle collider.
	float _Radius;

	bool _Inverse;

    
	// Offset from the center of the object
	glm::vec2 _Offset;

	public:


	ColliderCircle();

	ColliderCircle(float radius, bool inverse = false);

	ColliderCircle(glm::vec2 offset, bool inverse = false);

	ColliderCircle(float radius, glm::vec2 offset, bool hasOffset);

	ColliderCircle(ColliderCircle& original);
	
	~ColliderCircle();

	// Get the circle collider's radius.
	float GetRadius();

	glm::vec2 GetOffset();

	// Set the circle collider's radius.
	// Params:
	//   radius = the circle collider's new radius.
	void SetRadius(float radius);

	void SetOffset(glm::vec2 offset);

	bool IsInverse();

	void SwapInverse(bool state);

	bool _HasOffset;
};