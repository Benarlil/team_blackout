//------------------------------------------------------------------------------
//
// File Name:	Texture.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

class Texture {
public:
  Texture(std::string tex_name = "default", std::string filename = "");
  Texture(std::string tex_name, sf::Image& image, bool flip_vertically = false);
  ~Texture();

  void Bind();

  GLuint GetTextureID() const;
  std::string GetPath() const;
  std::string GetName() const;

  GLuint GetWidth() const;
  GLuint GetHeight() const;
private:
  std::string name;
  std::string path = "";

  GLuint glTexture = 0;

  GLuint width = 0;
  GLuint height = 0;
};

typedef std::shared_ptr<Texture> TexturePtr;