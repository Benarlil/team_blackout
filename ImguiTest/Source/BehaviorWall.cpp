#include "BehaviorWall.h"



BehaviorWall::BehaviorWall() : BehaviorBase(btWall)
{
	_StateCurr = bwInvalid;
	_StateNext = bwIdle;
}


BehaviorWall::~BehaviorWall()
{
}

void BehaviorWall::Update(float dt)
{
	_StateCurr = _StateNext;
}