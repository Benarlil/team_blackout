//------------------------------------------------------------------------------
//
// File Name:	Sprite.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

#include "Component.h"

class Mesh;
class Texture;
class Transform;

class SpriteSource {
public:
  SpriteSource(int rows = 1, int cols = 1, std::string texture_name = "default", std::string texture_path = "");

  int rows() const;
  int cols() const;
  void rows(int val);
  void cols(int val);

  std::string texture_name() const;
  std::string texture_path() const;

  void swap_texture(std::string t_name, std::string t_path);
  void swap_texture(sf::Image& image, std::string t_name);

  int frame_count() const;
  void get_uv(unsigned int frameIndex, float& u, float& v) const;
private:
  GLuint tex_rows = 1;
  GLuint tex_cols = 1;

  std::string tex_name = "";
  std::string tex_path = "";

  std::shared_ptr<Texture> texture;
};

class Sprite : public Component {
public:
  Sprite();
  Sprite(std::string mesh_name, std::string texture_name = "default", std::string opt_tex_path = "");
  Sprite(const Sprite& s);
  ~Sprite();

  void Update(float dt) override;

  void SetMesh(std::string m_name);
  void SetTexture(std::string t_name, std::string t_path = "", bool transform = false, glm::mat4 int_transform = glm::mat4(1.f));
  void SetTextTexture(sf::Image& image);

  void SetUVOffset(glm::vec2 new_uv);
  void SetColor(glm::vec4 color);
  void SetColor(float r, float g, float b, float a = 1.0f);
  // alpha doesnt work yet
  void SetAlpha(float a = 1.0f);
  void SetFrame(unsigned int frame);

  unsigned int GetFrame() const;
  glm::vec4 GetColor() const;
  glm::vec2 GetUVOffset() const;
  std::string GetMeshName() const;
  std::string GetTextureName() const;
  std::string GetTexturePath() const;
  const SpriteSource& GetSource() const;

  static GLuint next_sprite_id;
private:
  bool framed = true;
  bool colored = true;
  GLuint sprite_id;
  int frameIndex = 0;
  std::string mesh_name = "";
  std::shared_ptr<Mesh> mesh;
  std::weak_ptr<Transform> transform;
  glm::vec2 uv_offset = { 0.0f, 0.0f };
  glm::vec4 current_color = { 1.0f, 1.0f, 1.0f, 1.0f };
  glm::mat4 prev_transform = glm::mat4(1.f);
  SpriteSource source;
};

typedef std::shared_ptr<Sprite> SpritePtr;