#pragma once
#include "GameStateManager.h"
namespace Levels
{
  class MainMenu : public Engine::GameState
  {
  public:
    void Load()  override;
    void Init()  override;
    void Update(float dt)  override;
    void Unload()  override;
    void Shutdown()  override;
    void OnAction(sf::Event& event, float dt) override;
  };
}
