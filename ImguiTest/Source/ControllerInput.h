//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	ControllerInput.h                                                 //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "GameObject.h"

class ControllerSystem
{
private:
  struct ControllerData; // forward declaration for the list gettor
public:
  
  // Structure for every key
  struct ButtonData
  {
    bool isPressed = false; // flag to check if button is pressed
    std::function<void(void*)>PressedActionPtr; // function to call when button is pressed
    std::function<void(void*)>ReleasedActionPtr; // function to call when button is released
    void* functionArg; // the argument(s) for the function pointer
  };
  struct StickData
  {
    bool isPressed = false; // flag to check if button is pressed
    bool posDirection = false; // flag for what side of the stick is being pushed
    bool negDirection = false; // flag for what side of the stick is being pushed
    std::function<void(void*)>PosPushedActionPtr; // function to call when stick is pushed in the positive direction
    std::function<void(void*)>NegPushedActionPtr; // function to call when stick is pushed in the negative direction
    void* functionArg; // the argument(s) for the function pointer
  };

  // Enums for how sfml counts buttons
    enum Buttons
    {
      A,
      B,
      X,
      Y,
      LB,
      RB,
      BACK,
      START,
      LSTICKPRESS,
      RSTICKPRESS,
      BUTTONCOUNT,
    };
    enum Axis
    {
      LSTICK_X,
      LSTICK_Y,
      LTRIG,
      RTRIG,
      RSTICK_X,
      RSTICK_Y,
      DPAD_X,
      DPAD_Y
    };
 
  /*
     Default constructor, intializes all controllers for each player
     and populates the maps respectively
  */
  explicit ControllerSystem() ;

  /*
     Gets an event and:
     - Updates all key flags to collect the user data
     - Calls the corresponding function for the specified key
  */
  void Update(sf::Event& event);

  // Sets the desired function pointer for pressing the specified button
  void AssignPressedAction(int controllerIndex, Buttons button, std::function<void(void*)>funcptr);

  // Sets the desired function pointer for releasing the specified button
  void AssignReleasedAction(int controllerIndex, Buttons button, std::function<void(void*)>funcptr);

  // Sets the desired function for the stick's positive function pointer
  void AssignPosPushedAction(int controllerIndex, Axis axis, std::function<void(void*)>funcptr);

  // Sets the desired function for the stick's negative function pointer
  void AssignNegPushedAction(int controllerIndex, Axis axis, std::function<void(void*)>funcptr);

  // Sets the arguments for the button's argument
  void SetArguments(int controllerIndex, Buttons button, void* args);

  // Sets the arguments for the stick's argument
  void SetArguments(int controllerIndex, Axis axis, void * args);

  // Remaps a function for pressing a button on all controllers
  void RemapPressedAction(Buttons button, std::function<void(void*)>funcptr);

  // Remaps a function for releasing a button on all controllers
  void RemapReleasedAction(Buttons button, std::function<void(void*)>funcptr);

  // Remaps a function for the positive push of a stick for all controllers
  void RemapPosPressedAction(Axis axis, std::function<void(void*)>funcptr);

  // Remaps a function for the negative push of a stick for all controllers
  void RemapNegPressedAction(Axis axis, std::function<void(void*)>funcptr);

  // Loops through all the controllers and calls the functions that are
  // active for each
  void Exec();

  // Gets the arguments from a controller in the button map
  void* GetArguments(int controllerIndex, Buttons button);

  // Gets the arguments from a controller in the stick map
  void* GetArguments(int controllerIndex, Axis axis);

  // Returns the data for a controller in the map
  ControllerData& operator[](int controllerIndex);

  // Gets the status of the flag from a controller for a button
  bool GetPressedFlag(int controllerIndex, Buttons button);
  
  // Gets the status of the falg from a controller for a stick
  bool GetPressedFlag(int controllerIndex, Axis axis);

  // Gets the status of the positive direction flag for a stick
  // on a specified controller
  bool GetPosDirectionFlag(int controllerIndex, Axis axis);

  // Gets the status of the negative direction flag for a stick
  // on a specified controller
  bool GetNegDirectionFlag(int controllerIndex, Axis axis);

private:
  // A structure that holds the data needed for each controller connected
  struct ControllerData 
  {
    // Initializes controllers for each player in the game, including
    // button data for each input on the controller, as well as the
    // availiability for each controller
    ControllerData(const GameObject& player);

    int controllerID; // The id of the controller
    const GameObject& player; // The player object assosiated with each controller
    bool availiability; // Whether the player has a controller bound to it or not

    // Input maps for the controller
    std::map<Buttons, ButtonData> buttonMap;
    std::map<Axis, StickData> stickMap;
  };
  std::vector<ControllerData> controllerDataList; // List of controllers with data

};