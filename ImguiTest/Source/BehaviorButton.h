//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorButton.h                                                  //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// The behavior for the cursor object for menus.                                //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "Behavior.h"
#include "Transform.h"
#include "FunctionHandler.h"

class BehaviorButton : public BehaviorBase
{

public:

  // Default constructor that sets the mode to mouse
  BehaviorButton();

  // Constructor that takes the Hover and Click actions
  BehaviorButton(func_object onClick);

  // Update function that will change to another button or press a button based on input
  void Update(float dt);

  // what to do if the button is colliding with the cursor
  void* onCollide(void);

  void setColliderHandler();

  // set the function that is called when the button is clicked
  void setOnClick(func_object function);

  void setDefaultTexture(std::string, std::string);

  void setSelectedTexture(std::string, std::string);


private:

  FunctionHandler onClick_;

  std::string defaultTexture_;

  std::string selectedTexture_;

  std::string selectedPath_;

  std::string defaultPath_;

};