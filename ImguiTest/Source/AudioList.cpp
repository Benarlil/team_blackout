//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	AudioList.cpp                                                     //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// Desc: The manager for the list of audio files used in the game.              //
// Keeps track of all of them, provides them to components, and                 //
// makes sure we don't go over the SFML audio limit (256 files).                //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "stdafx.h"
#include "AudioList.h"
#include <sstream>

static std::map<std::string, sf::SoundBuffer> audioList;

using MapIterator = std::map<std::string, sf::SoundBuffer>::iterator;

// Add a sound to the audio list from the file name
void AudioListAdd(std::string audioFile)
{
  // make a pair with the audiofile name and a new buffer and insert them into the map
  audioList.insert(std::make_pair(audioFile, sf::SoundBuffer())); 

  // Get the file into the buffer
  audioList[audioFile].loadFromFile(audioFile);
}

// Remove a sound from the audio list
void AudioListRemove(std::string audioFile)
{
  MapIterator toRemove = audioList.find(audioFile); // find the audio

  audioList.erase(toRemove); // remove it from the list
}

// Find and return a sound from the audio list
sf::SoundBuffer& AudioListFind(std::string audioFile)
{
  if (audioList.find(audioFile) == audioList.end())
    return sf::SoundBuffer();

  else
    return audioList[audioFile];
}

// Adds all sounds in sounds directory to audio list
// Don't be mad Paul pls
void AddAllSounds()
{
  /* Look into directory sounds for all sounds */
  std::experimental::filesystem::path soundDirectory = ".\\Sounds";
  for (auto it : std::experimental::filesystem::directory_iterator(soundDirectory))
  {
    std::ostringstream oss;
    oss << it;
    std::string path = oss.str();

    /* Add to audio list */
    AudioListAdd(path);
  }
}
