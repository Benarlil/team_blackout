//------------------------------------------------------------------------------
//
// File Name:	Behavior.cpp
// Author(s):	William Patrick
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include "Behavior.h"

BehaviorBase::BehaviorBase() : Component()
{
	Component::Type(ctBehavior);
	_Type = btEmpty;
}

BehaviorBase::BehaviorBase(BehaviorTypes type) : Component()
{
	Component::Type(ctBehavior);
	_Type = type;
}


BehaviorBase::~BehaviorBase()
{

}

BehaviorTypes BehaviorBase::GetType()
{
	return _Type;
}

void BehaviorBase::SetType(BehaviorTypes type)
{
	_Type = type;

}

void BehaviorBase::Init()
{

}

void BehaviorBase::Update(float dt)
{

}

void BehaviorBase::Exit()
{

}



