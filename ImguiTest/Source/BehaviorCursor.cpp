//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorCursor.cpp                                                //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// The behavior for the cursor object for menus.                                //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "stdafx.h"
#include "BehaviorCursor.h"
#include "Graphics.h"
#include "ControllerInput.h"

extern sf::Event gEvent;

// Default constructor that sets the mode to mouse
BehaviorCursor::BehaviorCursor() : BehaviorBase(btCursor)
{
  currentMode_ = NONE;

  currentIndex_ = 0;

  Graphics::GetMainWindow().setMouseCursorVisible(false);

  Graphics::GetMainWindow().setMouseCursorGrabbed(true);
}

BehaviorCursor::~BehaviorCursor()
{
  Graphics::GetMainWindow().setMouseCursorVisible(true);

  Graphics::GetMainWindow().setMouseCursorGrabbed(false);
}

// Constructor that takes the mode
BehaviorCursor::BehaviorCursor(ModeOfOperation setMode) : BehaviorBase(btCursor)
{
  currentMode_ = setMode;

  if (currentMode_ == MOUSE)
  {
    Graphics::GetMainWindow().setMouseCursorVisible(false);

    Graphics::GetMainWindow().setMouseCursorGrabbed(true);
  }

  currentIndex_ = 0;

  cursorTransform_ = Parent().GetComponent(Transform);
}

void BehaviorCursor::mouseMove()
{

  sf::View view = Graphics::GetMainWindow().getView();

  sf::Vector2i position = sf::Mouse::getPosition(Graphics::GetMainWindow()); // get the position of the mouse relative to the game window
  sf::Vector2f winSize = sf::Vector2f(Graphics::GetMainWindow().getSize()); // get the size of the window 
  winSize.x /= 2;
  winSize.y /= 2; // divide the window size for NDC conversion
  sf::Vector2f ndc;
  ndc.x = (position.x / winSize.x - 1);
  ndc.y = (position.y / winSize.y - 1); // do NDC conversion

  //std::cout << ndc.x << " " << ndc.y << std::endl;  output mouse position for debugging

  glm::vec3 positionConvert = { 0.0f, 0.0f, 0.0f };
  positionConvert.x = ndc.x;
  positionConvert.y = -ndc.y; // set the position with the converted coordinates
  cursorTransform_->SetTranslation(positionConvert); // set the translation of the cursor object

}

void BehaviorCursor::controllerMove()
{
  //if controller left or right
  if (gEvent.joystickMove.axis == sf::Joystick::Axis::PovX)
  {

    if (gEvent.joystickMove.position > 50)
    {
      glm::vec3 positionConvert = { 0.0f, 0.0f, 0.0f };
      positionConvert.x = -0.45f;
      positionConvert.y = -0.25f;
      cursorTransform_->SetTranslation(positionConvert); // set the translation of the cursor object
    }

    if (gEvent.joystickMove.position < -50)
    {
      glm::vec3 positionConvert = { 0.0f, 0.0f, 0.0f };
      positionConvert.x = -0.45f;
      positionConvert.y = 0.2;
      cursorTransform_->SetTranslation(positionConvert); // set the translation of the cursor object
    }
  }

  // if controller up or down
  if (gEvent.joystickMove.axis == sf::Joystick::Axis::PovY)
  {
    if (gEvent.joystickMove.position < -50)
    {
      glm::vec3 positionConvert = { 1.0f, 1.0f, 0.0f };
      positionConvert.x = -0.45f;
      positionConvert.y = -0.25f;
      cursorTransform_->SetTranslation(positionConvert); // set the translation of the cursor object
    }

    if (gEvent.joystickMove.position > 50)
    {
      glm::vec3 positionConvert = { 1.0f, 1.0f, 0.0f };
      positionConvert.x = -0.45f;
      positionConvert.y = 0.2;
      cursorTransform_->SetTranslation(positionConvert); // set the translation of the cursor object
    }
  }
}

// Update function that will change to another button or press a button based on input
void BehaviorCursor::Update(float dt)
{

  cursorTransform_ = Parent().GetComponent(Transform);

  switch (currentMode_)
  {

  case NONE:

    if (gEvent.type == sf::Event::JoystickMoved)
    {
      ChangeMode(CONTROLLER);
    }

    if (gEvent.type == sf::Event::MouseMoved)
    {
      ChangeMode(MOUSE);
    }

    break;

  case MOUSE:
    
    mouseMove();

    if (gEvent.type == sf::Event::JoystickMoved)
    {
      ChangeMode(CONTROLLER);
    }

    break;

  case CONTROLLER:

    controllerMove();

    break;
  }
}

// Add a menu button transform directly
void BehaviorCursor::AddMenuButton(GameObject& button)
{
  menuButtons_.push_back(button.GetComponent(Transform)); // add the transform to the list

  ++maxIndex_; // set the max index
}

// Go to the next menu object based on input
void BehaviorCursor::GoToNextButton()
{
  if (currentIndex_ < maxIndex_)
  {
    ++currentIndex_;

    cursorTransform_->SetTranslation(menuButtons_[currentIndex_]->GetTranslation());
  }
}

// Go to the previous menu object based on input
void BehaviorCursor::GoToPreviousButton()
{
  if (currentIndex_ > 0)
  {
    --currentIndex_;

    cursorTransform_->SetTranslation(menuButtons_[currentIndex_]->GetTranslation());
  }
}

void BehaviorCursor::ChangeMode(ModeOfOperation setMode)
{
  currentMode_ = setMode;
}
