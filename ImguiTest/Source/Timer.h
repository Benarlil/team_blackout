//------------------------------------------------------------------------------
//
// File Name:	Timer.h
// Author(s):	William Patrick(william.patrick)
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once
#include "stdafx.h"


class Timer
{
	float _Miliseconds;
	int _Seconds;
	int _Minutes;

	std::string _Time;

public:

	Timer();

	Timer(Timer& original);
	
	~Timer();

	void Update(float dt);

	std::string GetTime();

	int GetMin();

	int GetSec();
	
	float GetMil();
};