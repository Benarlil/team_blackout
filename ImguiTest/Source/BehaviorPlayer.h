#pragma once
#include "Behavior.h"
#include "BehaviorWeapon.h"
#include "Collider.h"

#define PLAYER_DEFAULT_HEALTH 100;
#define DEFAULT_MAX_VELOCITY 8.0f;
#define DEFAULT_MAX_ACCELERATION 50.0f;

class BehaviorPlayer : public BehaviorBase
{


  float _MaxAccel;
  float _MaxVel;
  int _Health;
  int _StateCurr;
  int _StateNext;
  std::string _AnchorName;
  std::string deadIcon;
  std::string resultsTex;
public:

  enum States
  {
    bpInvalid,
    bpIdle,
    bpMoving,
    bpDead,
  };

  BehaviorPlayer();
  BehaviorPlayer(int health, std::string anchorName);
  BehaviorPlayer(BehaviorPlayer& original);
  ~BehaviorPlayer();
  int GetHealth();
  void SetResultsTexture(std::string textureName);
  void SetDeathIcon(std::string deadPath);
  void SetHealth(int health);
  void TakeDamage(int damage);
  void SetAnchor(std::string anchorName);
  std::string GetAnchor();
  void PlayerCollide(GameObject& object);
  void SetStateNext(States state);
  int GetStateCurr();
  void Init();
  void Update(float dt);
  void SetHandler(GameObject& object);
  void Exit();
};