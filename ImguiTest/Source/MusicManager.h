//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	MusicManager.h                                                    //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//


#include <string>

// Set the song that will be played/manipulated
void MusicManagerSetSong(std::string songName);

// Set the loop points of the currently set song
void MusicManagerSetLoopPoints(float startLoop, float loopLength);

// Set if the current song should loop or not
void MusicManagerSetLoop(bool toLoop);

// Play or resume the song
void MusicManagerPlaySong();

// Pause the song.
void MusicManagerPauseSong();

// Stops the song. DO THIS BEFORE SETTING IT TO A NEW SONG
void MusicManagerStopSong();

// Increase the volume of the currently set music
void MusicManagerIncreaseVolume(float volume);

// Directly set the volume of the current set music
void MusicManagerSetVolume(float volume);

// Play/Pause the current song
void MusicManagerToggleSong();