//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorProp.cpp                                                  //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "BehaviorProp.h"


BehaviorProp::BehaviorProp() : BehaviorBase(btProp)
{
  _StateCurr = bpActive;
  _StateNext = bpActive;
  _DoDamage = false;
}

BehaviorProp::BehaviorProp(bool canDoDamage) : BehaviorBase(btPlayer)
{
  _StateCurr = bpActive;
  _StateNext = bpActive;
  _DoDamage = canDoDamage;
}

void BehaviorProp::SetDamageFlag(bool canDamage)
{
  _DoDamage = canDamage;
}

bool BehaviorProp::GetDamageFlag(void)
{
  return _DoDamage;
}


BehaviorProp::~BehaviorProp()
{

}

void BehaviorProp::Update(float dt)
{
}

static void* CollisionHandler(BehaviorProp& bp, GameObject& object)
{
  bp.PropCollide(object);
  return nullptr;
}

void BehaviorProp::PropCollide(GameObject& object) {}

void BehaviorProp::SetHandler(GameObject& object)
{
  if (Parent().GetComponent(Collider))
  {
    std::function<void*(void)> f = [this, &object]() {
      CollisionHandler(*this, object);
      return nullptr; };
    Parent().GetComponent(Collider)->SetCollisionHandler(f);
  }
}