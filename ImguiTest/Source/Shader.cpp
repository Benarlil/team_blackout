//------------------------------------------------------------------------------
//
// File Name:	Shader.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Shader.h"

Shader::Shader(std::string shader_name, const char* vertexShaderPath, const char* fragmentShaderPath) : name(shader_name)
{
  unsigned int vertexShader;
  int compiled;
  auto vertexShaderSource = ReadShaderFile(vertexShaderPath);
  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  const auto c_str = vertexShaderSource.c_str();
  glShaderSource(vertexShader, 1, &c_str, NULL);
  glCompileShader(vertexShader);
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compiled);
  if (!compiled) {
    std::cout << "Vertex Shader Compilation Failed" << std::endl;
  }

  unsigned int fragmentShader;
  auto fragmentShaderSource = ReadShaderFile(fragmentShaderPath);
  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  const auto c_str2 = fragmentShaderSource.c_str();
  glShaderSource(fragmentShader, 1, &c_str2, NULL);
  glCompileShader(fragmentShader);
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compiled);
  if (!compiled) {
    std::cout << "Fragment Shader Compilation Failed" << std::endl;
  }
  
  program = glCreateProgram();
  glAttachShader(program, vertexShader);
  glAttachShader(program, fragmentShader);
  glLinkProgram(program);
  glGetProgramiv(program, GL_LINK_STATUS, &compiled);
  if (!compiled) {
    std::cout << "Shader Program Linking failed" << std::endl;
  }

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
}

Shader& Shader::operator=(const Shader& s) 
{
  name = s.name;
  program = s.program;

  return *this;
}

Shader::~Shader()
{

}

void Shader::Use(int state)
{
  if (!state)
    glUseProgram(state);
  else 
    glUseProgram(program);
}

std::string Shader::ReadShaderFile(const char* filename)
{
  std::fstream in(filename, std::ios_base::in);

  std::string str((std::istreambuf_iterator<char>(in)),
    std::istreambuf_iterator<char>());

  return str;
}