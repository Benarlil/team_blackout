//------------------------------------------------------------------------------
//
// File Name:	Particle.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Particle.h"
#include "Sprite.h"
#include "Mesh.h"
#include "Texture.h"
#include "Transform.h"
#include "Graphics.h"

static glm::vec3 get_rand_pos_in_radius(const glm::vec3& init_pos, float radius);
static float get_rand_float_in_range(float upper, float lower);

//-----------------------------ParticleBase class-------------------------------

ParticleBase::ParticleBase(const Transform& emitter_transform) :
  emtr_transform(emitter_transform),
  mesh(*Graphics::CreateNewMesh("dummy_mesh")),
  texture(*Graphics::CreateNewTexture("default"))
{
}

ParticleBase::ParticleBase(EmitterTypes type, 
                           std::string prt_mesh, 
                           std::string prt_texture, 
                           const Transform& emitter_transform,
                           unsigned int max_particles, 
                           float default_time, 
                           bool animated) :
  mesh(*Graphics::CreateNewMesh(prt_mesh)),
  texture(*Graphics::CreateNewTexture(prt_texture)),
  emtr_transform(emitter_transform),
  animated(animated),
  max_particles(max_particles),
  type(type),
  default_lifetime(default_time)
{
}

ParticleBase::~ParticleBase()
{
}

void ParticleBase::SetDefaultLifetime(float prt_lifetime)
{
  default_lifetime = prt_lifetime;
}

EmitterTypes ParticleBase::Type()
{
  return type;
}

//-----------------------ParticleBase derived classes---------------------------

ParticleBaseRadius::ParticleBaseRadius(std::string prt_mesh, 
                                       std::string prt_texture, 
                                       const Transform& emitter_transform,
                                       unsigned int max_particles,
                                       float emission_radius, 
                                       float default_time, 
                                       bool animated) :
  ParticleBase(emRadius, prt_mesh, prt_texture, emitter_transform, max_particles, default_time, animated),
  radius(emission_radius),
  set_lifetime(default_lifetime)
{
}

ParticleBaseRadius::ParticleBaseRadius(const ParticleBaseRadius& base) :
  ParticleBase(emRadius, 
               base.mesh.GetName(), 
               base.texture.GetName(), 
               base.emtr_transform, 
               base.max_particles, 
               base.default_lifetime, 
               base.animated),
  radius(base.radius),
  set_lifetime(base.set_lifetime)
{
  for (auto& prt : base.particles) {
    particles.emplace_back(Particle(*this, prt));
  }
  emitting = base.emitting;
  looping = base.looping;
}

void ParticleBaseRadius::Update(float dt)
{
  static bool stopped = false;
  if (emitting) {
    for (Particle& prt : particles) {
      if (!prt.Update(dt) && looping) {
        float particle_lifetime = get_rand_float_in_range(0.f, set_lifetime);
        prt.Lifetime(particle_lifetime);
        prt.Offset(get_rand_pos_in_radius(emtr_transform.GetTranslation(), radius));
        prt.Color(glm::vec3(1.f, 0.f, 0.f));
      }
    }
    stopped = false;
  }
  else if (!stopped) {
    for (Particle& prt : particles)
      prt.Lifetime(0.f);
    stopped = true;
  }
}

void ParticleBaseRadius::Emit(bool loop, float time)
{
  if (time >= 0)
    set_lifetime = time;
  else
    set_lifetime = default_lifetime;
  if (particles.empty()) {
    glm::vec3 em_pos = emtr_transform.GetTranslation();
    particles.reserve(max_particles);
    for (unsigned int i = 0; i < max_particles; i++)
      particles.emplace_back(Particle(*this, get_rand_pos_in_radius(em_pos, radius), set_lifetime));
  }
  else {
    for (Particle& prt : particles) {
      float particle_lifetime = get_rand_float_in_range(1 / set_lifetime, set_lifetime);
      prt.Lifetime(particle_lifetime);
      prt.Color(glm::vec3(1.f, 0.f, 0.f));
    }
  }
  emitting = true;
  looping = loop;
}

void ParticleBaseRadius::Stop()
{
  emitting = false;
}

ParticleBase& ParticleBaseRadius::UpCast()
{
  return static_cast<ParticleBase&>(*this);
}

//-------------------------------Particle class---------------------------------

Particle::Particle(ParticleBase& prt_base, glm::vec3 displacement, float lifetime) :
  base(prt_base),
  offset(displacement),
  id(Sprite::next_sprite_id++),
  lifetime(lifetime),
  visible((lifetime == -1) ? false : true)
{
  if (!base.animated)
    base.mesh.SetUVOffeset(base.texture.GetName(), render_id, glm::vec2{ 0.f, 0.f });
  else
    throw std::runtime_error("Animated particles are not supported !yet!"); // will add later ~Tim

  base.mesh.SetColor(base.texture.GetName(), render_id, color);
}

Particle::Particle(ParticleBase& new_base, const Particle& prt) :
  base(new_base),
  offset(prt.offset),
  id(Sprite::next_sprite_id++),
  lifetime(prt.lifetime),
  visible(prt.visible)
{
  base.mesh.SetColor(base.texture.GetName(), render_id, color);
}

Particle::~Particle()
{
  base.mesh.unreferenced(base.texture.GetName(), render_id);
}

void Particle::Offset(glm::vec3 new_offset)
{
  offset = new_offset;
  moved |= 1;
}

void Particle::Lifetime(float time)
{
  lifetime = time;
  if (lifetime > 0 || lifetime == -1.f)
    visible |= 1;
  else {
    visible &= 0;
    if (color.w) {
      color.w = 0.f;
      base.mesh.SetColor(base.texture.GetName(), render_id, color);
    }
  }
}

void Particle::Color(glm::vec3 c)
{
  color = glm::vec4(c, 1.f);
  base.mesh.SetColor(base.texture.GetName(), render_id, color);
}

float Particle::Lifetime() const
{
  return lifetime;
}

glm::vec3 Particle::Offset() const
{
  return offset;
}

float Particle::Update(float dt)
{
  if (visible && !color.w) {
    color.w = 1.f;
    base.mesh.SetColor(base.texture.GetName(), render_id, color);
  }
  if (lifetime > 0.f && visible) {
    lifetime -= dt;
    if (lifetime < 0) {
      visible = false;
      lifetime = 0.f;
      color.w = 0.f;
      base.mesh.SetColor(base.texture.GetName(), render_id, color);
    }
  }
  if (moved && visible) {
    const glm::vec3 &scale = base.emtr_transform.GetScale();
    glm::mat4 mat = glm::scale(glm::mat4(1.f), glm::vec3(scale.x, scale.y, 0.0f));
    mat = glm::rotate(mat, glm::radians(base.emtr_transform.GetRotation()), glm::vec3{ 0.0f, 0.0f, 1.0f });
    mat = glm::translate(mat, base.emtr_transform.GetTranslation() + offset);
    base.mesh.SetTransform(base.texture.GetName(), render_id, mat);
    moved = false;
  }
  return lifetime;
}

static glm::vec3 get_rand_pos_in_radius(const glm::vec3& init_pos, float radius)
{
  glm::vec3 prt_pos(0.f, 0.f, 0.f);
  for (int j = 0; j < 2; j++) {
    float lower = init_pos[j] - radius;
    float upper = init_pos[j] + radius;
    prt_pos[j] = get_rand_float_in_range(upper, lower);
  }
  return prt_pos;
}

static float get_rand_float_in_range(float upper, float lower)
{
  return lower + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (upper - lower)));
}
