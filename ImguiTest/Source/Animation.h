//------------------------------------------------------------------------------
//
// File Name:	Animation.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

#include "Component.h"
#include <vector>

class Sprite;

struct AnimationFrame {
  AnimationFrame(int index, float duration) :
    frameIndex(index),
    frameDuration(duration) {};
  int		frameIndex;
  float	frameDuration;
};

class AnimationSequence {
public:
  AnimationSequence(unsigned int frameCount, std::vector<AnimationFrame>& frameList, bool isLooping);
  AnimationSequence(unsigned int frameCount, float frameDuration, bool isLooping);
  AnimationSequence(const AnimationSequence& as);
  AnimationFrame& operator[](unsigned int sequenceIndex);
  unsigned int frame_count();
  bool looping();
  std::function <void(void)> OnComplete = NULL;
private:
  // The number of frames in the animation sequence.
  unsigned int frameCount;
  // A pointer to an array of AnimationFrame records.
  std::vector<AnimationFrame> frameList;
  // True if the animation loops, false otherwise.
  bool isLooping;
};

class Animation : public Component {
public:
  Animation();

  void Play(int frameCount, float frameDuration, bool isLooping);
  void PlaySequence(AnimationSequence& seq);
  void Update(float dt);
  
  unsigned int GetFrameIndex() const;
  unsigned int GetFrameCount() const;
  float GetFrameDelay() const;
  float GetFrameDuration() const;
  bool IsRunning() const;
  bool IsLooping() const;
  bool IsDone() const;
  AnimationSequence& GetAnimationSequence();

  void SetFrameIndex(unsigned int frameIndex);
  void SetFrameCount(unsigned int frameCount);
  void SetFrameDelay(float frameDelay);
  void SetFrameDuration(float frameDuration);
  void SetIsRunning(bool state);
  void SetIsLooping(bool state);
  void SetIsDone(bool state);
  void SetAnimationSequence(AnimationSequence& seq);
private:
  void advance_frame();
  // The current frame being displayed.
  unsigned int frameIndex_ = 0;
  // The maximum number of frames in the sequence.
  unsigned int frameCount_ = 0;
  // The time remaining for the current frame.
  float frameDelay_ = 0;
  // The amount of time to display each frame.
  // (Used only when playing simple animations (0 .. numFrame).)
  float frameDuration_ = 0;
  // True if the animation is running; false if the animation has stopped.
  bool isRunning_ = false;
  // True if the animation loops back to the beginning.
  bool isLooping_ = false;
  // True if the end of the animation has been reached, false otherwise.
  // (Hint: This should be true for only one game loop.)
  bool isDone_ = false;

  std::unique_ptr<AnimationSequence> sequence;
  std::weak_ptr<Sprite> sprite;
};

typedef class std::shared_ptr<Animation> AnimationPtr;