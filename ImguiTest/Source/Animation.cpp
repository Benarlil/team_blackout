//------------------------------------------------------------------------------
//
// File Name:	Animation.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Animation.h"
#include "Sprite.h"
#include "GameObject.h"

// AnimationSequence class

static void CreateDefaultList(std::vector<AnimationFrame>& list, unsigned int frameCount, float frameDuration);

AnimationSequence::AnimationSequence(unsigned int frameCount, std::vector<AnimationFrame>& frameList, bool isLooping) :
  frameCount(frameCount),
  frameList(frameList),
  isLooping(isLooping)
{
}

AnimationSequence::AnimationSequence(unsigned int frameCount, float frameDuration, bool isLooping) :
  frameCount(frameCount),
  isLooping(isLooping)
{
  CreateDefaultList(frameList, frameCount, frameDuration);
}

AnimationSequence::AnimationSequence(const AnimationSequence& as) :
  frameCount(as.frameCount),
  frameList(as.frameList),
  isLooping(as.isLooping)
{
}

AnimationFrame& AnimationSequence::operator[](unsigned int sequenceIndex)
{
  return frameList[sequenceIndex];
}

unsigned int AnimationSequence::frame_count()
{
  return frameCount;
}

bool AnimationSequence::looping()
{
  return isLooping;
}

// Animation class

Animation::Animation() : Component(ComponentTypes::ctAnimation)
{
}

void Animation::Play(int frameCount, float frameDuration, bool isLooping)
{
  if (sprite.expired()) {
    sprite = std::dynamic_pointer_cast<Sprite>(Parent().Get(ctSprite));
  }

  isRunning_ = true;
  frameIndex_ = 0;
  sequence = std::make_unique<AnimationSequence>(frameCount, frameDuration, isLooping);
  frameCount_ = frameCount;
  frameDuration_ = frameDuration;
  frameDelay_ = frameDuration;
  isLooping_ = isLooping;
  sprite.lock()->SetFrame(frameIndex_);
}

void Animation::PlaySequence(AnimationSequence& seq)
{
  if (sprite.expired()) {
    sprite = std::dynamic_pointer_cast<Sprite>(Parent().Get(ctSprite));
  }

  isRunning_ = true;
  isDone_ = false;
  frameIndex_ = 0;
  sequence = std::make_unique<AnimationSequence>(seq);
  frameCount_ = seq.frame_count();
  isLooping_ = seq.looping();
  frameDelay_ = seq[0].frameDuration;
  frameDuration_ = seq[0].frameDuration;
  sequence->OnComplete = seq.OnComplete;
  sprite.lock()->SetFrame(seq[frameIndex_].frameIndex);
}

void Animation::advance_frame()
{
  if (sprite.expired()) {
    sprite = std::dynamic_pointer_cast<Sprite>(Parent().Get(ctSprite));
  }

  AnimationSequence& seq = *sequence;

  frameIndex_++;
  if (frameIndex_ >= frameCount_)
  {
    if (isLooping_)
      frameIndex_ = 0;
    else
    {
      frameIndex_ = frameCount_ - 1;
      isRunning_ = false;
    }
    isDone_ = true;
    if (seq.OnComplete != NULL)
    {
      seq.OnComplete();
      return;
    }
  }
  if (isRunning_)
  {
    if (sequence)
    {
      frameDuration_ = seq[frameIndex_].frameDuration;
      sprite.lock()->SetFrame(seq[frameIndex_].frameIndex);
      frameDelay_ += seq[frameIndex_].frameDuration;
    }
    else
    {
      sprite.lock()->SetFrame(frameIndex_);
      frameDelay_ += frameDuration_;
    }
  }
  else
    frameDelay_ = 0;
}

void Animation::Update(float dt)
{
  isDone_ = false;
  if (isRunning_)
  {
    frameDelay_ -= dt;
    if (frameDelay_ <= 0)
    {
      advance_frame();
    }
  }
}


unsigned int Animation::GetFrameIndex() const 
{ 
  return frameIndex_; 
}

unsigned int Animation::GetFrameCount() const
{ 
  return frameCount_;
}

float Animation::GetFrameDelay() const
{ 
  return frameDelay_; 
}

float Animation::GetFrameDuration() const
{ 
  return frameDuration_; 
}

bool Animation::IsRunning() const
{ 
  return isRunning_; 
}

bool Animation::IsLooping() const
{ 
  return isLooping_; 
}

bool Animation::IsDone() const
{ 
  return isDone_; 
}

AnimationSequence& Animation::GetAnimationSequence()
{ 
  return *sequence; 
}


void Animation::SetFrameIndex(unsigned int frameIndex)
{ 
  frameIndex_ = frameIndex; 
}

void Animation::SetFrameCount(unsigned int frameCount)
{ 
  frameCount_ = frameCount; 
}

void Animation::SetFrameDelay(float frameDelay)
{ 
  frameDelay_ = frameDelay; 
}

void Animation::SetFrameDuration(float frameDuration)
{ 
  frameDuration_ = frameDuration; 
}

void Animation::SetIsRunning(bool state)
{ 
  isRunning_ = state; 
}

void Animation::SetIsLooping(bool state)
{ 
  isLooping_ = state; 
}

void Animation::SetIsDone(bool state)
{ 
  isDone_ = state; 
}

void Animation::SetAnimationSequence(AnimationSequence& seq)
{ 
  sequence = std::make_unique<AnimationSequence>(seq); 
}

static void CreateDefaultList(std::vector<AnimationFrame>& list, unsigned int frameCount, float frameDuration)
{
  list.clear();
  for (unsigned i = 0; i < frameCount; i++) {
    list.push_back(AnimationFrame(i, frameDuration));
  }
}