#include "BehaviorPlayer.h"
#include "Behavior.h"
#include "Component.h"
#include "Physics.h"
#include "Graphics.h"
#include "Camera.h"
#include "Sprite.h"
#include "Input.h"
#include "Movement.h"
#include "BehaviorWeapon.h"
#include "Text.h"
#include "Font.h"
#include "GameObject.h"
#include "Collider.h"
#include "ColliderCircle.h"
#include "GameObjectManager.h"
#include "GameStateManager.h"
#include "ControllerInput.h"
#include "BehaviorEnemy.h"
#include "SoundComponent.h"
#include "Animation.h"
#include "RoundData.h"
#include "Gui.h"

extern sf::Event gEvent;
static Input input = Input();
static void* CollisionHandler(BehaviorPlayer& bp, GameObject& object);


BehaviorPlayer::BehaviorPlayer() : BehaviorBase(btPlayer)
{
	_MaxAccel = DEFAULT_MAX_ACCELERATION;
	_MaxVel = DEFAULT_MAX_VELOCITY;
	_Health = PLAYER_DEFAULT_HEALTH;
  _AnchorName = "";
	_StateCurr = bpInvalid;
	_StateNext = bpIdle;

}
BehaviorPlayer::BehaviorPlayer(int health, std::string anchorName) : BehaviorBase(btPlayer)
{
	_MaxAccel = DEFAULT_MAX_ACCELERATION;
	_MaxVel = DEFAULT_MAX_VELOCITY;
	_Health = health;
  _AnchorName = anchorName;
	_StateCurr = bpInvalid;
	_StateNext = bpIdle;
}

BehaviorPlayer::BehaviorPlayer(BehaviorPlayer& original) : BehaviorBase(btPlayer)
{
	_MaxAccel = DEFAULT_MAX_ACCELERATION;
	_MaxVel = DEFAULT_MAX_VELOCITY;
	_Health = original.GetHealth();
	_AnchorName = original.GetAnchor();
	_StateCurr = bpInvalid;
	_StateNext = original.GetStateCurr();
}

BehaviorPlayer::~BehaviorPlayer()
{
	
}
int BehaviorPlayer::GetHealth()
{
	return _Health;
}
void BehaviorPlayer::SetHealth(int health)
{
	_Health = health;
}
void BehaviorPlayer::TakeDamage(int damage)
{
  if(_Health > 0)
	  _Health -= damage;
  if (_Health < 0)
    _Health = 0;
}


void BehaviorPlayer::SetHandler(GameObject& object)
{
		if (Parent().GetComponent(Collider))
		{
			std::function<void*(void)> f = [this, &object]() {
				CollisionHandler(*this, object);
				return nullptr; };
			Parent().GetComponent(Collider)->SetCollisionHandler(f);
		}
}


void BehaviorPlayer::PlayerCollide(GameObject& object)
{
  GameObject* parent = &Parent();
  GameObject* obj = &object;


  std::shared_ptr<BehaviorBase> behavior = std::dynamic_pointer_cast<BehaviorBase>(obj->Get(Component::ctBehavior));

  std::shared_ptr<RoundData> data = std::dynamic_pointer_cast<RoundData>(object.Get(Component::ctData));

  if (behavior->GetType() == btWeapon)
  {
    std::shared_ptr<BehaviorWeapon> wbehavior = std::dynamic_pointer_cast<BehaviorWeapon>(object.Get(Component::ctBehavior));

    if (wbehavior->GetStateCurr() == BehaviorWeapon::bwIdle)
    {
      //pick up weapon
    }

    if (wbehavior->GetStateCurr() == BehaviorWeapon::bwHeld)
    {
      std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

	  // the rotation of the weapon is governed by the rotation of the parent
  	  std::shared_ptr<Physics> wphysics = std::dynamic_pointer_cast<Physics>((object.GetParent()).Get(Component::ctPhysics));
      std::shared_ptr<BehaviorPlayer> pbehavior = std::dynamic_pointer_cast<BehaviorPlayer>((object.GetParent()).Get(Component::ctBehavior));
  
      std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));
      std::shared_ptr<Transform> otransform = std::dynamic_pointer_cast<Transform>(object.Get(Component::ctTransform));
  	
      std::shared_ptr<ColliderCircle> wCollider = std::dynamic_pointer_cast<ColliderCircle>(object.Get(Component::ctCollider));
      std::shared_ptr<Sound> pSound = std::dynamic_pointer_cast<Sound>(Parent().Get(Component::ctSound));
  

	  ptransform->SetTranslation(pphysics->GetOldPos());

	  float force = (abs(wphysics->GetRotationalVel()) * wbehavior->GetHandleLength());

	  if (force > 10.0f)
	  {
		  glm::vec3 away;

		  if (force < 0)
		  {
			  away = glm::normalize(((otransform->GetTranslation() - ptransform->GetTranslation())));
		  }
		  else
		  {
			  away = glm::normalize((ptransform->GetTranslation() - otransform->GetTranslation()));
		  }

      if (pbehavior->GetHealth() != 0 && force > 15)
      {
        TakeDamage((int)force);
        pSound->Play(pSound->GetNames()[0]);
      }

	  if (data)
	  {
		  data->TookDamage((int)force);
	  }


    if (_Health <= 0)
      _StateNext = bpDead;

		  pphysics->SetVelocity(force * away * 2.0f);
		  pphysics->SetAcceleration(pphysics->GetVelocity());
	  }
		}
	}

	if (behavior->GetType() == btPlayer)
	{
		std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));
		std::shared_ptr<Transform> otransform = std::dynamic_pointer_cast<Transform>(object.Get(Component::ctTransform));
		
		std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

		ptransform->SetTranslation(pphysics->GetOldPos());

		glm::vec3 away = glm::normalize((ptransform->GetTranslation() - otransform->GetTranslation()));

		pphysics->SetVelocity(glm::length(pphysics->GetVelocity()) * away);
		pphysics->AddAcceleration(pphysics->GetVelocity() * 1.5f);


	}
  if (behavior->GetType() == btProp)
  {
    std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));
    std::shared_ptr<Transform> otransform = std::dynamic_pointer_cast<Transform>(object.Get(Component::ctTransform));


    std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

    ptransform->SetTranslation(pphysics->GetOldPos());

    glm::vec3 away = glm::normalize((ptransform->GetTranslation() - otransform->GetTranslation()));
	away.z = 0.f;

	if (data)
	{
		data->CollidedProp();
	}

    pphysics->SetVelocity(glm::length(pphysics->GetVelocity())* away );
    pphysics->SetAcceleration(pphysics->GetVelocity() * 1.5f);
  }


  if (behavior->GetType() == btWall)
  {
	  std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>(Parent().Get(Component::ctTransform));
	  std::shared_ptr<Transform> otransform = std::dynamic_pointer_cast<Transform>(object.Get(Component::ctTransform));


	  std::shared_ptr<Physics> pphysics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

	  ptransform->SetTranslation(pphysics->GetOldPos());

	  glm::vec3 away = glm::normalize((otransform->GetTranslation() - ptransform->GetTranslation() ));
	  away.z = 0.f;

	  if (data)
	  {
		  data->CollidedProp();
	  }

	  pphysics->SetVelocity(glm::length(pphysics->GetVelocity())* away);
	  pphysics->SetAcceleration(pphysics->GetVelocity() * 1.5f);
  }
 

  //if (behavior->GetType() == btEnemy)
  //{
  //  std::shared_ptr<BehaviorEnemy> bEnemy = std::dynamic_pointer_cast<BehaviorEnemy>(behavior);
  //  if (bEnemy->GetHealth() != 0)
  //  {
  //    int health = PLAYER_DEFAULT_HEALTH;
  //    _Health += health / 4;
  //  }
  //}
}

void BehaviorPlayer::SetStateNext(States state)
{
	_StateNext = state;
}

int BehaviorPlayer::GetStateCurr()
{
	return _StateCurr;
}

void BehaviorPlayer::Init()
{


}

void BehaviorPlayer::SetResultsTexture(std::string textureName)
{
  resultsTex = textureName;
}


void BehaviorPlayer::SetAnchor(std::string anchorName)
{
  _AnchorName = anchorName;
}

std::string BehaviorPlayer::GetAnchor()
{
  return _AnchorName;
}

void BehaviorPlayer::Update(float dt)
{
  _StateCurr = _StateNext;
  std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

  std::shared_ptr<Transform> ptransform = std::dynamic_pointer_cast<Transform>((Parent().Get(Component::ctTransform)));

  //std::shared_ptr<Transform> wtransform = std::dynamic_pointer_cast<Transform>(Parent().GetChild(2).Get(Component::ctTransform));

  glm::vec3 pTranslation = ptransform->GetTranslation();
  glm::vec3 pVelocity = physics->GetVelocity();


  pTranslation.z = 0.0f;



  glm::vec3 acceleration = physics->GetAcceleration();



  if (glm::length(pVelocity) > _MaxVel)
  {
    pVelocity = (pVelocity / glm::length(pVelocity)) * _MaxVel;
  }
  if (glm::length(acceleration) > _MaxAccel)
  {
    acceleration = (acceleration / glm::length(acceleration)) * _MaxAccel;
  }

  physics->SetAcceleration(acceleration);
  physics->SetVelocity(pVelocity);

  if (physics->GetRotationalAccel() > 35.0f)
  {
    physics->SetRotationalAccel(35.0f);
  }

  if (physics->GetRotationalVel() > 30.f)
  {
    physics->SetRotationalVel(30.f);
  }

  if (physics->GetRotationalAccel() < -35.0f)
  {
    physics->SetRotationalAccel(-35.0f);
  }

  if (physics->GetRotationalVel() < -30.f)
  {
    physics->SetRotationalVel(-30.f);
  }

  physics->SetAcceleration(acceleration);
  physics->SetVelocity(pVelocity);
  // GAME CAMERA CODE
  Camera& camera = Graphics::camera;
  GameObject& p2 = GameObjectManagerFindObject(_AnchorName);
  if (p2.GetName() != "DUMMY")
  {
    TransformPtr p1Transform = Parent().GetComponent(Transform);
    TransformPtr p2Transform = p2.GetComponent(Transform);
    glm::vec3 p1Translation = p1Transform->GetTranslation();
    glm::vec3 p2Translation = p2Transform->GetTranslation();

    float playersXRatio = (p1Translation.x + p2Translation.x) / 2;
    float playersYRatio = (p1Translation.y + p2Translation.y) / 2;
    float distance = sqrt(((p2Translation - p1Translation).x * (p2Translation - p1Translation).x)
      + ((p2Translation - p1Translation).y * (p2Translation - p1Translation).y));
    camera.SetData(glm::vec3(playersXRatio, playersYRatio, camera.GetZoom()));

    auto& view = Graphics::GetMainWindow().getView();
    float width = view.getSize().x;
    float height = view.getSize().y;
    static float ogCameraZoom = Graphics::camera.GetZoom();
    if (distance > 12 )
      camera.SetZoom(ogCameraZoom + ((distance - 8) * 1.5));
  }

  if (_StateCurr == bpDead)
  {

    //GameObject& gO = GameObjectManagerCreateObject("P1ConnectedText");
    //glm::vec3 vecc(0.0f, 0.0f, 0.0f);

    //Transform transform = Transform(vecc.x, vecc.y, 0);
    //transform.SetScale(glm::vec3(2.0f, 2.0f, 0.0f));

    //static Font font = Font(".\\Fonts\\CrimsonText-Regular.ttf", 60);
    //Text cText = Text(font);
    //gO.Add(std::make_shared<Text>(cText));
    //cText.SetString("Press A to Continue");

    std::shared_ptr<Physics> physics = std::dynamic_pointer_cast<Physics>(Parent().Get(Component::ctPhysics));

    std::shared_ptr<Sprite> pSprite = std::dynamic_pointer_cast<Sprite>(Parent().Get(Component::ctSprite));

    GameObject& crowd = GameObjectManagerFindObject("Crowd");
    std::shared_ptr<Sprite> crowdSprite = std::dynamic_pointer_cast<Sprite>(crowd.Get(Component::ctSprite));
    crowdSprite->SetAlpha(1);
    pSprite->SetTexture(Parent().GetName() + "dead", deadIcon);

    int childCount = Parent().GetChildCount();
    for (int i = 0; i < childCount; i++)
    {
      typedef BehaviorBase Behavior;
      if (Parent().GetChild(i).GetComponent(Behavior))
      {
        if (Parent().GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btWeapon)
        {
          std::shared_ptr<Sprite> cSprite = std::dynamic_pointer_cast<Sprite>(Parent().GetChild(i).Get(Component::ctSprite));
          cSprite->SetAlpha(0);
        }
      }
    }
    static float deathTimer = 0.0f;
    deathTimer += dt;
    if (deathTimer >= 3.0f)
    {
      if (gEvent.joystickButton.joystickId < 10 && (gEvent.joystickButton.button <= ControllerSystem::BUTTONCOUNT && sf::Joystick::isButtonPressed(gEvent.joystickButton.joystickId, 0)))
      {
        HardReset((void*)_Health);
        deathTimer = 0;
      }
      GameObject& Results = GameObjectManagerFindObject("ResultsScreen");
      if (Results.GetName() != "DUMMY")
      {
        std::shared_ptr<Sprite> pSprite = std::dynamic_pointer_cast<Sprite>(Results.Get(Component::ctSprite));
        pSprite->SetTexture(/*resultsTex.substr(15, resultsTex.size() - 15)*/resultsTex, resultsTex, true, pSprite->Parent().GetComponent(Transform)->GetMatrix());
        pSprite->SetAlpha(1);
      }
     
    }
  }
}

void BehaviorPlayer::Exit()
{

}


void BehaviorPlayer::SetDeathIcon(std::string deadPath)
{
  deadIcon = deadPath;
}


static void* CollisionHandler(BehaviorPlayer& bp, GameObject& object) { bp.PlayerCollide(object);     return nullptr; }