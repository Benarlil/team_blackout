//------------------------------------------------------------------------------
//
// File Name:	Engine.h
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

//////////////////////////////////////////////////////////
#ifndef ENGINE_H
#define ENGINE_H
//////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------

namespace Engine
{
  // Initialize game engine
  void EngineInit();

  //Update game engine
  void EngineUpdate(float dt);

  // Shutdown game engine
  void EngineShutdown();
}
#endif //ENGINE_H
