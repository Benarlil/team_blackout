//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Listener.cpp                                                      //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// Desc: An audio listener that will change how game object sounds are heard    //
// based on the camera                                                          //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include"stdafx.h"
#include "Listener.h"
#include "DebugLog.h"

// construct the listener with a set of floats
Listener::Listener(float x, float y, float z, float volume)
{
  // set the position of the listener
  base_.setPosition(x, y, z);

  // set the position within the class itself for getting
  position_.x = x;
  position_.y = y;
  position_.z = z;

  // only set the volume if it is within an acceptable range
  if(volume >= 0.0f && volume <= 100.0f)
  { 
  
    base_.setGlobalVolume(volume); // set the volume of the listener

  }
  DebugMessage("Listener constructed with floats and volume: ", volume);
}

// construct the listener with a vector
Listener::Listener(glm::vec3 position, float volume)
{

  // set the position of the listener, essentially converting from glm::vector to sf::vector
  base_.setPosition(position.x, position.y, position.z);

  // only set the volume if it is within an acceptable range
  if (volume >= 0.0f && volume <= 100.0f)
  {
    base_.setGlobalVolume(volume); // set the volume of the listener
  }

  DebugMessage("Listener constructed with vector and volume: ", volume);
}

// raise the volume of the listener by a certain amount
void Listener::raiseVolume(float volume)
{
  float currVolume = base_.getGlobalVolume(); // get the current volume for comparison

  // ensure that volume changes will remain within range
  if (currVolume + volume <= 100.0f && currVolume + volume >= 0)
  {
    base_.setGlobalVolume(currVolume + volume); // raise the volume
  }

  // if it goes above (or someone is being a joker and sets it to go below)
  else
  {
    base_.setGlobalVolume(100.0f); // maximize the volume
  }
}

// lower the volume of the listener by a certain amount
void Listener::lowerVolume(float volume)
{
  float currVolume = base_.getGlobalVolume(); // get the current volume for comparison

  // ensure that volume changes will remain within range
  if (currVolume - volume <= 100.0f && currVolume - volume >= 0)
  {
    base_.setGlobalVolume(currVolume - volume); // lower the volume
  }

  // if it goes below (or someone is being a joker and sets it to go above)
  else
  {
    base_.setGlobalVolume(0.0f); // mute the volume
  }
}

// set the volume of the listener
void Listener::setVolume(float volume)
{
  // ensure that volume changes will remain within range
  if (volume <= 100.0f && volume >= 0)
  {
    base_.setGlobalVolume(volume); // set the volume
  }

  // if it goes above
  else if (volume >= 100.0f)
  {
    base_.setGlobalVolume(100.0f); // clamp the volume to max
  }

  // if it goes below
  else if (volume <= 0.0f)
  {
    base_.setGlobalVolume(0.0f); // clamp the volume to mute
  }
}

// set the position of the listener with floats
void Listener::setPosition(float x, float y, float z)
{
  // first, ensure that the max position is not set
  if (maxPosition_.x == 0 && maxPosition_.y == 0 && maxPosition_.z == 0)
  {
    // set the position of the listener
    base_.setPosition(x, y, z);

    // set the position within the class itself for getting
    position_.x = x;
    position_.y = y;
    position_.z = z;
  }

  // if the max position is being used, make sure all values are within it
  else
  {
    // if x is not within it
    if (glm::abs(x) >= maxPosition_.x)
    {
      // set negative max position
      if (x < 0)
        x = -maxPosition_.x;

      // set positive max position
      else if (x > 0)
        x = maxPosition_.x;
    }

    // if y is not within it
    if (glm::abs(y) >= maxPosition_.y)
    {
      // set negative max position
      if (y < 0)
        y = -maxPosition_.y;

      // set positive max position
      else if (y > 0)
        y = maxPosition_.y;
    }

    // if z is not within it
    if (glm::abs(z) >= maxPosition_.z)
    {
      // set negative max position
      if (z < 0)
        z = -maxPosition_.z;

      // set positive max position
      else if (z > 0)
        z = maxPosition_.z;
    }

    // set the position of the listener
    base_.setPosition(x, y, z);

    // set the position within the class itself for getting
    position_.x = x;
    position_.y = y;
    position_.z = z;
  }
}

// set the position of the listener with a vector
void Listener::setPosition(glm::vec3 position)
{
  // first, ensure that the max position is not set
  if (maxPosition_.x == 0 && maxPosition_.y == 0 && maxPosition_.z == 0)
  {
    // set the position of the listener
    base_.setPosition(position.x, position.y, position.z);

    // set the position within the class itself for getting
    position_.x = position.x;
    position_.y = position.y;
    position_.z = position.z;
  }

  // if the max position is being used, make sure all values are within it
  else
  {
    // if x is not within it
    if (glm::abs(position.x) >= maxPosition_.x)
    {
      // set negative max position
      if (position.x < 0)
        position.x = -maxPosition_.x;

      // set positive max position
      else if (position.x > 0)
        position.x = maxPosition_.x;
    }

    // if y is not within it
    if (glm::abs(position.y) >= maxPosition_.y)
    {
      // set negative max position
      if (position.y < 0)
        position.y = -maxPosition_.y;

      // set positive max position
      else if (position.y > 0)
        position.y = maxPosition_.y;
    }

    // if z is not within it
    if (glm::abs(position.z) >= maxPosition_.z)
    {
      // set negative max position
      if (position.z < 0)
        position.z = -maxPosition_.z;

      // set positive max position
      else if (position.z > 0)
        position.z = maxPosition_.z;
    }

    // set the position of the listener
    base_.setPosition(position.x, position.y, position.z);

    // set the position within the class itself for getting
    position_.x = position.x;
    position_.y = position.y;
    position_.z = position.z;
  }
}

// set the max position of the listener with a vector
void Listener::setMaxPosition(glm::vec3 position)
{
  // set all the values
  maxPosition_.x = position.x;
  maxPosition_.y = position.y;
  maxPosition_.z = position.z;
}

// set the max position of the listener with floats
void Listener::setMaxPosition(float x, float y, float z)
{
  // set all the values
  maxPosition_.x = x;
  maxPosition_.y = y;
  maxPosition_.z = z;
}

// get the current position of the listener
glm::vec3 Listener::getPosition()
{
  return position_; // return the position
}

// get the clamp values of the listener position
glm::vec3 Listener::getMaxPosition()
{
  return maxPosition_; // return the max absolute position
}