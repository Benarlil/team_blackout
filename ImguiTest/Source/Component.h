//------------------------------------------------------------------------------
//
// File Name:	Component.h
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
//
// Copyright © 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#pragma once // Ensure the Game Object and Components don't just recurr
#include <memory> // Shared Pointer class

class GameObject; // Forward declaration of the game object class so that the Component can use it

// a base component class to build off of for each component
class Component
{
public:
	// typedef for the types of components we will have
	typedef enum
	{
	  ctTransform,
	  ctPhysics,
    ctSound,
    ctCollider,
    ctSprite,
	  ctBehavior,
    ctAnimation,
    ctEmitter,
	  ctData
	}
	ComponentTypes;

  // Default Constructor, DOES NOTHING
  Component() {};

	// Non-default constructor. Allows easy setting of a component type upon construction
	Component(ComponentTypes type) { componentType_ = type; };

	// Gets the type of the current Component
	Component::ComponentTypes Type() const { return componentType_; }

	// Sets the type of the current Component (easier to set that in the constructor)
	void Type(Component::ComponentTypes type) { componentType_ = type; }

	// Sets the parent of the component
	void Parent(GameObject& parent) { componentParent_ = &parent; }

	// Gets the parent of the component
	GameObject& Parent() const { return *componentParent_; }

	// Update for each specific component. 
	// Left virtual as you can't update a component that has nothing in it.
	virtual void Update(float dt) {};

private:
	// the type of the particular component
	ComponentTypes componentType_;

	// the Game Object this component is attached to
	GameObject* componentParent_;
};