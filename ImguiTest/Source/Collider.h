//------------------------------------------------------------------------------
//
// File Name:	Collider.h
// Author(s):	William Patrick
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "Component.h"
#include "GameObject.h"
#include "FunctionHandler.h"

class ColliderCircle;

//typedef void(*CollisionEventHandler)(GameObject& gameObject1);

typedef glm::vec3 vec3;

class Collider : public Component
{
public:

	typedef enum ColliderTypes
	{
		ColliderTypeInvalid = -1,
		ColliderTypeCircle,
		ColliderTypeLine,
		ColliderTypeBox,
		ctMax

	} ColliderType;
	
	// Pointer to a function that handles collisions between two objects.
	FunctionHandler	_Handler;

	// Constructor for the collider base class
	// Params:
	//      ColliderType: what type of collider is being created
	Collider(ColliderTypes ctype);

	// Destructor for the collider base class
	~Collider();

	// Gets the type of the collider
	// Return:
	//		ColliderTypes: should be the type of collider (declared on construction)
	ColliderTypes GetType();

	// Gets the type of the collider
	// Param:
	//		ColliderTypes: should be the type of collider (declared on construction)
	void SetType(ColliderTypes type);

	// Set the collision event handler for a collider.
	// Params:
	//	 handler = Pointer to the collision event handler (may be NULL).
	void SetCollisionHandler(func_object func);

private:
	// The type of collider used by this component.
	ColliderTypes _ColliderType;
};

typedef class std::shared_ptr<Collider> ColliderPtr;


void IsColliding(Collider& collider1, Collider& collider2);