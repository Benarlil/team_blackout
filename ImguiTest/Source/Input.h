//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Input.h                                                           //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "GameObject.h"
//typedef void(*FunctionPtr)(GameObject*);

class Input
{
  public:
    // Structure for every key
    struct KeyData
    {
      bool isPressed = false; // flag to check if key is pressed
      sf::Event::KeyEvent * modifier; // mod key variable
      std::function<void(void*)>PressedActionPtr; // function to call when key is pressed
      std::function<void(void*)>ReleasedActionPtr; // function to call when key is released
      void* functionArg; // the argument(s) for the function pointer
    };

    /* 
       Default constructor, intializes all key structures and populates
       the map
    */
    Input();

    /* 
       Gets an event and:
       - Updates all key flags to collect the user data
       - Calls the corresponding function for the specified key
    */
    void Update(sf::Event& event);

    // Sets the desired function for the specified key
    void AssignPressedAction(sf::Keyboard::Key key, std::function<void(void*)> func);//void(*funcptr)(void*));

    // Sets the desired function for the specified key
    void AssignReleasedAction(sf::Keyboard::Key key, std::function<void(void*)> func);//void(*funcptr)(void*));

    // Sets the arguments for the function pointer
    void SetArguments(sf::Keyboard::Key key, void* args);

    void Exec();

    // Gets the arguments for the function pointer
    void* GetArguments(sf::Keyboard::Key key);

    std::map<sf::Keyboard::Key, KeyData> GetMap();

    bool GetFlag(sf::Keyboard::Key key);

	void ClearMap(void);

  private:
    // A map for storing all of the keys
    std::map<sf::Keyboard::Key, KeyData> keyMap;

};