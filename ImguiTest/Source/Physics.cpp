//------------------------------------------------------------------------------
//
// File Name:	Physics.cpp
// Author(s):	William Patrick (william.patrick)
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include <memory>
#include "Component.h"
#include "Physics.h"
#include "Transform.h"
#include "GameObject.h"
#include "Behavior.h"
#include "BehaviorPlayer.h"

typedef glm::vec3 vec3;

Physics::Physics() : Component()
{
	//velocity has 3 components
	Type(Component::ctPhysics);
	_Velocity = { 0.0f, 0.0f, 0.0f };
	_Oldposition = { 0.0f, 0.0f, 0.0f };
	_Acceleration = { 0.0f, 0.0f, 0.0f };
	_Mass = 0.5f;
	_RotationalVel = 0.0f;
	_RotationalAccel = 0.0f;
	_Oldrotation = 0.0f;

}

Physics::Physics(float mass) : Component()
{
	Type(Component::ctPhysics);
	if (mass <= 0)
		mass = 1.0f;

	_Oldposition = { 0.0f, 0.0f, 0.0f };
	_Velocity = { 0.0f, 0.0f, 0.0f };
	_Acceleration = { 0.0f, 0.0f, 0.0f };
	_Mass = mass;
	_RotationalVel = 0.0f;
	_RotationalAccel = 0.0f;
	_Oldrotation = 0.0f;
}

Physics::Physics(Physics& original) : Component()
{
	Type(Component::ctPhysics);

	_Oldposition = original.GetOldPos();
	_Velocity = original.GetVelocity();
	_Acceleration = original.GetAcceleration();
	_Mass = original.GetMass();
	_RotationalVel = original.GetRotationalVel();
	_RotationalAccel = original.GetRotationalAccel();
	_Oldrotation = original.GetOldRot();
}


Physics::~Physics()
{

}

vec3 Physics::GetVelocity()
{
		return _Velocity;
}

void Physics::SetVelocity(vec3 input)
{
	if (&input)
	{
		_Velocity = input;
	}
}

void Physics::SetVelocity(float x, float y)
{
	vec3 input = { x, y, 0.0f };
	_Velocity = input;
}

vec3 Physics::GetAcceleration()
{
		return _Acceleration;
}

void Physics::SetAcceleration(vec3 input)
{
	if (&input)
	{
		_Acceleration = input;

	}
}

void Physics::SetAcceleration(float x, float y)
{
		vec3 input = { x, y, 0.0f };
		_Acceleration = input;
}

void Physics::AddAcceleration(vec3 input)
{
	if (&input)
	  _Acceleration += input;
}

void Physics::AddAcceleration(float x, float y)
{
	vec3 input = { x, y, 0.0f };
	_Acceleration += input;
}

float Physics::CalculateForce()
{
	float length = (float)_Velocity.length();
	float force = length * _Mass;
	return force;
}

void Physics::SetMass(float input)
{
	if (input != 0.0f)
	{
		_Mass = input;
	}
	else
		_Mass = 1.0f;

}


float Physics::GetMass()
{
	return _Mass;
}

vec3 Physics::GetOldPos()
{

	return _Oldposition;
}

float Physics::GetOldRot()
{
	return _Oldrotation;
}

float Physics::GetRotationalAccel()
{
	return _RotationalAccel;
}

void Physics::SetRotationalAccel(float input)
{
	_RotationalAccel = input;

}

float Physics::GetRotationalVel()
{
	return _RotationalVel;
}


void Physics::SetRotationalVel(float input)
{
	_RotationalVel = input;
}

void Physics::AddRotationalVel(float input)
{
	_RotationalVel += input;

}

void Physics::AddRotationalAccel(float input)
{
	_RotationalAccel += input;

}

void Physics::Update(float dt)
{
	std::shared_ptr<Transform> transform = std::dynamic_pointer_cast<Transform>((Parent().Get(Component::ctTransform)));
	std::shared_ptr<BehaviorBase> behavior1 = std::dynamic_pointer_cast<BehaviorBase>(Parent().Get(Component::ctBehavior));

		
		vec3 position = transform->GetTranslation();

		float rotation = transform->GetRotation();

		_Oldposition = position;
		_Oldrotation = rotation;

    if (behavior1)
    {
      if (behavior1->GetType() == btPlayer)
      {
        if (((BehaviorPlayer&)*behavior1).GetStateCurr() == BehaviorPlayer::bpDead)
        {
          _Velocity *= 0.91f;

          _Acceleration *= 0.0f;

          _RotationalAccel *= 0.0f;
          _RotationalVel *= 0.96f;

          rotation += (_RotationalVel * dt);
          position += (_Velocity * dt);

          transform->SetTranslation(position);
          transform->SetRotation(rotation);

          return;

        }
      }
    }

		_Velocity += ((_Acceleration));
    
    /*_Velocity *= 0.91f;*/
    _Velocity.x -= _Velocity.x * dt * 12;
    _Velocity.y -= _Velocity.y * dt * 12;

    _Acceleration *= 0.0f;

		position += (_Velocity * dt);

		_RotationalVel += (_RotationalAccel * dt * 150);

		_RotationalAccel *= 0.0f;
		/*_RotationalVel *= 0.96f;*/
    _RotationalVel -= _RotationalVel * dt * 7;

    rotation += (_RotationalVel * dt);


		transform->SetTranslation(position);
		transform->SetRotation(rotation);
}