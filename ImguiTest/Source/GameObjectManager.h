//------------------------------------------------------------------------------
//
// File Name:	GameObjectManager.h
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "GameObject.h"
#include <memory>

typedef class std::unique_ptr<GameObject> GameObjectPtr; // A shared Game Object Pointer

enum BehaviorTypes;


// Finds a Game Object in the active list with the given a name and returns it if it exists
GameObject& GameObjectManagerFindObject(std::string objectName);

// Update every active game object
void GameObjectManagerUpdate(float dt);

void GameObjectManagerCheckCollisions();

// Clear the list
void GameObjectManagerShutdown();

// Creates AND adds a game object to the list
GameObject& GameObjectManagerCreateObject(std::string ObjectName);

// Add an existing object to the list
void GameObjectManagerAddObject(GameObject& object);

// Given a behavior type and a list to populate, returns the names of objects with the desired behavior type
void GameObjectManagerPopulateByBehaviorName(std::vector<std::string>& toPopulate, BehaviorTypes behavior);

// Given a behavior type, creates and returns a list of game object names with the desired object names
std::vector<std::string> GameObjectManagerPopulateByBehaviorName(BehaviorTypes behavior);

///////////////////////////////////// Archetype Functions //////////////////////////////////////

// Add an archetype to the archetype list
void GameObjectManagerAddArchetype(GameObject& archetype);

// Creates a Game Object archetype from an object and adds it to the archetype list
GameObject& GameObjectManagerCreateArchetype(std::string name);

// Adds a game object to the list from an archetype
GameObject& GameObjectManagerObjectFromArchetype(std::string archetypeName, std::string objectName);

GameObject& GameObjectManagerCreateArchetype(std::string name);

// Creates a copy of an already existing object and gives it a new name before returning it
GameObject& CloneObject(GameObject& object, std::string newName);
GameObject& CloneObject(const char * objectName, std::string newName);

