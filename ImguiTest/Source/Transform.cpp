//------------------------------------------------------------------------------
//
// File Name:	Transform.cpp
// Author(s):	Jack Klein (jack.klein)
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------


#include "stdafx.h"
#include "Transform.h"
#include "Physics.h"


Transform::Transform(float x, float y, float z) : Component()
{
	Component::Type(ctTransform);
	glm::vec3 tempVec = { 1.0f, 1.0f, 0.0f }; /* Temporary vector to initialize values */

	scale_ = tempVec;
  affineScalePoint_ = { 0.0f, 0.0f, 0.0f };
	rotation_ = (0);
	isDirty_ = true;
	translation_.x = x;
	translation_.y = y;
	translation_.z = z;
}


//ComponentPtr Transform::Clone(void) const
//{
//	/* Create new this, set variables and return */
//	TransformPtr newTransform = std::make_shared<Transform>(this->translation_.x, this->translation_.y);
//	if (newTransform)
//	{
//		*newTransform = *this;
//		return newTransform;
//	}
//	else
//	{
//		return NULL;
//	}
//}

Transform::~Transform()
{
}

/* Return the matrix */
const glm::mat4x4 & Transform::GetMatrix() const 
{
	return this->matrix_;
}

/* Update the transform component */
void Transform::Update(float dt)
{

	/* If the isDirty_ flag is set, calculate the matrix_ first */
	if (this->isDirty_)
	{

		/* Calculate scale_, rotate, then translate matricies */
		
    /* If there's an affine scale point, use it */
    glm::mat4x4 newScale;
    if (affineScalePoint_.x != 0.0f || affineScalePoint_.y != 0.0f || affineScalePoint_.z != 0.0f)
    {
      newScale = glm::translate(glm::identity<glm::mat4x4>(), glm::vec3(affineScalePoint_.x, affineScalePoint_.y, 0.0f));
      newScale = glm::scale(newScale, glm::vec3(scale_.x, scale_.y, 0.0f));
      newScale = glm::translate(newScale, glm::vec3(-affineScalePoint_.x, -affineScalePoint_.y, 0.0f));
    }
    /* Otherwise just scale */
    else
    {
      newScale = glm::scale(glm::identity<glm::mat4x4>(), glm::vec3(scale_.x, scale_.y, 0.0f));
    }
		glm::mat4x4 newRot = glm::rotate(glm::identity<glm::mat4x4>(), this->rotation_, glm::vec3(0.0f, 0.0f, 1.0f));
		glm::mat4x4 newTrans = glm::translate(glm::identity<glm::mat4x4>(), glm::vec3(this->translation_.x, this->translation_.y, this->translation_.z));

		/* Concatenate all matricies in T*R*S order and store result */
		matrix_ = newTrans * newRot * newScale;

		this->isDirty_ = false;
	}
}

const glm::vec3 & Transform::GetTranslation() const 
{
	return this->translation_;
}

float Transform::GetRotation() const 
{

	return this->rotation_;
}

const glm::vec3 & Transform::GetScale() const 
{
	return this->scale_;
}

void Transform::SetTranslation(const glm::vec3 & translation)
{
	/* If the pointers exist, set the translation_ */
	translation_ = translation;
	isDirty_ = true;

}

void Transform::SetRotation(float rotation)
{
	/* If the pointers exist, set the rotation_ */
	this->rotation_ = rotation;
	this->isDirty_ = true;
}

void Transform::SetScale(const glm::vec3 & scale, const glm::vec3 & affinePoint)
{
	/* If the pointers exist, set the scale_ */
	scale_ = scale;
  affineScalePoint_ = affinePoint;
	isDirty_ = true;
}
void Transform::AddTranslation(const glm::vec3 & translation)
{
	/* If the pointers exist, set the translation_ */
	translation_ = translation_ + translation;
	isDirty_ = true;

}

void Transform::AddRotation(float rotation)
{
	/* If the pointers exist, set the rotation_ */
	rotation_ = rotation_ + rotation;
	isDirty_ = true;
}

void Transform::AddScale(const glm::vec3 & scale)
{
	/* If the pointers exist, set the scale_ */
	scale_ = scale_ + scale;
	isDirty_ = true;
}