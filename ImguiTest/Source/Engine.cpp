//------------------------------------------------------------------------------
//
// File Name:	Engine.cpp
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "Engine.h"
#include "GameStateManager.h"
//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------

namespace Engine
{
	void EngineInit()
	{
    srand(time(0));
    GameStateManager::Init();
	}

	void EngineUpdate(float dt)
	{
    GameStateManager::Update(dt);
	}

	void EngineShutdown()
	{
    GameStateManager::Shutdown();
	}
}