//------------------------------------------------------------------------------
//
// File Name:	DebugObject.cpp
// Author(s):	Alex Couch
// Project:		Blackout
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include "DebugObject.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "Physics.h"
#include "Transform.h"
#include "Graphics.h"
#include "Sprite.h"
#include "ColliderCircle.h"
#include "BehaviorDebug.h"


void DebugVelocity(GameObject& parent)
{
  std::string objName = parent.GetName();
  objName.append("DebugVelocity");
  
  GameObject& dArrow = GameObjectManagerCreateObject(objName);
  std::shared_ptr<Physics> parentPhysics = std::dynamic_pointer_cast<Physics>(parent.Get(Component::ctPhysics));
  TransformPtr parentTransform = std::dynamic_pointer_cast<Transform>(parent.Get(Component::ctTransform));

  glm::vec3 transform(parentTransform->GetTranslation());
  Transform debTransform = Transform(transform.x, transform.y, transform.z);
  debTransform.SetScale(glm::vec3(0.0f, 1.0f, 0.0f));
  debTransform.SetRotation(0.0f);
  dArrow.Add(std::make_shared<Transform>(debTransform));

  Sprite aSprite = Sprite("mesh1x1", "PlayerVelocity", "Assets\\Debugs\\white_arrow_01.png");
  aSprite.SetAlpha(0.0f);
  dArrow.Add(std::make_shared<Sprite>(aSprite));
  dArrow.Add(std::make_shared<BehaviorVelocity>());
  dArrow.AddParent(&parent);
}

void DebugVelocity(std::string parent)
{
  std::string objName = parent;
  objName.append("DebugVelocity");

  GameObject& dArrow = GameObjectManagerCreateObject(objName);
  GameObject& Parent = GameObjectManagerFindObject(parent);

  std::shared_ptr<Physics> parentPhysics = std::dynamic_pointer_cast<Physics>(Parent.Get(Component::ctPhysics));
  TransformPtr parentTransform = std::dynamic_pointer_cast<Transform>(Parent.Get(Component::ctTransform));

  glm::vec3 transform(parentTransform->GetTranslation());
  Transform debTransform = Transform(transform.x, transform.y, transform.z);
  debTransform.SetScale(glm::vec3(0.0f, 1.0f, 0.0f));
  debTransform.SetRotation(0.0f);
  dArrow.Add(std::make_shared<Transform>(debTransform));

  Sprite aSprite = Sprite("mesh1x1", "PlayerVelocity", "Assets\\Debugs\\white_arrow_01.png");
  aSprite.SetAlpha(0.0f);
  dArrow.Add(std::make_shared<Sprite>(aSprite));
  dArrow.Add(std::make_shared<BehaviorVelocity>());
  dArrow.AddParent(&Parent);
}



void DebugCollider(GameObject& parent)
{
  std::string objName = parent.GetName();
  objName.append("DebugCollider");

  GameObject& dCircle = GameObjectManagerCreateObject(objName);
  std::shared_ptr<ColliderCircle> parentCollider = std::dynamic_pointer_cast<ColliderCircle>(parent.Get(Component::ctCollider));
  TransformPtr parentTransform = std::dynamic_pointer_cast<Transform>(parent.Get(Component::ctTransform));

  glm::vec3 transform(parentTransform->GetTranslation());
  Transform debTransform = Transform(transform.x , transform.y, transform.z);
  debTransform.SetScale(glm::vec3(parentCollider->GetRadius() * 2, parentCollider->GetRadius() * 2, 0.0f));
  debTransform.SetRotation(0.0f);
  dCircle.Add(std::make_shared<Transform>(debTransform));

  Sprite cSprite = Sprite("mesh1x1", "PlayerCollider", "Assets\\Debugs\\white_circle_02.png");
  cSprite.SetAlpha(0.0f);
  dCircle.Add(std::make_shared<Sprite>(cSprite));
  dCircle.Add(std::make_shared<BehaviorCollider>());
  dCircle.AddParent(&parent);
}

void DebugCollider(std::string parent)
{
  std::string objName = parent;
  objName.append("DebugCollider");

  GameObject& dCircle = GameObjectManagerCreateObject(objName);
  GameObject& Parent = GameObjectManagerFindObject(parent);

  std::shared_ptr<ColliderCircle> parentCollider = std::dynamic_pointer_cast<ColliderCircle>(Parent.Get(Component::ctCollider));
  TransformPtr parentTransform = std::dynamic_pointer_cast<Transform>(Parent.Get(Component::ctTransform));

  glm::vec3 transform(parentTransform->GetTranslation());
  Transform debTransform = Transform(transform.x, transform.y, transform.z);
  debTransform.SetScale(glm::vec3(parentCollider->GetRadius() * 2, parentCollider->GetRadius() * 2, 0.0f));
  debTransform.SetRotation(0.0f);
  dCircle.Add(std::make_shared<Transform>(debTransform));

  Sprite cSprite = Sprite("mesh1x1", "PlayerCollider", "Assets\\Debugs\\white_circle_02.png");
  cSprite.SetAlpha(0.0f);
  dCircle.Add(std::make_shared<Sprite>(cSprite));
  dCircle.Add(std::make_shared<BehaviorCollider>());
  dCircle.AddParent(&Parent);
}



void CreateDebugDraw(std::string objectName)
{
  GameObject& gO = GameObjectManagerFindObject(objectName);
  if (gO.GetName() != "DUMMY")
  {
    if(gO.Get(Component::ComponentTypes::ctPhysics))
      DebugVelocity(gO);
    DebugCollider(gO);
  }
}

void ToggleDebugObjects(std::string objectName)
{
  GameObject& gO = GameObjectManagerFindObject(objectName);
  for (int i = 0; i < gO.GetChildCount(); i++)
  {
    //if (!gO.GetChild(i).GetName().find_first_of("Weapon"))
    //{

      typedef BehaviorBase Behavior;
      if (gO.GetChild(i).GetComponent(Behavior))
      {
        if (gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btCollider || gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btVelocity)
        {
          if (gO.GetChild(i).GetComponent(Sprite)->GetColor().a == 1)
          {
            gO.GetChild(i).GetComponent(Sprite)->SetAlpha(0);
          }
          else if (gO.GetChild(i).GetComponent(Sprite)->GetColor().a == 0)
            gO.GetChild(i).GetComponent(Sprite)->SetAlpha(1);
        }
      }
   // }
  }
}

void ToggleAllVelocityDebugObjects(std::string objectName)
{
  std::vector<std::string> debugObjectList = GameObjectManagerPopulateByBehaviorName(btVelocity);

  for (std::string i : debugObjectList)
  {
    GameObject& gO = GameObjectManagerFindObject(i);

    if (gO.GetComponent(Sprite)->GetColor().a == 1)
    {
      gO.GetComponent(Sprite)->SetAlpha(0);
    }
    else if (gO.GetComponent(Sprite)->GetColor().a == 0)
      gO.GetComponent(Sprite)->SetAlpha(1);
  }
}

void ToggleVelocityDebugObject(std::string objectName)
{
  GameObject& gO = GameObjectManagerFindObject(objectName);
  for (int i = 0; i < gO.GetChildCount(); i++)
  {
    //if (!gO.GetChild(i).GetName().find_first_of("Weapon"))
    //{

    typedef BehaviorBase Behavior;
    if (gO.GetChild(i).GetComponent(Behavior))
    {
      if (gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btVelocity)
      {
        if (gO.GetChild(i).GetComponent(Sprite)->GetColor().a == 1)
        {
          gO.GetChild(i).GetComponent(Sprite)->SetAlpha(0);
        }
        else if (gO.GetChild(i).GetComponent(Sprite)->GetColor().a == 0)
          gO.GetChild(i).GetComponent(Sprite)->SetAlpha(1);
      }
    }
    // }
  }
}

void ToggleAllColliderDebugObjects(std::string objectName)
{
  std::vector<std::string> debugObjectList = GameObjectManagerPopulateByBehaviorName(btCollider);

  for (std::string i : debugObjectList)
  {
    GameObject& gO = GameObjectManagerFindObject(i);

    if (gO.GetComponent(Sprite)->GetColor().a == 1)
    {
      gO.GetComponent(Sprite)->SetAlpha(0);
    }
    else if (gO.GetComponent(Sprite)->GetColor().a == 0)
      gO.GetComponent(Sprite)->SetAlpha(1);
  }
}

void ToggleColliderDebugObject(std::string objectName)
{
  GameObject& gO = GameObjectManagerFindObject(objectName);
  for (int i = 0; i < gO.GetChildCount(); i++)
  {
    //if (!gO.GetChild(i).GetName().find_first_of("Weapon"))
    //{

    typedef BehaviorBase Behavior;
    if (gO.GetChild(i).GetComponent(Behavior))
    {
      if (gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btCollider)
      {
        if (gO.GetChild(i).GetComponent(Sprite)->GetColor().a == 1)
        {
          gO.GetChild(i).GetComponent(Sprite)->SetAlpha(0);
        }
        else if (gO.GetChild(i).GetComponent(Sprite)->GetColor().a == 0)
          gO.GetChild(i).GetComponent(Sprite)->SetAlpha(1);
      }
    }
    // }
  }
}



void ToggleAllDebugObjects()
{
  std::vector<std::string> debugObjectList = GameObjectManagerPopulateByBehaviorName(btCollider);
  GameObjectManagerPopulateByBehaviorName(debugObjectList, btVelocity);

  for (std::string i : debugObjectList)
  {
    GameObject& gO = GameObjectManagerFindObject(i);

    if (gO.GetComponent(Sprite)->GetColor().a == 1)
    {
      gO.GetComponent(Sprite)->SetAlpha(0);
    }
    else if (gO.GetComponent(Sprite)->GetColor().a == 0)
      gO.GetComponent(Sprite)->SetAlpha(1);
  }
}

void DestroyDebugObjects(std::string objectName)
{
  GameObject& gO = GameObjectManagerFindObject(objectName);


  for (int i = 0; i < gO.GetChildCount(); i++)
  {
    typedef BehaviorBase Behavior;
    if (gO.GetChild(i).GetComponent(Behavior))
    {
      if (gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btCollider || gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btVelocity)
      {
        gO.GetChild(i).SetToDestroy();
      }
    }
  }
}

void DestroyAllDebugObjects()
{
  std::vector<std::string> debugObjectList = GameObjectManagerPopulateByBehaviorName(btCollider);
  GameObjectManagerPopulateByBehaviorName(debugObjectList, btVelocity);

  for (std::string i : debugObjectList)
  {
    GameObject& gO = GameObjectManagerFindObject(i);

    gO.SetToDestroy();
  }
}

void DestroyVelocityDebugObject(std::string objectName)
{
  GameObject& gO = GameObjectManagerFindObject(objectName);


  for (int i = 0; i < gO.GetChildCount(); i++)
  {
    typedef BehaviorBase Behavior;
    if (gO.GetChild(i).GetComponent(Behavior))
    {
      if (gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btVelocity)
      {
        GameObject &child = gO.GetChild(i);
        gO.Disown(child);
        child.SetToDestroy();
      }
    }
  }
}

void DestroyAllVelocityDebugObjects()
{
  std::vector<std::string> debugObjectList = GameObjectManagerPopulateByBehaviorName(btVelocity);

  for (std::string i : debugObjectList)
  {
    GameObject& gO = GameObjectManagerFindObject(i);
    gO.GetParent().Disown(gO);
    gO.SetToDestroy();
  }
}

void DestroyColliderDebugObject(std::string objectName)
{
  GameObject& gO = GameObjectManagerFindObject(objectName);


  for (int i = 0; i < gO.GetChildCount(); i++)
  {
    typedef BehaviorBase Behavior;
    if (gO.GetChild(i).GetComponent(Behavior))
    {
      if (gO.GetChild(i).GetComponent(Behavior)->GetType() == BehaviorTypes::btCollider)
      {
        GameObject &child = gO.GetChild(i);
        gO.Disown(child);
        child.SetToDestroy();
      }
    }
  }
}

void DestroyAllColliderDebugObjects()
{
  std::vector<std::string> debugObjectList = GameObjectManagerPopulateByBehaviorName(btCollider);

  for (std::string i : debugObjectList)
  {
    GameObject& gO = GameObjectManagerFindObject(i);
    gO.GetParent().Disown(gO);
    gO.SetToDestroy();
  }
}