#pragma once
#include "stdafx.h"
#include "Collider.h"

class LineSegment
{
	std::vector<glm::vec2> _Points;
public:
	
	LineSegment();

	LineSegment(glm::vec2 point1, glm::vec2 point2);

	~LineSegment();

	glm::vec2 LineSegment::operator[](const int index);
	
};

class ColliderLine : public Collider
{
public:

	std::vector<LineSegment> _Lines;

	ColliderLine();

	~ColliderLine();

	void AddLine(glm::vec2 point1, glm::vec2 point2);

};

