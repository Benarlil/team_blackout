//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Listener.h                                                        //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
// Desc: An audio listener that will change how game object sounds are heard    //
// based on the camera                                                          //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "stdafx.h"

class Listener
{

public:

  // construct the listener with a set of floats
  Listener(float x, float y, float z, float volume = 100.0f);

  // construct the listener with a vector
  Listener(glm::vec3 position, float volume = 100.0f);

  // useful for slowing raising or lowering volume, if we want to fade in or out
  void raiseVolume(float volume); // raise the volume of the listener by a certain amount
  void lowerVolume(float volume); // lower the volume of the listener by a certain amount

  void setVolume(float volume); // set the volume of the listener

  void setPosition(float x, float y, float z); // set the position of the listener with floats

  void setPosition(glm::vec3 position); // set the position of the listener with a vector

  void setMaxPosition(float x, float y, float z); // set the position of the listener with floats

  void setMaxPosition(glm::vec3 position); // set the position of the listener with a vector

  glm::vec3 getPosition(); // get the current position of the listener

  glm::vec3 getMaxPosition(); // get the clamp values of the listener position

private:

  sf::Listener base_; // the sf listener this class is wrapped around

  glm::vec3 position_; // the position of the listener

  glm::vec3 maxPosition_; // the max ABSOLUTE values the listener should ever have. Useful for clamping once the camera zooms all the way out

};