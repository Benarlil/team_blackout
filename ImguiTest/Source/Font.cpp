//------------------------------------------------------------------------------
//
// File Name:	Font.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Graphics.h"
#include "Font.h"

std::vector<sf::Uint8> Font::pixels32bit;

#define SPACE_WIDTH 20

GlyphRenderData::GlyphRenderData(sf::Uint8 *pixels, unsigned width, unsigned height) :
  pixels(pixels),
  width(width),
  height(height)
{
}

Font::Font(std::string font_path, int char_width, int char_height)
{
  if (FT_New_Face(Graphics::ft_lib, font_path.c_str(), 0, &facehdl))
    throw std::runtime_error("Failed to load font file.");
  if (FT_Set_Char_Size(facehdl, char_width * 64, char_height * 64, 0, 0))
    throw std::runtime_error("Failed to set character size for the face object");
  ch_width = char_width;
}

Font::Font(Font&& other)
{
  if (facehdl)
    delete facehdl;
  facehdl = other.facehdl;
  other.facehdl = nullptr;
  ch_width = other.ch_width;
}

Font::~Font()
{
  pixels32bit.clear();
}

GlyphRenderData Font::CharTo32Bitmap(FT_Char symbol)
{
  FT_UInt  glyph_index = FT_Get_Char_Index(facehdl, symbol);
  if (FT_Load_Glyph(facehdl, glyph_index, FT_LOAD_DEFAULT))
    throw std::runtime_error("Failed to load glyph from the face object");
  if (FT_Render_Glyph(facehdl->glyph, FT_RENDER_MODE_NORMAL))
    throw std::runtime_error("Failed to render glyph to bitmap");
  FT_Bitmap *bitmap = &facehdl->glyph->bitmap;
  if (bitmap->buffer) {
    unsigned maxPixels = bitmap->rows * bitmap->width * 4;
    pixels32bit.clear();
    if (!pixels32bit.size()) {
      pixels32bit.reserve(maxPixels);
    }
    for (int i = 0; i < bitmap->rows; i++) {
      for (int j = 0; j < bitmap->width; j++) {
        unsigned char bitmapVal = bitmap->buffer[i * bitmap->width + j];
        pixels32bit.push_back(bitmapVal);
        pixels32bit.push_back(bitmapVal);
        pixels32bit.push_back(bitmapVal);
        pixels32bit.push_back(bitmapVal);
      }
    }
    return GlyphRenderData(pixels32bit.data(),
                           bitmap->width,
                           bitmap->rows);
  }
  else {
    if (pixels32bit.size())
      pixels32bit.clear();
    for (int i = 0; i < ch_width; i++) {
      pixels32bit.push_back(0);
      pixels32bit.push_back(0);
      pixels32bit.push_back(0);
      pixels32bit.push_back(0);
    }
    return GlyphRenderData(pixels32bit.data(),
                           SPACE_WIDTH,
                           1);
  }
}