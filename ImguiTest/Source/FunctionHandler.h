//------------------------------------------------------------------------------
//
// File Name:	FunctionHandler.h
// Author(s):	Timur Kazhimuratov
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

typedef std::function<void*(void)> func_object;

class FunctionHandler {
public:
  FunctionHandler();
  FunctionHandler(func_object function);

  void* operator()();
  void operator=(func_object function);
  void operator=(const FunctionHandler &fh);
private:
  func_object func;
};
