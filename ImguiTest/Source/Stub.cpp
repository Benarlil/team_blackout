//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Stub.cpp                                                          //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "Stub.h"
void Stub::Load()
{

}
void Stub::Init()
{

}
void Stub::Update(float dt)
{

}
void Stub::Unload()
{

}
void Stub::Shutdown()
{

}
void Stub::OnAction(sf::Event& event, float dt)
{

}