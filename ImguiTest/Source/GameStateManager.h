//------------------------------------------------------------------------------
//
// File Name:	GameStateManager.h
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

//////////////////////////////////////////////////////////
#ifndef GAMESTATEMANAGER_H
#define GAMESTATEMANAGER_H
//////////////////////////////////////////////////////////
#include "GameState.h"
#include "stdafx.h"

namespace Engine
{
  class GameStateManager
  {
  public:
    static void Init();

    static void Update(float dt);

    static void* Go(GAMESTATES gameState);

    static void Shutdown();

    static bool IsValid(GAMESTATES gameState);

    static bool IsSpecial(GAMESTATES gameState);

    static bool IsChanging();

    static bool IsRestarting();

    static bool IsInvalid();

    static bool IsRunning();

    static GameState& GetCurrent();

	static std::string GetCurrentName();

    static std::vector<GameState*> States;

  private:
    static GameState* current_;
    static GameState* next_;
  };
}
#endif //GAMESTATEMANAGER_H
