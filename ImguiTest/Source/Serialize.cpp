//------------------------------------------------------------------------------
//
// File Name:	Serialize.cpp
// Author(s):	Jack Klein (jack.klein)
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Serialize.h"

#include "Component.h"
#include "Transform.h"
#include "Physics.h"
#include "Sprite.h"
#include "Collider.h"
#include "ColliderCircle.h"
#include "SoundComponent.h"
#include "Animation.h"

#include "Behavior.h"
#include "BehaviorPlayer.h"
#include "BehaviorCursor.h"
#include "BehaviorWeapon.h"
#include "BehaviorEnemy.h"
#include "BehaviorHealthBar.h"
#include "BehaviorProp.h"
#include "BehaviorIcon.h"
#include "BehaviorWall.h"

#include "Input.h"

#include "GameObject.h"
#include "GameObjectManager.h"
#include "Graphics.h"
#include <memory>

//static const int LENGTH_OF_TEXTURE_PATH = 9;

using json = nlohmann::json;


static constexpr int ASCII_LETTER_TO_ENUM = 97; /* The value to change a lowercase letter to sf::key enum */
static constexpr int ASCII_NUMBER_TO_ENUM = 22; /* The value to change a number to sf::key enum */

/* 
 * Initializes the serializer
 * args: filename - name of the file to open
 * output: None 
 */
Serializer::Serializer(std::string filename)
{
	/* Read json file */
	std::ifstream file("json\\" + filename);
	assert(file); /* JSON file not found */

	/* Allocate memory for json and populate it */
	configFile_ = new json;
	file >> *configFile_;

	/* Set the filename */
	fileName_ = filename;
}


/*
 * Shuts down the serializer
 * args: None
 * output: None
 */
Serializer::~Serializer()
{
	/* Deallocate memory for json */
	delete configFile_;
}

/*
 * Serializes data back into the json file
 * args: None
 * output: None
 */
void Serializer::Serialize(void)
{
	/* Read json file */
	std::ofstream file("json\\" + fileName_);
	assert(file); /* JSON file not found */

	/* Write the json data into file */
	file << *configFile_;
}

/*
 * Changes component enum to a string for serializer
 * args: type - component type in enum form
 * output: component type in string literal form
 */
std::string componentEnumToString(Component::ComponentTypes type)
{
	switch (type)
	{

	case Component::ComponentTypes::ctTransform:
	{
		return "Transform";
	}
	case Component::ComponentTypes::ctSprite:
	{
		return "Sprite";
	}
	case Component::ComponentTypes::ctPhysics:
	{
		return "Physics";
	}
	case Component::ComponentTypes::ctSound:
	{
		return "Sound";
	}
	case Component::ComponentTypes::ctCollider:
	{
		return "Collider";
	}
  case Component::ComponentTypes::ctBehavior:
  {
    return "Behavior";
  }
  case Component::ComponentTypes::ctAnimation:
  {
    return "Animation";
  }
  default:
  {
    throw(std::runtime_error("You passed a component that hasn't been programmed"));
  }

	}
}

/*
 * Changes string to enum component for serializer
 * args: string - component type in string form
 * output: component type in enum form
 */
Component::ComponentTypes componentStringToEnum(std::string string)
{

	if (string == "Transform")
	{
		return Component::ComponentTypes::ctTransform;
	}
	else if (string == "Sprite")
	{
		return Component::ComponentTypes::ctSprite;
	}
	else if (string == "Physics")
	{
		return Component::ComponentTypes::ctPhysics;
	}
	else if (string == "Sound")
	{
		return Component::ComponentTypes::ctSound;
	}
	else if (string == "Collider")
	{
		return Component::ComponentTypes::ctCollider;
	}
	else if (string == "Behavior")
	{
	return Component::ComponentTypes::ctBehavior;
	}
  else if (string == "Animation")
  {
    return Component::ComponentTypes::ctAnimation;
  }
	else
	{
		throw (std::runtime_error("You passed a string that isn't a component!"));
	}

}

/*
 * Changes string to enum for collider type for serializer
 * args: string - collider type in string form
 * output: collider type in enum form
 */
Collider::ColliderType ColliderTypeStringToEnum(std::string string)
{
	if (string == "Circle")
	{
		return Collider::ColliderTypes::ColliderTypeCircle;
	}
  else
  {
    throw(std::runtime_error("You passed a string to a collider type that hasn't been programmed"));
  }
}

/*
 * Changes collider type enum to a string for serializer
 * args: type - collider type in enum form
 * output: collider type in string literal form
 */
std::string ColliderTypeEnumToString(Component::ComponentTypes type)
{
	switch (type)
	{

	case Collider::ColliderType::ColliderTypeCircle:
	{
		return "Circle";
	}
  default:
  {
    throw(std::runtime_error("You passed a collider type that hasn't been programmed"));
  }

	}
}

/*
 * Changes string to enum for behavior type for serializer
 * args: string - behavior type in string form
 * output: behavior type in enum form
 */
BehaviorTypes BehaviorTypeStringToEnum(std::string string)
{
	if (string == "Player")
	{
		return BehaviorTypes::btPlayer;
	}
	else if (string == "Weapon")
	{
		return BehaviorTypes::btWeapon;
	}
	else if (string == "Cursor")
	{
		return BehaviorTypes::btCursor;
	}
  else if (string == "Enemy")
  {
    return BehaviorTypes::btEnemy;
  }
  else if (string == "Health Bar")
  {
    return BehaviorTypes::btHealthBar;
  }
  else if (string == "Prop")
  {
    return BehaviorTypes::btProp;
  }
  else if (string == "Icon")
  {
    return BehaviorTypes::btIcon;
  }
  else if (string == "Wall")
	{
		return BehaviorTypes::btWall;
	}
	else if (string == "Empty")
	{
		return BehaviorTypes::btEmpty;
	}
	else
	{
		throw (std::runtime_error("You passed a string that isn't a behavior!"));
	}
}

/*
 * Changes behavior type enum to a string for serializer
 * args: type - behavior type in enum form
 * output: behavior type in string literal form
 */
std::string BehaviorTypeEnumToString(BehaviorTypes type)
{
	switch (type)
	{

	case BehaviorTypes::btPlayer:
	{
		return "Player";
		break;
	}
	case BehaviorTypes::btWeapon:
	{
		return "Weapon";
		break;
	}
	case BehaviorTypes::btCursor:
	{
		return "Cursor";
		break;
	}
  case BehaviorTypes::btEnemy:
  {
    return "Enemy";
    break;
  }
  case BehaviorTypes::btHealthBar:
  {
    return "Health Bar";
    break;
  }
  case BehaviorTypes::btProp:
  {
    return "Prop";
    break;
  }
  case BehaviorTypes::btIcon:
  {
    return "Icon";
    break;
  }
    case BehaviorTypes::btWall:
  {
    return "Wall";
    break;
  }
	default:
	{
		return "Empty";
		break;
	}
	//default:
	//{
	//	throw (std::runtime_error("You passed collider that doesn't exist!"));
	//}

	}
}

/*
 * Sets all of the components of a game object
 * args: object - the object to read
 * output: None
 */
void Serializer::ReadObject(GameObject & object)
{
	int count = 0;
	int i = 0;
	/* Read through all components */
	while (count < object.GetComponentCount())
	{
		/* If there exists a component of a certain type */
		if (object.Get(static_cast<Component::ComponentTypes>(i)))
		{
			/* Read component */
			json objectData = (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))];

			/* Read component data */
			switch (static_cast<Component::ComponentTypes>(i))
			{

			/* Transform component */
			case Component::ComponentTypes::ctTransform:
			{
				TransformPtr objectTransform = std::dynamic_pointer_cast<Transform>(object.Get(Component::ComponentTypes::ctTransform));

				/* Fill in rotation */
				objectTransform->SetRotation(objectData["Rotation"]);

				/* Fill in scale */
				glm::vec3 initialScale;
				initialScale.x = objectData["Scale"]["x"];
				initialScale.y = objectData["Scale"]["y"];
				objectTransform->SetScale(initialScale);

				/* Fill in translation */
				glm::vec3 initialTranslation;
				initialTranslation.x = objectData["Translation"]["x"];
				initialTranslation.y = objectData["Translation"]["y"];
				initialTranslation.z = objectData["Translation"]["z"];
				objectTransform->SetTranslation(initialTranslation);

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctSprite:
			{
				char U[3];
				char V[3];
				_itoa_s(objectData["U"], U, 10);
				_itoa_s(objectData["V"], V, 10);
				std::string meshString = "mesh" + static_cast<std::string>(U);
				std::string meshString2 = "x" + static_cast<std::string>(V);

				if (objectData["isUI"])
				{
					meshString = "UI" + meshString;
				}

				SpritePtr objectSprite = std::dynamic_pointer_cast<Sprite>(object.Get(Component::ComponentTypes::ctSprite));
				std::string texturePath = objectData["Texture"];
				texturePath.find_last_of(texturePath, 0);
        if (objectData["isUI"])
        {
          objectSprite->SetMesh(meshString + meshString2);
        }
        else
        {
          objectSprite->SetMesh(meshString + meshString2);
        }
				objectSprite->SetTexture(texturePath.substr(texturePath.find_last_of(texturePath, 0), texturePath.size() - texturePath.find_last_of(texturePath, 0)), texturePath);
				/* Set ui shader if the flag is set */
				objectSprite->SetColor(objectData["Color"]["r"], objectData["Color"]["g"], objectData["Color"]["b"], objectData["Color"]["a"]);

				/* Increment component found count */
				count++;
				break;
			}

      case Component::ComponentTypes::ctAnimation:
      {
        /* Get animation */
        AnimationPtr objectAnimation = std::dynamic_pointer_cast<Animation>(object.Get(Component::ComponentTypes::ctAnimation));

        /* Set animation variables */
        objectAnimation->SetFrameIndex(objectData["frameIndex"]);
        objectAnimation->SetFrameCount(objectData["frameCount"]);
        objectAnimation->SetFrameDelay(objectData["frameDelay"]);
        objectAnimation->SetFrameDuration(objectData["frameDuration"]);
        objectAnimation->SetIsLooping(objectData["isLooping"]);
        objectAnimation->SetIsRunning(objectData["isRunning"]);

        AnimationSequence sequence(objectData["frameCount"], objectData["frameDuration"], objectData["isLooping"]);
        objectAnimation->SetAnimationSequence(sequence);
        /* Increment component found count */
        count++;
        break;
      }

			case Component::ComponentTypes::ctPhysics:
			{
				PhysicsPtr objectPhysics = std::dynamic_pointer_cast<Physics>(object.Get(Component::ComponentTypes::ctPhysics));

				objectPhysics->SetMass(objectData["Mass"]);

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctCollider:
			{
				/* Get collider */
				ColliderPtr tempObjectCollider = std::dynamic_pointer_cast<Collider>(object.Get(Component::ComponentTypes::ctCollider));

				/* Check collider type */
				Collider::ColliderType colliderType = ColliderTypeStringToEnum(objectData["Type"]);

				/* Set collider type */
				tempObjectCollider->SetType(colliderType);

				switch (colliderType)
				{

				case (Collider::ColliderType::ColliderTypeCircle) : 
				{
					/* Collider is circle, set radius */
					std::shared_ptr<ColliderCircle> objectCollider = std::dynamic_pointer_cast<ColliderCircle>(tempObjectCollider);
          glm::vec2 offset;
					objectCollider->SetRadius(objectData["Radius"]);
          if (objectData["Offset"] != nullptr)
          {
            offset.x = objectData["Offset"]["x"];
            offset.y = objectData["Offset"]["y"];
            objectCollider->SetOffset(offset);
          }
          if (objectData["isInverse"] != nullptr)
          {
            objectCollider->SwapInverse(objectData["isInverse"]);
          }
				}

				/* End switch */
				}

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctSound:
			{
				/* Get sound */
				SoundPtr objectSound = std::dynamic_pointer_cast<Sound>(object.Get(Component::ComponentTypes::ctSound));

				objectSound->SetVolumeAll(objectData["Volume"]);
				objectSound->SetPitchAll(objectData["Pitch"]);

				/* Loop through sounds and add them to component */
				for (json::iterator it = objectData["Sounds"].begin(); it != objectData["Sounds"].end(); ++it)
				{
					objectSound->Add(it.value());
				}

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctBehavior:
			{
				/* Delete current behavior */
				BehaviorPtr objectBehavior = std::dynamic_pointer_cast<BehaviorBase>(object.Get(Component::ComponentTypes::ctBehavior));
				object.RemoveComponent(Component::ComponentTypes::ctBehavior);


				switch (BehaviorTypeStringToEnum(objectData["Type"]))
				{

				case (BehaviorTypes::btPlayer):
				{
					/* Create player behavior */
					object.Add(std::make_shared<BehaviorPlayer>());

					/* Set health */
					std::shared_ptr<BehaviorPlayer> playerBehavior = std::dynamic_pointer_cast<BehaviorPlayer>(object.Get(Component::ComponentTypes::ctBehavior));
					playerBehavior->SetHealth(objectData["Health"]);
          /* Set camera anchor if applicable */
          if (objectData["Anchor"] != nullptr)
          {
            playerBehavior->SetAnchor(objectData["Anchor"].get<std::string>());
          }
          if (objectData["deadPath"] != nullptr)
          {
            playerBehavior->SetDeathIcon(objectData["deadPath"]);
          }
          if (objectData["resultsTex"] != nullptr)
          {
            playerBehavior->SetResultsTexture(objectData["resultsTex"]);
          }

					break;
				}
				case (BehaviorTypes::btWeapon):
				{
					/* Create weapon behavior */
					object.Add(std::make_shared<BehaviorWeapon>(2.f, objectData["GoesOnLeft"], object, &(GameObjectManagerFindObject(objectData["Parent"]))));

					/* Set weapon length */
					std::shared_ptr<BehaviorWeapon> playerBehavior = std::dynamic_pointer_cast<BehaviorWeapon>(object.Get(Component::ComponentTypes::ctBehavior));
					playerBehavior->SetHandleLength(objectData["WeaponLength"]);
          

					break;
				}
				case (BehaviorTypes::btCursor):
				{
					/* Create cursor behavior */
					object.Add(std::make_shared<BehaviorCursor>());

					break;
				}
        case (BehaviorTypes::btEnemy):
        {
          /* Create health bar behavior */
          object.Add(std::make_shared<BehaviorEnemy>());

          break;
        }
        case (BehaviorTypes::btHealthBar):
        {
          if (GameObjectManagerFindObject(objectData["Parent"]).GetName() != "DUMMY")
          {
            /* Create health bar behavior */
            object.Add(std::make_shared<BehaviorHealthBar>(objectData["ScaleLeft"], objectData["Parent"]));
          }

          break;
        }
        case (BehaviorTypes::btProp):
        {
          /* Create health bar behavior */
          object.Add(std::make_shared<BehaviorProp>());

          break;
        }
        case (BehaviorTypes::btIcon):
        {
          /* Create health bar behavior */
          object.Add(std::make_shared<BehaviorIcon>());
          std::shared_ptr<BehaviorIcon> playerBehavior = std::dynamic_pointer_cast<BehaviorIcon>(object.Get(Component::ComponentTypes::ctBehavior));
          playerBehavior->SetParent(objectData["Parent"]);
          if (objectData["deadPath"] != nullptr)
          {
            playerBehavior->SetDeathIcon(objectData["deadPath"]);
          }

          break;
        }
		case (BehaviorTypes::btWall):
		{
			/* Create health bar behavior */
			object.Add(std::make_shared<BehaviorWall>());

			break;
		}


				/* End switch */
				}

				/* Increment component found count */
				count++;
				break;
			}

			/* End switch */
			}
		}

		/* Go to next component type */
		i++;
	}


}

/*
 * Write all components of a game object
 * args: object - the object to write
 * output: None
 */
void Serializer::WriteObject(GameObject & object) const
{
	int i = 0;
	int count = 0;
	/* Read through all components */
	while (count < object.GetComponentCount())
	{
		/* If there exists a component of a certain type */
		if (object.Get(static_cast<Component::ComponentTypes>(i)))
		{
			/* Write component */
			json objectData = (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))];

			/* Write component data */
			switch (static_cast<Component::ComponentTypes>(i))
			{

			/* Transform component */
			case Component::ComponentTypes::ctTransform:
			{
				/* Write all transform data back to JSON */
				TransformPtr objectTransform = std::dynamic_pointer_cast<Transform>(object.Get(Component::ComponentTypes::ctTransform));
				objectData["Translation"]["x"] = objectTransform->GetTranslation().x;
				objectData["Translation"]["y"] = objectTransform->GetTranslation().y;
				objectData["Translation"]["z"] = objectTransform->GetTranslation().z;
				objectData["Rotation"] = objectTransform->GetRotation();
				objectData["Scale"]["x"] = objectTransform->GetScale().x;
				objectData["Scale"]["y"] = objectTransform->GetScale().y;

				/* Save back into JSON object */
				(*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctSprite:
			{
				SpritePtr objectSprite = std::dynamic_pointer_cast<Sprite>(object.Get(Component::ComponentTypes::ctSprite));
        
				objectData["Color"]["r"] = objectSprite->GetColor().r;
				objectData["Color"]["g"] = objectSprite->GetColor().g;
				objectData["Color"]["b"] = objectSprite->GetColor().b;
				objectData["Color"]["a"] = objectSprite->GetColor().a;

				objectData["U"] = atoi(&(objectSprite->GetMeshName()[objectSprite->GetMeshName().find_first_of("x") - 1]));
        objectData["V"] = atoi(&(objectSprite->GetMeshName()[objectSprite->GetMeshName().find_first_of("x") + 1]));

				/* If it's a UV mesh, it'll start with a "U" */
				if (!objectSprite->GetMeshName().find("UI"))
				{
					objectData["isUI"] = true;
				}
				else
				{
					objectData["isUI"] = false;
				}

				objectData["Texture"] = objectSprite->GetTexturePath();

				/* Save back into JSON object */
				(*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

				/* Increment component found count */
				count++;
				break;
			}

      case Component::ComponentTypes::ctAnimation:
      {
        /* Get animation */
        AnimationPtr objectAnimation = std::dynamic_pointer_cast<Animation>(object.Get(Component::ComponentTypes::ctAnimation));

        /* Serialize variables */
        objectData["frameIndex"] = objectAnimation->GetFrameIndex();
        objectData["frameCount"] = objectAnimation->GetFrameCount();
        objectData["frameDelay"] = objectAnimation->GetFrameDelay();
        objectData["frameDuration"] = objectAnimation->GetFrameDuration();
        objectData["isLooping"] = objectAnimation->IsLooping();
        objectData["isRunning"] = objectAnimation->IsRunning();

        
        /* Save back into JSON object */
        (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

        /* Increment component found count */
        count++;
        break;
      }

			case Component::ComponentTypes::ctPhysics:
			{
				PhysicsPtr objectPhysics = std::dynamic_pointer_cast<Physics>(object.Get(Component::ComponentTypes::ctPhysics));

				objectData["Mass"] = objectPhysics->GetMass();

				(*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctCollider:
			{
				/* Get collider */
				ColliderPtr tempObjectCollider = std::dynamic_pointer_cast<Collider>(object.Get(Component::ComponentTypes::ctCollider));

				/* Check collider type */
				Collider::ColliderType colliderType = tempObjectCollider->GetType();
				switch (colliderType)
				{

				case (Collider::ColliderType::ColliderTypeCircle):
				{
					/* Collider is circle, set radius */
					std::shared_ptr<ColliderCircle> objectCollider = std::dynamic_pointer_cast<ColliderCircle>(tempObjectCollider);
					objectData["Type"] = "Circle";
					objectData["Radius"] = objectCollider->GetRadius();
					objectData["OffsetX"] = objectCollider->GetOffset().x;
					objectData["OffsetY"] = objectCollider->GetOffset().y;
          objectData["isInverse"] = objectCollider->IsInverse();
				}

				/* End switch */
				}



				/* Save back into JSON object */
				(*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctSound:
			{
				/* Get sound component */
				SoundPtr objectSound = std::dynamic_pointer_cast<Sound>(object.Get(Component::ComponentTypes::ctSound));

				objectData["Volume"] = objectSound->GetVolume();
				objectData["Pitch"] = objectSound->GetPitch();

				/* Loop through all sounds and save them */
				for (unsigned int i = 0; i < objectSound->GetCount(); i++)
				{
					char soundNumber[3];
					_itoa_s(i, soundNumber, 10);
					objectData["Sounds"]["Sound" + static_cast<std::string>(soundNumber)] = objectSound->GetNames()[i];
				}

				/* Save back into JSON object */
				(*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

				/* Increment component found count */
				count++;
				break;
			}

			case Component::ComponentTypes::ctBehavior:
			{
				/* Get Behavior */
				BehaviorPtr objectBehavior = std::dynamic_pointer_cast<BehaviorBase>(object.Get(Component::ComponentTypes::ctBehavior));


				switch (objectBehavior->GetType())
				{

				case (BehaviorTypes::btPlayer):
				{
					/* Cast to correct behavior */
					std::shared_ptr<BehaviorPlayer> playerBehavior = std::dynamic_pointer_cast<BehaviorPlayer>(objectBehavior);
					/* Serialize type */
					objectData["Type"] = "Player";

					/* Serialize health for player */
					objectData["Health"] = playerBehavior->GetHealth();
					/* Serialize anchor for player */
					objectData["Anchor"] = playerBehavior->GetAnchor();

					/* Save back into JSON object */
					(*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

					break;
				}
				case (BehaviorTypes::btWeapon):
				{
					/* Cast to correct behavior */
					std::shared_ptr<BehaviorWeapon> weaponBehavior = std::dynamic_pointer_cast<BehaviorWeapon>(objectBehavior);
					/* Serialize type */
					objectData["Type"] = "Weapon";

					/* Serialize parent */
					objectData["Parent"] = (object.GetParent()).GetName().c_str();
					/* Serialize weapon length for weapon */
					objectData["WeaponLength"] = weaponBehavior->GetHandleLength();

					/* Save back into JSON object */
					(*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

					break;
				}
				case (BehaviorTypes::btCursor):
				{
					/* Nothing to do for cursor */

					break;
				}
        case (BehaviorTypes::btEnemy):
        {
          /* Cast to correct behavior */
          std::shared_ptr<BehaviorEnemy> enemyBehavior = std::dynamic_pointer_cast<BehaviorEnemy>(objectBehavior);
          /* Serialize type */
          objectData["Type"] = "Enemy";

          /* Save back into JSON object */
          (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

          break;
        }
        case (BehaviorTypes::btHealthBar):
        {
          /* Cast to correct behavior */
          std::shared_ptr<BehaviorHealthBar> healthBarBehavior = std::dynamic_pointer_cast<BehaviorHealthBar>(objectBehavior);
          /* Serialize type */
          objectData["Type"] = "Health Bar";

          /* Serialize parent and scale direction */
          objectData["ScaleLeft"] = healthBarBehavior->GetIsLeft();
          objectData["Parent"] = healthBarBehavior->Parent().GetName();

          /* Save back into JSON object */
          (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

          break;
        }
        case (BehaviorTypes::btProp):
        {
          /* Cast to correct behavior */
          std::shared_ptr<BehaviorProp> propBehavior = std::dynamic_pointer_cast<BehaviorProp>(objectBehavior);
          /* Serialize type */
          objectData["Type"] = "Prop";

          /* Serialize whether the prop does damage */
          objectData["DoDamage"] = propBehavior->GetDamageFlag();

          /* Save back into JSON object */
          (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

          break;
        }
        case (BehaviorTypes::btIcon):
        {
          /* Cast to correct behavior */
          std::shared_ptr<BehaviorIcon> iconBehavior = std::dynamic_pointer_cast<BehaviorIcon>(objectBehavior);
          /* Serialize type */
          objectData["Type"] = "Icon";

          /* Serialize parent and texture path */
          objectData["deadPath"] = iconBehavior->GetDeathIcon();
          objectData["Parent"] = iconBehavior->Parent().GetName();

          /* Save back into JSON object */
          (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

          break;
        }
        case (BehaviorTypes::btWall):
        {
          /* Cast to correct behavior */
          std::shared_ptr<BehaviorWall> wallBehavior = std::dynamic_pointer_cast<BehaviorWall>(objectBehavior);
          /* Serialize type */
          objectData["Type"] = "Wall";

          /* Save back into JSON object */
          (*configFile_)[object.GetName()][componentEnumToString(static_cast<Component::ComponentTypes>(i))] = objectData;

          break;
        }


				/* End switch */
				}

				/* Increment component found count */
				count++;
				break;
			}

			/* End switch */
			}

			/* Go to next component type */
			i++;
		}
		else
		{
			i++;
			continue;
		}
	}
}

/*
 * Create all objects from a json file
 * args: None
 * output: None
 */
void Serializer::CreateFromFile(void)
{
	std::string inputFile;

	/* Loop through all objects */
	for (json::iterator it = configFile_->begin(); it != configFile_->end(); ++it)
	{

		if (it.key() == "InputFile")
		{
			const auto val = (*it);
			inputFile = val.get<std::string>();
			continue;
		}

		/* Make the object */
		GameObject& gO = GameObjectManagerCreateObject((it.key().data())); // make the game object a shared pointer

		for (json::iterator it2 = it->begin(); it2 != it->end(); ++it2)
		{
			/* Make all components */
			switch (componentStringToEnum(static_cast<std::string>(it2.key())))
			{

			case Component::ComponentTypes::ctTransform:
			{
				/* Add base transform (to be overwritten) */
				glm::vec3 vecc(0.0f, 0.0f, 0.0f);
				(gO).Add(std::make_shared<Transform>(vecc.x, vecc.y, vecc.z));
				break;
			}

			case Component::ComponentTypes::ctSprite:
			{
				/* Add base SPRITE */
				/*(gO).Add(std::make_shared<Sprite>(Sprite(Graphics::CreateNewMesh(), )));*/
				gO.Add(std::make_shared<Sprite>());
				break;
			}

      case Component::ComponentTypes::ctAnimation:
      {
        /* Add base Animation */
        gO.Add(std::make_shared<Animation>());
        break;
      }
			
			case Component::ComponentTypes::ctPhysics:
			{
				/* Add base physics */
				(gO).Add(std::make_shared<Physics>(200.0f));
				break;
			}

			case Component::ComponentTypes::ctCollider:
			{
				/* Add base collider */
				gO.Add(std::make_shared<ColliderCircle>());

				break;
			}

			case Component::ComponentTypes::ctSound:
			{
				/* Add base sound */
				gO.Add(std::make_shared<Sound>());
				break;
			}

			case Component::ComponentTypes::ctBehavior:
			{
				/* Add base behavior */
				gO.Add(std::make_shared<BehaviorBase>());
				break;
			}

			}
		}

		/* Add the objects to the object vector */
		objectNames_.push_back(it.key().data());

		ReadObject(gO);
	}

	if (!inputFile.empty())
	{
		ReadInput(inputFile);
	}
}

/*
* Returns all names of objects in json file
* args: None
* output : the names of all objects in json file
*/
std::vector<std::string> & Serializer::GetObjectList()
{
	return objectNames_;
}

/*
* Adds a new item to the serializer's list of objects
* args: newObjectName - the name of the new object to add
* output : None
*/
void Serializer::addToObjectList(std::string newObjectName)
{
	objectNames_.push_back(newObjectName);
}

/*
* Clears the serializer's list of objects
* args: None
* output : None
*/
void Serializer::ClearObjectList(void)
{
	objectNames_.clear();
}

/*
* Deletes an object in the json file (so it doesn't get cloned)
* args: objectToDelete - the object in the json file to delete
* output : None
*/
void Serializer::Delete(const std::string objectToDelete) const
{
	/* Loop through all objects */
	for (json::iterator it = configFile_->begin(); it != configFile_->end(); ++it)
	{
		/* Delete object */
		if (it.key() == objectToDelete)
		{
			configFile_->erase(it);
			return;
		}
	}
}

/*
* Sets the name of the json file
* args: newName - The new name of the json file
* output : None
*/
void Serializer::SetName(std::string newName)
{
	/* Set new name */
	fileName_ = newName;

	/* Read new json file */
	std::ifstream file("json\\" + newName);
	assert(file); /* JSON file not found */

	/* Delete old json */
	delete configFile_;

	/* Allocate memory for new json and populate it */
	configFile_ = new json;
	file >> *configFile_;

}

/*
* Gets the name of the json file
* args: None
* output : The name of the json file in the serializer
*/
std::string Serializer::GetName(void)
{
	return fileName_;
}

/*
* Converts a string with a key into the key enum
* args: keyToConvert - the string containing the key to make into a sf::Keyboard::Key
* output : the sf::Keyboard::Key enum version of the key
*/
sf::Keyboard::Key convertToKey(std::string keyToConvert)
{
	/* Check to see if string is normal letter or number */
	if (keyToConvert.size() == 1)
	{
		char key = keyToConvert[0]; /* Make it a character to check ascii values */

		if (key >= 'a' && key <= 'z')
		{
			/* Key is lowercase alphabet, convert with ascii letter offset */
			return static_cast<sf::Keyboard::Key>(key - ASCII_LETTER_TO_ENUM);
		}
		else if (key >= '0' && key <= '9')
		{
			/* Key is number, convert with ascii number offset */
			return static_cast<sf::Keyboard::Key>(key - ASCII_NUMBER_TO_ENUM);
		}
	}

	/* Key is special character, make a million if statements because you're bad at programming */
	if (keyToConvert == "LControl")
	{
		return sf::Keyboard::Key::LControl;
	}
	else if (keyToConvert == "LShift")
	{
		return sf::Keyboard::Key::LShift;
	}
	else if (keyToConvert == "LAlt")
	{
		return sf::Keyboard::Key::LAlt;
	}
	else if (keyToConvert == "RShift")
	{
		return sf::Keyboard::Key::RShift;
	}
	else if (keyToConvert == "RAlt")
	{
		return sf::Keyboard::Key::RAlt;
	}
	else if (keyToConvert == "RControl")
	{
		return sf::Keyboard::Key::RControl;
	}
	else if (keyToConvert == "Space")
	{
		return sf::Keyboard::Key::Space;
	}
	else if (keyToConvert == "Escape")
	{
		return sf::Keyboard::Key::Escape;
	}
	else if (keyToConvert == "Left")
	{
		return sf::Keyboard::Key::Left;
	}
	else if (keyToConvert == "Right")
	{
		return sf::Keyboard::Key::Right;
	}
	else if (keyToConvert == "Up")
	{
		return sf::Keyboard::Key::Up;
	}
	else if (keyToConvert == "Down")
	{
		return sf::Keyboard::Key::Down;
	}

	/* Key not found, throw error */
	throw(std::runtime_error("Input key not programmed!"));

}

/*
* Converts a string with an input function name to the input function itself
* args: fnName - the name of the function to return
* output : the function from input that matches the name
*/
static std::function<void(void*)> stringToInputFn(std::string fnName)
{
	/* Get all input functions */
	extern std::vector<std::function<void(void *)>> inputFunctionList;
	//MovePlayerUp, MovePlayerDown, MovePlayerLeft,
	//	MovePlayerRight, PlayerStopHorizontal,
	//	PlayerStopVertical

	/* Make a bunch of if statements to return the input functions because I don't know how else to do it */
	if (fnName == "MovePlayerUp")
	{
		return inputFunctionList[0];
	}
	else if (fnName == "MovePlayerDown")
	{
		return inputFunctionList[1];
	}
	else if (fnName == "MovePlayerLeft")
	{
		return inputFunctionList[2];
	}
	else if (fnName == "MovePlayerRight")
	{
		return inputFunctionList[3];
	}
	else if (fnName == "HardReset")
	{
		return inputFunctionList[4];
	}
  else if (fnName == "RotatePlayerLeft")
  {
    return inputFunctionList[5];
  }
  else if (fnName == "RotatePlayerRight")
  {
    return inputFunctionList[6];
  }

	/* Function not found, throw exception */
	else
	{
		throw(std::runtime_error("Input function not programmed!"));
	}

}

/*
* Reads the inputs from a json file
* args: inputFile - the json file with input
* output : None
*/
void Serializer::ReadInput(std::string inputFile)
{
	Serializer inputSerializer("Input\\" + inputFile);
	extern Input input;

	/* Loop through all inputs */
	for (json::iterator it = inputSerializer.configFile_->begin(); it != inputSerializer.configFile_->end(); ++it)
	{
    bool argsSetFlag = false;

		/* Make switch for arg types for press and release */
		if ((*it)["onPressArgType"] == "object")
		{
      /* Make sure the object exists */
      if (GameObjectManagerFindObject((*it)["onPressArgs"]["object"]).GetName() != "DUMMY")
      {
        /* Set press input arguments */
        input.SetArguments(convertToKey(it.key()), &(GameObjectManagerFindObject((*it)["onPressArgs"]["object"])));
        argsSetFlag = true;
      }
		}

		/* Assign the press and release functions */
		if ((*it)["onPress"] != "null")
		{
      /* Check to make sure args have been set */
      if (argsSetFlag || (*it)["onPressArgs"] == nullptr)
      {
        input.AssignPressedAction(convertToKey(it.key()), stringToInputFn((*it)["onPress"]));
      }
		}

	}
}

/*
* Reads the inputs from a json file
* args: inputFile - the json file with input
* output : None
*/
void Serializer::CreateJSON(std::string fileName)
{
	std::ofstream file(".\\json\\" + fileName, std::ios::out | std::ios::app);
	file << *configFile_;
}