//------------------------------------------------------------------------------
//
// File Name:	Mesh.h
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#pragma once

#include "Component.h"

class Transform;
class Texture;
class Shader;

enum glBufferType {
  bfUndefined = 0,

  bfVertexArray,
  bfArray,
  bfIndexArray,
};

class glBuffer {
public:
  glBuffer() {}
  glBuffer(glBufferType buffer_type);
  glBuffer& operator=(const glBuffer& b);
  ~glBuffer();

  void Bind();
  void SetData(std::vector<float>& data, int first_element = 0);

  static void Unbind(glBufferType type);

  void SetID(GLuint nID) { ID = nID; }
  void SetSize(GLuint nSize) { size = nSize; }

  GLuint GetID() { return ID; }
  GLuint GetSize() { return size; }
private:
  glBufferType type = bfUndefined;
  GLuint ID = 0;
  GLuint size = 0;
};

class Mesh {
public:
  typedef std::map<std::string, std::map<GLuint, glm::mat4>> TransformMap;

  Mesh();
  Mesh(std::string mesh_name, Shader& mesh_shader, int rows = 1, int cols = 1);
  ~Mesh();

  void PrepareForDraw();
  void Draw();
  void DrawCleanup();

  void unreferenced(std::string tex_name, GLuint id);

  void SetTransform(std::string texture_name, GLuint id, glm::mat4 transform);
  void SetColor(std::string texture_name, GLuint id, glm::vec4 color);
  void SetUVOffeset(std::string texture_name, GLuint id, glm::vec2 offset);

  const std::string &GetName() const;
  glBuffer GetVArrayID() const;
  glBuffer GetVglBufferID() const;
  glBuffer GetEglBufferID() const;
  int cols() const;
  int rows() const;
private:
  std::string name;
  Shader& shader;

  int r;
  int c;

  glBuffer gl_vao;
  glBuffer gl_vbo;
  glBuffer gl_ebo;
  glBuffer gl_transforms_buffer;
  glBuffer gl_colors_buffer;
  glBuffer gl_uv_offset_buffer;

  TransformMap transforms;
  std::map<std::string, std::map<GLuint, glm::vec4>> colors;
  std::map<std::string, std::map<GLuint, glm::vec2>> uvoffsets;

  std::vector<float> vertices;
  std::vector<unsigned> indices;

  void process_transforms(const std::map<GLuint, glm::mat4>& matrices);
  void process_colors(const std::string& tex_name);
  void process_uv_offsets(const std::string& tex_name);
};

typedef std::shared_ptr<Mesh> MeshPtr;
