#include "stdafx.h"
#include "FunctionHandler.h"

FunctionHandler::FunctionHandler()
{
}

FunctionHandler::FunctionHandler(func_object function) :
  func(function)
{
}

void* FunctionHandler::operator()()
{
  if (func) {
    return func();
  }
  return (void*)-1;
}

void FunctionHandler::operator=(func_object function)
{
  if (function)
    func = function;
}

void FunctionHandler::operator=(const FunctionHandler &fh)
{
  func = fh.func;
}
