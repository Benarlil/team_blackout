//------------------------------------------------------------------------------
//
// File Name:	Graphics.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Graphics.h"
#include "Sprite.h"
#include "Shader.h"
#include "Mesh.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "Camera.h"
#include "Texture.h"
#include "DebugLog.h"
#include <glm/gtc/matrix_transform.hpp>

sf::RenderWindow                  Graphics::mainWindow(sf::VideoMode(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT), "Flail Fighters", sf::Style::Default, sf::ContextSettings(24));
std::vector<ShaderPtr>            Graphics::shaders;
std::vector<MeshPtr>              Graphics::meshes;
std::map<std::string, TexturePtr> Graphics::textures;
Camera                            Graphics::camera;
std::shared_ptr<Mesh>             Graphics::dummy_mesh;
Shader                            Graphics::dummy_shader;
std::shared_ptr<Texture>          Graphics::dummy_texture;
glm::mat4                         Graphics::camera_matrix;
FT_Library                        Graphics::ft_lib;

void Graphics::Init()
{
  if (GLEW_OK != glewInit()) {
    throw std::runtime_error("Failed to initialize GLEW.");
  }
  if (FT_Init_FreeType(&ft_lib)) {
    throw std::runtime_error("Failed to load Freetype library.");
  }
  //glEnable(GL_BLEND); // doesnt work, requires sorting the draw order based on z values
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);
  glDepthFunc(GL_LEQUAL);
  glDepthRange(MIN_Z_DEPTH, MAX_Z_DEPTH);
  glClearColor(1.0f, 1.0f, 0.0f, 1.0f);
  // load all shaders.
  CreateNewShader("std_shader", "Shaders/vertexShader.glsl", "Shaders/fragmentShader.glsl");
  CreateNewShader("ui_shader", "Shaders/uivertexShader.glsl", "Shaders/fragmentShader.glsl");
  // preload all the meshes.
  CreateNewMesh("mesh1x1");
  CreateNewMesh("UImesh1x1");
}

void Graphics::Update()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearDepthf(1.0f);
  //mainWindow.clear(sf::Color::Cyan);
  
  // sending camera data over to the GPU
  glm::vec3 camera_data = Graphics::camera.GetData();
  glm::mat4x4 proj = glm::perspective(glm::radians(45.f),
                                      (GLfloat)mainWindow.getSize().x / mainWindow.getSize().y,
                                      1.0f,
                                      200.f);
  glm::mat4x4 view = glm::lookAt(glm::vec3(camera_data.x, camera_data.y, camera_data.z),
                                 glm::vec3(camera_data.x, camera_data.y, -1.0f),
                                 glm::vec3(0.f, 1.f, 0.f));

  camera_matrix = proj * view;

  for (auto &mesh : meshes) {
    int instances_processed = 0;
    mesh->PrepareForDraw();
    mesh->Draw();
    mesh->DrawCleanup();
  }
}


void Graphics::Shutdown()
{

}

void Graphics::BindTexture(std::string texture_name)
{
  textures[texture_name]->Bind();
}

MeshPtr Graphics::CreateNewMesh(std::string mesh_name, std::string shader_name)
{
  for (auto &mesh : meshes) {
    if (mesh->GetName() == mesh_name)
      return mesh;
  }
  int rows = 1;
  int cols = 1;
  std::regex rows_cols("([0-9]+)x([0-9]+)");
  std::smatch match;
  std::regex_search(mesh_name, match, rows_cols);
  if (!match.empty()) {
    rows = std::stoi(match[1]);
    cols = std::stoi(match[2]);
    if (shader_name.empty()) {
      std::regex ui("UI");
      std::regex_search(mesh_name, match, ui);
      if (!match.empty())
        shader_name = "ui_shader";
      else
        shader_name = "std_shader";
    }
  }
  else {
    std::string err_msg("Invalid mesh name provided for CreateNewMesh function");
    std::cout << err_msg;
    throw std::runtime_error(err_msg);
  }
  meshes.push_back(std::make_shared<Mesh>(mesh_name, GetShader(shader_name), rows, cols));
  return meshes[meshes.size() - 1];
}

Shader& Graphics::CreateNewShader(std::string shader_name,
                                  const char* vertex_shader_path,
                                  const char* fragment_shader_path)
{
  for (auto &shader : shaders) {
    if (shader->GetName() == shader_name)
      return *shader;
  }
  shaders.push_back(std::make_unique<Shader>(shader_name, 
                                                    vertex_shader_path,
                                                    fragment_shader_path));
  return *shaders[shaders.size() - 1];
}

std::shared_ptr<Texture> Graphics::CreateNewTexture(std::string tex_name, std::string tex_path)
{
  const auto it = textures.find(tex_name);

  if (it != textures.end())
    return it->second;

  const auto& pair = textures.insert({ tex_name, std::make_shared<Texture>(tex_name, tex_path.c_str()) });
  if(pair.second)
    return pair.first->second;
  return dummy_texture;
}

std::shared_ptr<Texture> Graphics::CreateNewTexture(sf::Image& image, std::string tex_name)
{
  const auto it = textures.find(tex_name);

  if (it != textures.end())
    return it->second;

  const auto& pair = textures.insert({ tex_name, std::make_shared<Texture>(tex_name, image) });
  if (pair.second)
    return pair.first->second;
  return dummy_texture;
}

std::shared_ptr<Mesh> Graphics::GetMesh(std::string name)
{
  for (auto &mesh : meshes) {
    if (mesh->GetName() == name)
      return mesh;
  }
  return dummy_mesh;
}

Shader& Graphics::GetShader(std::string name)
{
  for (auto &shader : shaders) {
    if (shader->GetName() == name)
      return *shader;
  }
  return dummy_shader;
}

std::shared_ptr<Texture> Graphics::GetTexture(std::string name)
{
  const auto it = textures.find(name);

  if (it != textures.end())
    return it->second;
  return dummy_texture;
}

void Graphics::ResizeWindow(sf::Event::SizeEvent size) {
  glViewport(0, 0, size.width, size.height);
}

sf::RenderWindow &Graphics::GetMainWindow()
{
  return mainWindow;
}