//------------------------------------------------------------------------------
//
// File Name:	Camera.h
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include "stdafx.h"

typedef glm::vec3 vec3;


//Use this class to change how the game objects are drawn based on the camera.
//When drawing the objects, apply the camera data to their transformation as
//offsets for the x and y, and then using the zoom to multiply the scale.

class Camera
{

public:

	Camera(); // Default constructor. Will set all values to 0, 0, 1

	Camera(float x, float y, float zoom = 10.f); // Non-default constructor. Usual zoom will be 1

	void Update(float dt); // update the camera

  void SetData(vec3 pos);

  void SetZoom(float z) { zoom_ = z; }

	vec3 GetData(); // get the data of the camera as a vector

  float GetZoom() { return zoom_; }

private:

	float x_; // the x position/offset of the camera

	float y_; // the y position/offset of the camera

	float zoom_; // the zoom of the camera based on player positions

};