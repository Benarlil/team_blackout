//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	Movement.h                                                        //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "GameObject.h"

void MovePlayerUp(void * gameObject);

void MovePlayerDown(void * gameObject);

void MovePlayerLeft(void * gameObject);

void MovePlayerRight(void * gameObject);

void RotatePlayerLeft(void * gameObject);

void RotatePlayerRight(void * gameObject);

void RotatePlayerStop(void * gameObject);

void PlayerStopVertical(void * gameObject);

void PlayerStopHorizontal(void * gameObject);

void HardReset(void * unused);