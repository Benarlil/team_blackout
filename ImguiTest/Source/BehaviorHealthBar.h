//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorHealthBar.h                                               //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "Behavior.h"
#include "BehaviorPlayer.h"
#include "Animation.h"
class BehaviorHealthBar : public BehaviorBase
{
private:
  int _StateCurr;
  int _StateNext;
  int _HealthCurr;
  int _HealthMax;
  bool _scaleGoesLeft;
  std::shared_ptr<BehaviorPlayer> _ParentBehavior;
public:
  enum States
  {
    bpInvalid = -1,
    bpActive,
    bpHit,
    bpDead
  };
  BehaviorHealthBar();
  bool GetIsLeft();
  BehaviorHealthBar(bool goesLeft, std::string gameObject);
  void Update(float dt);
};
