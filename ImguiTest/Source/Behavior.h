//------------------------------------------------------------------------------
//
// File Name:	Behavior.h
// Author(s):	William Patrick
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once
#include"stdafx.h"
#include "GameObject.h"
#include "Component.h"
enum BehaviorTypes
{
	btEmpty,
	btPlayer,
	btWeapon,
	btCursor,
	btCollider,
	btVelocity,
	btEnemy,
	btHealthBar,
	btProp,
	btIcon,
	btWall,
  btMax
};

class BehaviorBase : public Component 
{
	BehaviorTypes _Type;
	int _State;
	int _NextState;

public:



	BehaviorBase(); 
	BehaviorBase(BehaviorTypes type);
	~BehaviorBase();

	BehaviorTypes GetType();
	//empty in base, overwritten by specific behavior classes
	
	void SetType(BehaviorTypes type);
	
	void Init();
	//empty in base, overwritten by specific behavior classes
	void Update(float dt);
	//empty in base, overwritten by specific behavior classes
	void Exit();
};

typedef class std::shared_ptr<BehaviorBase> BehaviorPtr;