//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorDebug.cpp                                                 //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#include "BehaviorDebug.h"
#include "Behavior.h"
#include "Component.h"
#include "Physics.h"
#include "GameObject.h"
#include "Collider.h"
#include "ColliderCircle.h"

BehaviorCollider::BehaviorCollider() : BehaviorBase(btCollider)
{
  _StateCurr = bpActive;
  _StateNext = bpActive;

}

BehaviorVelocity::BehaviorVelocity() : BehaviorBase(btVelocity)
{
  _StateCurr = bpActive;
  _StateNext = bpActive;

}

void BehaviorCollider::Update(float dt)
{
  GameObject& debugObject = Parent();
  GameObject& parent = debugObject.GetParent();
  TransformPtr parentTransform = std::dynamic_pointer_cast<Transform>(parent.Get(Component::ctTransform));
  TransformPtr debugTransform = std::dynamic_pointer_cast<Transform>(debugObject.Get(Component::ctTransform));
  std::shared_ptr<ColliderCircle> parentCollider = std::dynamic_pointer_cast<ColliderCircle>(parent.Get(Component::ctCollider));
  glm::vec2 point1 = { parentTransform->GetTranslation().x, parentTransform->GetTranslation().y };
  float rot1 = parentTransform->GetRotation();

  float radius1 = parentCollider->GetRadius();

  glm::vec2 offset1 = parentCollider->GetOffset();

  offset1 = { ((offset1.x * cos(rot1)) - (offset1.y * sin(rot1))), ((offset1.x * sin(rot1)) + (offset1.y * cos(rot1))) };

  point1 += offset1;
  debugTransform->SetTranslation(glm::vec3(point1.x, point1.y, parentTransform->GetTranslation().z)); 
  debugTransform->SetScale(glm::vec3(parentCollider->GetRadius() * 2, parentCollider->GetRadius() * 2, 0.0f));
}

void BehaviorVelocity::Update(float dt)
{
  GameObject& debugObject = Parent();
  GameObject& parent = debugObject.GetParent();
  TransformPtr parentTransform = std::dynamic_pointer_cast<Transform>(parent.Get(Component::ctTransform));
  TransformPtr debugTransform = std::dynamic_pointer_cast<Transform>(debugObject.Get(Component::ctTransform));

  debugTransform->SetTranslation(glm::vec3(parentTransform->GetTranslation()));

  std::shared_ptr<Physics> parentPhysics = std::dynamic_pointer_cast<Physics>(parent.Get(Component::ctPhysics));
  glm::vec3 parentVelocity = parentPhysics->GetVelocity();
  float magnitude = sqrt((parentVelocity.x * parentVelocity.x) + (parentVelocity.y * parentVelocity.y));

  debugTransform->SetScale(glm::vec3(magnitude, 1.0f, 0.0f));

  if (parentVelocity.x < 0)
    debugTransform->SetRotation(glm::atan(parentVelocity.y / parentVelocity.x) + 3.14f);
  else
    debugTransform->SetRotation(glm::atan(parentVelocity.y / parentVelocity.x));
}