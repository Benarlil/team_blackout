//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorDebug.h                                                   //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "Behavior.h"
#include "BehaviorWeapon.h"
#include "Collider.h"

class BehaviorCollider : public BehaviorBase
{
public:
  int _StateCurr;
  int _StateNext;
  enum States
  {
    bpActive
  };
  BehaviorCollider();
  void Update(float dt);
};

class BehaviorVelocity : public BehaviorBase
{
public:
  int _StateCurr;
  int _StateNext;
  enum States
  {
    bpActive
  };
  BehaviorVelocity();
  void Update(float dt);
};