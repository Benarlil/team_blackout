//------------------------------------------------------------------------------
//
// File Name:	Texture.cpp
// Author(s):	Timur Kazhimuratov (timur.kazhimuratov)
// Project:		Flail Fighters
//	
// Copyright � 2018 DigiPen (USA) Corporation.
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Texture.h"

static void load_image(GLuint &gltexture, sf::Image& image, GLuint width, GLuint height);

Texture::Texture(std::string tex_name, std::string filename)
{
  sf::Image image;
  std::string tName = tex_name;
  std::string tPath = filename;
  if (filename.empty() || !image.loadFromFile(filename))
  {
    image.create(1, 1, sf::Color(255, 255, 255, 255));
    tName = "default";
    tPath = "";
    //something bad happened, probably requires throwing an exception and catching it in Graphics.                        
  }
  name = tName;
  path = tPath;
  //image.flipVertically();

  width = image.getSize().x;
  height = image.getSize().y;

  load_image(glTexture, image, width, height);
}

Texture::Texture(std::string tex_name, sf::Image& image, bool flip_vertically)
{
  name = tex_name;
  if (flip_vertically)
    image.flipVertically();

  width = image.getSize().x;
  height = image.getSize().y;

  load_image(glTexture, image, width, height);
}

Texture::~Texture()
{

}

void Texture::Bind()
{
  if (glTexture) {
    glBindTexture(GL_TEXTURE_2D, glTexture);
  }
}

GLuint Texture::GetTextureID() const
{
  return glTexture;
}

std::string Texture::GetPath() const
{
  return path;
}

std::string Texture::GetName() const
{
  return name;
}

GLuint Texture::GetWidth() const
{ 
  return width; 
}
GLuint Texture::GetHeight() const
{ 
  return height;
}

static void load_image(GLuint &gltexture, sf::Image& image, GLuint width, GLuint height)
{
  glGenTextures(1, &gltexture);
  glBindTexture(GL_TEXTURE_2D, gltexture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
    GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr()
  );
  glEnable(GL_TEXTURE_2D);
}