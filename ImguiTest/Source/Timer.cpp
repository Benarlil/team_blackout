#include "Timer.h"

Timer::Timer()
{
	_Miliseconds = 0.f;
	_Seconds = 0;
	_Minutes = 0;

}

Timer::Timer(Timer& original)
{
	_Miliseconds = original.GetMil();
	_Seconds = original.GetSec();
	_Minutes = original.GetMin();
}

Timer::~Timer()
{
}

void Timer::Update(float dt)
{
	_Miliseconds += (dt * 100);

	if (_Miliseconds >= 100)
	{
		_Seconds++;

		_Miliseconds -= 100;
	}

	if (_Seconds == 60)
	{
		_Minutes++;
		_Seconds = 0;
	}
}

std::string Timer::GetTime()
{
	return _Time;
}

int Timer::GetMin()
{
	return _Minutes;
}

int Timer::GetSec()
{
	return _Seconds;
}

float Timer::GetMil()
{
	return _Miliseconds;
}