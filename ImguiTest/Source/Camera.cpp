//------------------------------------------------------------------------------
//
// File Name:	Camera.cpp
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "Camera.h"

typedef glm::vec3 vec3;

// Default constructor.
Camera::Camera() : Camera(0.0f, 0.0f, 10.0f)
{
}

// Non-default constructor. Default zoom will be 1.
Camera::Camera(float x, float y, float zoom) 
{
	x_ = x;
	y_ = y;
	zoom_ = zoom;
}

// update the camera
void Camera::Update(float dt) 
{
	// this will eventually change the camera based on the positions of all players
}

void Camera::SetData(vec3 pos)
{
  x_ = pos.x;
  y_ = pos.y;
  zoom_ = pos.z;
}

// get the data of the camera as a vector
vec3 Camera::GetData() 
{
	vec3 position(x_, y_, zoom_);

	return position;
}
