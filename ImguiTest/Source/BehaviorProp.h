//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	BehaviorProp.h                                                    //
// Author(s):	Alex Couch (a.couch)                                              //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//
#pragma once
#include "Behavior.h"
#include "Collider.h"
class BehaviorProp : public BehaviorBase
{
  int _StateCurr;
  int _StateNext;
  bool _DoDamage;
public:

  enum States
  {
    bpInvalid = -1,
    bpActive
  };

  BehaviorProp();
  BehaviorProp(bool canDoDamage);
  ~BehaviorProp();
  void Update(float dt);
  void SetDamageFlag(bool canDamage);
  bool GetDamageFlag(void);
  void PropCollide(GameObject& object);
  void SetHandler(GameObject& object);
}; 
