//------------------------------------------------------------------------------
//
// File Name:	GameObject.cpp
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
//
// Copyright © 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "GameObject.h"
#include "DebugLog.h"
#include <algorithm>
#include <functional>

#include "Transform.h"
#include "Physics.h"
#include "Sprite.h"
#include "SoundComponent.h"
#include "Graphics.h"
#include "Collider.h"
#include "Particle.h"

typedef class std::unique_ptr<GameObject> GameObjectPtr;

typedef std::vector<ComponentPtr>::iterator ComponentIterator; // iterator for the vector of Component pointers
typedef std::vector<GameObjectPtr>::iterator ObjectIterator; // iterator for the vector of Object pointers
typedef std::vector<GameObject*>::iterator ChildIterator; // iterator for the vector of child Object pointers

GameObjectPtr GLOBAL_DUMMY = std::make_unique<GameObject>("GLOBAL DUMMY");

// Default constructor. Gives the game object the name "DUMMY"
GameObject::GameObject()
{
  name_ = std::string("DUMMY");

  toDestroy_ = true; // Set the object to not be destroyed yet

  isChild_ = false; // Set the object to not be a child be default

  parent_ = NULL;

  DebugMessage("DUMMY Object Created. DO NOT MODIFY THIS OBJECT, AS IT WILL BE DELETED NEXT UPDATE"); // Send out the Debug Message
}

GameObject& GameObject::GetDummy()
{
  return *GLOBAL_DUMMY; // Return the global dummy so the manager can return it in the event of a problem
}

// Non-default constructor. Gives the game object the passed in name
GameObject::GameObject(std::string name) : name_(name)
{
  toDestroy_ = false; // Set the object to not be destroyed yet

  isChild_ = false; // Set the object to not be a child be default

  parent_ = NULL;

  DebugMessage("Object Created:", name); // Print the object and the name of the object created
}

// Copy Constructor
GameObject::GameObject(GameObject& toClone) : name_(toClone.name_)
{
  toDestroy_ = false; // Set the object to not be destroyed

  isChild_ = false; // Set the object to not be a child be default

  // go through the components and copy the data of each
  for (auto& component : toClone.components_)
  {
    Component::ComponentTypes cType = component->Type();

    switch (cType)
    {
    case Component::ctTransform:
      components_.push_back(std::make_shared<Transform>(*(std::dynamic_pointer_cast<Transform>(component))));
      components_.back()->Parent(*this);
      break;

    case  Component::ctPhysics:
      components_.push_back(std::make_shared<Physics>(*(std::dynamic_pointer_cast<Physics>(component))));
      components_.back()->Parent(*this);
      break;

    case  Component::ctSprite:
      components_.push_back(std::make_shared<Sprite>(*(std::dynamic_pointer_cast<Sprite>(component))));
      components_.back()->Parent(*this);
      break;

    case  Component::ctCollider:
      components_.push_back(std::make_shared<Collider>(*(std::dynamic_pointer_cast<Collider>(component))));
      components_.back()->Parent(*this);
      break;

    case  Component::ctSound:
      components_.push_back(std::make_shared<Sound>(*(std::dynamic_pointer_cast<Sound>(component))));
      components_.back()->Parent(*this);
      break;
    case Component::ctEmitter:
      components_.push_back(std::make_shared<Emitter>(*this, *(std::dynamic_pointer_cast<Emitter>(component))));
      components_.back()->Parent(*this);
      break;
    }
    
  }

  // if the clone has a parent, add that same parent to this object
  if (toClone.parent_)
  {
    AddParent(parent_);
  }
  else
  {
    parent_ = nullptr;
  }

  DebugMessage("Object Clone:", this->name_); // Print that the object has been cloned. If there is a problem in this message, something went terribly wrong in cloning
}


// Destructor
GameObject::~GameObject()
{
  DebugMessage("Game Object Deconstructor:", this->name_); // print the deconstructor that is being called

  if (parent_)
  {
    parent_ = NULL;
  }

  components_.clear(); // Clear the components list
}

// update the game object and its components
void GameObject::Update(const float dt)
{
  if (!IsToDestroy())
  {
    //DebugMessage("Updating Game Object:", this->name_); // Print the name of the object being updated

    for (ComponentIterator it = components_.begin(); it != components_.end(); ++it)
    {
      (*it)->Update(dt); // update any and all components
    }

    for (ChildIterator it = children_.begin(); it != children_.end(); ++it)
    {
      (*it)->Update(dt); // update any and all child objects
    }
  }

  // if the object is going to be destroyed
  else
  {
    // make sure all the children are set to be destroyed as well
    for (ChildIterator it = children_.begin(); it != children_.end(); ++it)
    {
      (**it).toDestroy_ = true; // set the child to be desroyed
      Disown(**it); // diswon the child
    }
  }
}

// set a new name for the game object
void GameObject::Rename(std::string newName)
{
  name_ = newName; // set the new name
}

// Check if the Game Object is named the passed in name. (For finding in the Manager)
bool GameObject::IsNamed(std::string name) const
{
  // if the passed in name matches the object's name
  if (name_ == name)
    return true; // return that this object has that name

  return false; // otherwise, return that it does not.
}

// Set the Game Object to be destroyed. FOR DEBUG ONLY, THIS WILL BE DONE THROUGH BEHAVIOURS LATER
void GameObject::SetToDestroy()
{
  toDestroy_ = true; // set the game object to be destroyed in the next update loop
}

// Check if the game object is set to the destroyed or not.
bool GameObject::IsToDestroy() const
{
  return toDestroy_; // return the current destroy state of the game object
}


///////////////////////////////////////////////////////////////////
/////////////////////// Component functions /////////////////////// 
///////////////////////////////////////////////////////////////////

static bool ComponentSorter(ComponentPtr component1, ComponentPtr component2)
{
  return (*component1).Type() < (*component2).Type(); // return if the type of the first component is before the list of the second component
}

// Add a component to the game object
void GameObject::Add(ComponentPtr component)
{
  if (Get(component->Type()))
  {
    DebugMessage("Component already exists, edit the existing component or remove it before adding another");
    return;
  }

  component->Parent(*this);

  components_.push_back(component); // add the component to the vector of components

  std::sort(components_.begin(), components_.end(), ComponentSorter); // sort the list to ensure proper update order

  DebugMessage("Added Component to Object:", (float)component->Type()); // Print the type of the component added. May have some issues as the type is an enum.
}

// Get a component from the game object
ComponentPtr GameObject::Get(Component::ComponentTypes type) const
{
  int size = components_.size(); // get the size of the components list

  // iterate through the list of components
  for (int i = 0; i < size; i++)
  {
    // if a component the same type as the desired one is found
    if (components_[i]->Type() == type)
      return components_[i]; // return the matching component
  }

  return NULL; // if no matching components are found, return NULL
}

void GameObject::RemoveComponent(Component::ComponentTypes type)
{
  ComponentIterator it = components_.begin();

  while (it != components_.end())
  {
    if ((*it)->Type() == type)
    {
      components_.erase(it);
      DebugMessage("Component Erased!");
      return;
    }

    it++;
  }

  DebugMessage("No component of type ", type);
  DebugMessage("in game object named ", name_);
}


/////////////////////////////////////////////////////////////////
/////////////////////// For Serialization ///////////////////////
/////////////////////////////////////////////////////////////////

// Get the number of components in the object
int GameObject::GetComponentCount() const
{
  return components_.size(); // return the size of the vector of components
}

// Get the name of the object
const std::string& GameObject::GetName() const
{
  return name_;
}

int GameObject::GetChildCount() const
{
  return children_.size();
}

///////////////////////////////////////////////////////////////////////////
/////////////////////// Game Object Child Functions ///////////////////////
///////////////////////////////////////////////////////////////////////////

// returns if the object is a child of another object or not
bool GameObject::isChild() const
{
  return isChild_;
}

// adds a child to the game object
void GameObject::AddChild(GameObject& child)
{
  // set the child flag of the object to true
  child.isChild_ = true;

  // add the child to the vector
  children_.push_back(&child);
}

static bool CheckChild(std::unique_ptr<GameObject>& child, GameObject& checkAgainst)
{
  return child->GetName().compare(checkAgainst.GetName());
}

// removes a child from the game object and sets all necessary flags
void GameObject::Disown(GameObject& child)
{
  // unset the child flag
  child.isChild_ = false;

  int size = children_.size(); // get the size of the components list

  // iterate through the list of children
  for (int i = 0; i < size; i++)
  {
    ChildIterator it = children_.begin() + i;

    // if the passed in child and the one in the list have the same name
    if ((*it)->GetName() == child.GetName())
    {
      std::remove<ChildIterator, GameObject *>(it, it, &child); // move the child to the back of the list
      children_.erase(it); // erase the child
      return;
    }
  }
}

// adds a parent to the game object if it is marked as a child
void GameObject::AddParent(GameObject* parent)
{
  if (parent != NULL || parent->IsNamed("DUMMY"))
  {
    parent_ = parent;

    parent_->AddChild(*this);

    return;
  }

  DebugMessage("Parent Object is unusable, cannot add parent to child"); //Print that the object was not found;
}

// Get the child of the passed in game object
GameObject& GameObject::GetChild(unsigned id) const
{
  if (children_.size() > id)
    return *(children_[id]);

  else
  {
    DebugMessage("No child with ID", (float)id); //Print that the object was not found;
    DebugMessage("Returning Global Dummy");

    return *GLOBAL_DUMMY;
  }
}

// Get the parent of the passed in game object
GameObject& GameObject::GetParent() const
{
  if (parent_)
    return *parent_;

  else
  {
    DebugMessage("No parent in object: ", this->name_); //Print that the object was not found;
    DebugMessage("Returning Global Dummy");

    return *GLOBAL_DUMMY;
  }
}

// To set game objects to be other objects
GameObject& GameObject::operator=(GameObject& other)
{
  if (name_ == other.name_)
    return *this;
  
  else
    return other;
}
