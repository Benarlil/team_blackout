//------------------------------------------------------------------------------//
//                                                                              //
// File Name:	SoundComponent.h                                                  //
// Author(s):	Paul Hause (paul.hause)                                           //
// Project:		Flail Fighters                                                    //
//                                                                              //
//                                                                              //
// Copyright � 2018 DigiPen (USA) Corporation.                                  //
//                                                                              //
//------------------------------------------------------------------------------//

#include "Component.h"
#include <memory>

using vec3 = glm::vec3; // for the position vector

// A Component that allows a game object to take ownership of its own sounds and when to play them.
// This may also allow positions for sounds based on where the object is.
class Sound : public Component
{

public:

  Sound(); // Default constructor. Creates a sound component with default volume and pitch

  // Non-defualt constructor. Creates a sound component with a given volume and pitch. 
  //Optionally, can pass in a position for the sound. It will default to (0, 0, 0).
  Sound(float volume, float pitch, vec3 position); 

  // copy-constructor
  Sound(Sound& copy);

  ~Sound(); // Destructor

  void Update(float dt); // Updates the position of the sound based on the game object

  void Play(std::string id); // Play one of the sounds in the vector using a given id

  int Add(std::string audioFile); // Add a sound to the vector of sounds within the component and return its ID

  void SetVolumeAll(float volume); // Set the volume of every sound in the vector

  void SetVolumeOne(float volume, std::string id); // Set the volume of a single sound

  void SetPitchAll(float pitch); // Set the pitch of every sound in the vector

  void SetPitchOne(float pitch, std::string id); // Set the pitch of a single sound

  float GetPitch();  // Return the pitch of the component

  float GetVolume(); // Return the volume of the component

  float GetVolumeOne(std::string name);

  float GetPitchOne(std::string name);

  void SetLoop(bool loop, std::string id); // set ONE SOUND to loop. There is no function to set every sound in a component to loop, as that could go very wrong very quickly

  std::vector<std::string> GetNames(); // Gets every name from the component and returns them in a vector.

  unsigned int GetCount(); // Gets how many sounds are in the component

  sf::Time Sound::GetSoundLength(std::string name); // Gets the length of a specific sound

  void RemoveSound(std::string audioFile); // Removes a sound from the vector

private:

  std::map<std::string, sf::Sound> sounds_; // The map of sounds attached to this component

  float pitch_; // The pitch to play each sound at

  float volume_; // The volume to play each sound at

  vec3 position_; // The position of the sound (for the listener to use, should we have one)

};

typedef class std::shared_ptr<Sound> SoundPtr;