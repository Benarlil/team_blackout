#pragma once
//------------------------------------------------------------------------------
//
// File Name:	ObjectGui.cpp
// Author(s):	Jack Klein (jack.klein)
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#include "GameObject.h"
#include "Serialize.h"

/* Creates an ImGui window for a game object */
void CreateObjectGui(GameObject & gameObject, Serializer & serializer, sf::Clock clock);