//------------------------------------------------------------------------------
//
// File Name:	GameState.cpp
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "GameState.h"
#include "DebugLog.h"
#include "Graphics.h"


//------------------------------------------------------------------------------
// Public Functions:
//------------------------------------------------------------------------------
namespace Engine
{
  void GameState::Load()
  {
    DebugMessage("GameState Loading");
  }

  void GameState::Init()
  {
    DebugMessage("GameState Initializing");
  }

  void GameState::Update(float dt)
  {
    DebugMessage("GameState Updating");
  }

  void GameState::Shutdown()
  {
    DebugMessage("GameState Shutting down");
  }

  void GameState::Unload()
  {
    DebugMessage("GameState Unloading");
  }

  void GameState::OnAction(sf::Event& event, float dt)
  {
    switch (event.type)
    {
      case sf::Event::Resized:
      {
        sf::View view = Graphics::GetMainWindow().getView();
        view.setSize(event.size.width, event.size.height);
        view.setViewport(sf::FloatRect(0, 0, event.size.width, event.size.height));
        Graphics::GetMainWindow().setView(view);
      }
    }
  }

}