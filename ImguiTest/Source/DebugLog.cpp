//------------------------------------------------------------------------------
//
// File Name:	DebugLog.cpp
// Author(s):	Paul Hause (paul.hause)
// Project:		Flail Fighters
//
//
// Copyright © 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "DebugLog.h"
#include <string>  // string class

// Private variable for the file stream.
static std::ofstream debugFile;

// Initialize the logging module.
void DebugInit()
{
  std::string filePath; // contain the file path

  filePath = std::getenv("APPDATA"); // get the appdata path
  filePath += "\\Flail Fighters"; // add the Flail Fighters folder for creation

  CreateDirectoryA(filePath.c_str(), NULL); // create the Flail Fighters directory in AppData

  filePath += "\\debug.log"; // add the debug log file now that the directory exists

  debugFile.open(filePath, std::fstream::out); // open the debug file

  // catch if the file fails to open
  if (!debugFile)
  {
    std::cout << "Failed to open/create debug.log." << std::endl; // print so
  }

}

// Output a message to the logging file.
void DebugMessage(std::string message)
{
  // if there is a problem with the file
  if (!debugFile)
  {
    std::cout << "Failed to write to debug.log." << std::endl; // print so
  }

  // print to debug file
  else
  {
		debugFile << message; // print based on the variables
		
		debugFile << std::endl; // output an endline
  }
}

// Output a message to the logging file. This is overloaded to take a second string as a "variable"
void DebugMessage(std::string message, std::string variable)
{
  // if there is a problem with the file
  if (!debugFile)
  {
    std::cout << "Failed to write to debug.log." << std::endl; // print so
  }

  // print to debug file
  else
  {
    debugFile << message; // print based on the variables

    debugFile << variable; // print the second string passed in

    debugFile << std::endl; // output an endline
  }
}

// Output a message to the logging file. This is overloaded to take a float as a "variable"
void DebugMessage(std::string message, float number)
{
  // if there is a problem with the file
  if (!debugFile)
  {
    std::cout << "Failed to write to debug.log." << std::endl; // print so
  }

  // try to print to debug file
  else
  {
    debugFile << message; // print based on the variables

    debugFile << number; // print the passed in number

    debugFile << std::endl; // output an endline
  }
}

// Shutdown the logging module.
void DebugShutdown()
{
  if(debugFile)
   debugFile.close(); // close the debug file
}
