//------------------------------------------------------------------------------
//
// File Name:	GameState.h
// Author(s):	Alex Couch
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------
#pragma once

//////////////////////////////////////////////////////////
#ifndef GAMESTATE_H
#define GAMESTATE_H
//////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "stdafx.h"

namespace Engine
{
  class GameState
  {
  public:
    virtual void Load() = 0; // pointer to the load gamestate
    virtual void Init() = 0; // pointer to the init gamestate
    virtual void Update(float dt) = 0; // pointer to the update gamestate
    virtual void Shutdown() = 0; // pointer to the shutdown gamestate
    virtual void Unload() = 0; // pointer to the unload gamestate
    virtual void OnAction(sf::Event& event, float dt);
  };

#define REGISTER_GAMESTATE(x) x,
  typedef enum 
  {
    #include "GameStates.h"
    NUM_GAMESTATES
  }GAMESTATES;
#undef REGISTER_GAMESTATE

#define REGISTER_GAMESTATE(x) #x,
  static const char* GAMESTATE_STRINGS[] = 
  {
    #include "GameStates.h"
    "Invalid_State"
  };
#undef REGISTER_GAMESTATE
}
#endif //GAMESTATE_H
