//------------------------------------------------------------------------------
//
// File Name:	Transform.h
// Author(s):	Jack Klein
// Project:		Blackout
// Course:		GAM200
//
// Copyright � 2018 DigiPen (USA) Corporation.
//
//------------------------------------------------------------------------------

#pragma once

//------------------------------------------------------------------------------
// Include Files:
//------------------------------------------------------------------------------
#include "Component.h"
#include  <glm/ext/vector_float3.hpp>
#include <glm/ext/matrix_float4x4.hpp>
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// Forward References:
//------------------------------------------------------------------------------

//typedef struct Matrix2D Matrix2D;
//typedef struct Vector2D Vector2D;

//------------------------------------------------------------------------------
// Public Consts:
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Public Structures:
//------------------------------------------------------------------------------



typedef class Transform : public Component
{
private:
	// The translation (or world position) of a game object.
	glm::vec3	translation_;

	// The rotation (or orientation) of a game object.
	float	rotation_;

	// The scale (or size) of a game object.
	glm::vec3	scale_;

  /* The point to scale around if doing an affine scaling */
  glm::vec3 affineScalePoint_;

	// The transformation matrix resulting from concatenating the 
	//	 matrix = Translation*Rotation*Scale matrices.
	glm::mat4x4	matrix_;

	// True if the transformation matrix needs to be recalculated.
	bool	isDirty_;

public:
	// Dynamically allocate a new transform object.
	// (Hint: Use calloc() to ensure that all member variables are initialized to 0.)
	// (Hint: Failing to initialize the scale values to non-zero values will result in invisible sprites.)
	// Params:
	//	 x = Initial world position on the x-axis.
	//	 y = Initial world position on the y-axis.
	// Returns:
	//	 If the memory allocation was successful,
	//	   then return a pointer to the allocated memory,
	//	   else return NULL.
	Transform(float x, float y, float z);


	// Dynamically allocate a clone of an existing transform.
	// (Hint: Make sure to perform a shallow copy or deep copy, as appropriate.)
	// Params:
	//	 other = Pointer to the component to be cloned.
	// Returns:
	//	 If 'other' is valid and the memory allocation was successful,
	//	   then return a pointer to the cloned component,
	//	   else return NULL.
	//ComponentPtr Clone(void) const;

	// Free the memory associated with a transform object.
	// (Also, set the transform pointer to NULL.)
	~Transform();

	// Get the transform matrix, based upon translation, rotation and scale settings.
	// Returns:
	//	 If the transform pointer is valid,
	//		then return a pointer to the component's matrix structure,
	//		else return a NULL pointer.
	const glm::mat4x4 & GetMatrix() const;

	// Get the transform matrix, based upon translation, rotation and scale settings.
	// Params:
	//	 dt = Change in time since last update
	void Update(float dt);

	// Get the translation of a transform component.
	// Returns:
	//	 If the transform pointer is valid,
	//		then return a pointer to the component's translation structure,
	//		else return a NULL pointer.
	const glm::vec3 & GetTranslation() const;

	// Get the rotation value of a transform component.
	// Returns:
	//	 If the transform pointer is valid,
	//		then return the component's rotation value (in radians),
	//		else return 0.0f.
	float GetRotation() const;

	// Get the scale of a transform component.
	// Returns:
	//	 If the transform pointer is valid,
	//		then return a pointer to the component's scale structure,
	//		else return a NULL pointer.
	const glm::vec3 & GetScale() const;

	// Set the translation of a transform component.
	// Params:
	//	 translation = Pointer to a translation vector.
	void SetTranslation(const glm::vec3 & translation);

	// Set the rotation of a transform component.
	// Params:
	//	 rotation = The rotation value (in radians).
	void SetRotation(float rotation);

	// Set the scale of a transform component.
	// Params:
	//	 translation = Pointer to a scale vector.
  void SetScale(const glm::vec3 & scale, const glm::vec3 & affinePoint = glm::vec3(0.0f, 0.0f, 0.0f));

	// Add the translation of a transform component.
	// Params:
	//	 translation = Pointer to a translation vector.
	void AddTranslation(const glm::vec3 & translation);

	// Add the rotation of a transform component.
	// Params:
	//	 rotation = The rotation value (in radians).
	void AddRotation(float rotation);

	// Set the scale of a transform component.
	// Params:
	//	 translation = Pointer to a scale vector.
	void AddScale(const glm::vec3 & scale);

} Transform;

typedef class std::shared_ptr<Transform> TransformPtr;
